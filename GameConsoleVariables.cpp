#include "StdAfx.h"
#include "GameConsoleVariables.h"

SGameCVars::SGameCVars()
{
	if (gEnv->IsDedicated())
		InitServerCvars();
	else
		InitClientCvars();

	InitBothSidedCvars();
}

void SGameCVars::InitServerCvars()
{
	IConsole *console = gEnv->pConsole;
	//general
	REGISTER_STRING("sv_admin_password", "", VF_NULL, "Whoever types the right password, he will have access to certain server functionality");
	pAdminPass = console->GetCVar("sv_admin_password");
	REGISTER_COMMAND("sv_restart_match", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Restarts entire game and data");
	REGISTER_COMMAND("sv_admin_log_in", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Logs client into server admin interface");
	REGISTER_COMMAND("sv_player_move_to_player", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Moves player of specified name to another player");
	REGISTER_COMMAND("sv_kick", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Kicks player from server");
	REGISTER_COMMAND("sv_team_name", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Team with index specified here will get its name modified");
	REGISTER_COMMAND("sv_player_set_team", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Moves player to specified team");
	REGISTER_COMMAND("sv_player_kill", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Kills player with no death count");
	REGISTER_COMMAND("sv_player_respawn", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Respawns player if he is dead");
	REGISTER_COMMAND("sv_give_admin", (ConsoleCommandFunc)SServerComponent::ServerCommandHandler, VF_NULL, "Gives admin rights to specified player");
}

void SGameCVars::InitClientCvars()
{
	REGISTER_COMMAND("sv_admin_log_in", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Logs client into server admin interface");
	REGISTER_COMMAND("sv_restart_match", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Restarts entire game and data");
	REGISTER_COMMAND("sv_player_move_to_player", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Moves player of specified name to another player");
	REGISTER_COMMAND("sv_kick", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Kicks player from server");
	REGISTER_COMMAND("sv_team_name", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Team with index specified here will get its name modified");
	REGISTER_COMMAND("sv_player_set_team", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Moves player to specified team");
	REGISTER_COMMAND("sv_player_kill", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Kills player with no death count");
	REGISTER_COMMAND("sv_player_respawn", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Respawns player if he is dead");
	REGISTER_COMMAND("sv_give_admin", (ConsoleCommandFunc)SServerComponent::ClientCommandHandler, VF_NULL, "Gives admin rights to specified player");
}

void SGameCVars::InitBothSidedCvars()
{
	IConsole *console = gEnv->pConsole;
	//general

}