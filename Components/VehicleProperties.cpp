#include "StdAfx.h"
#include "VehicleProperties.h"
inline bool Serialize(Serialization::IArchive& archive, SAdvancedAnimationComponent::SDefaultScopeSettings& defaultSettings, const char* szName, const char* szLabel)
{
	archive(Serialization::MannequinControllerDefinitionPath(defaultSettings.m_controllerDefinitionPath), "ControllerDefPath", "Controller Definition");
	archive.doc("Path to the Mannequin controller definition");

	std::shared_ptr<Serialization::SMannequinControllerDefResourceParams> pParams;
	if (archive.isEdit())
	{
		pParams = std::make_shared<Serialization::SMannequinControllerDefResourceParams>();

		IAnimationDatabaseManager &animationDatabaseManager = gEnv->pGameFramework->GetMannequinInterface().GetAnimationDatabaseManager();
		if (defaultSettings.m_controllerDefinitionPath.size() > 0)
		{
			pParams->pControllerDef = animationDatabaseManager.LoadControllerDef(defaultSettings.m_controllerDefinitionPath);
		}
	}

	archive(Serialization::MannequinScopeContextName(defaultSettings.m_contextName, pParams), "DefaultScope", "Default Scope Context Name");
	archive.doc("The Mannequin scope context to activate by default");

	archive(Serialization::MannequinFragmentName(defaultSettings.m_fragmentName, pParams), "DefaultFragment", "Default Fragment Name");
	archive.doc("The fragment to play by default");

	return true;
}