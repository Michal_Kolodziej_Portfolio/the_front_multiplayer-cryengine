/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Audio component. Mostly used for weapons but only temporarily.

--------------------------------------------------------------------------------- */

#pragma once

#include <CryAudio/IAudioInterfacesCommonData.h>
#include <CrySerialization/Forward.h>
#include <CrySchematyc/Reflection/TypeDesc.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CryString/CryString.h>

struct SEntityAudioComponent final : public IEntityComponent
{
public:
	SEntityAudioComponent::SEntityAudioComponent(string n1, string n2 = "", string n3 = "", string n4 = "", string n5 = "")
	{
		SetNameAndId(n1, n2, n3, n4, n5);
	}
	virtual ~SEntityAudioComponent() {}
	virtual void   Initialize() override;
	inline void ReflectType(Schematyc::CTypeDesc<SEntityAudioComponent>& desc)
	{
		desc.SetGUID("{924B2A79-B026-4592-B215-1CBFB1BC8169}"_cry_guid);
	}
	//AUDIO
	void Play(int index);
	void Stop(int index);
	void SetNameAndId(string n1, string n2 = "", string n3 = "", string n4 = "", string n5 = "");
	//VARIABLES
	//Number of triggers possible to fire
	CryAudio::ControlId sTrigger[5] = { CryAudio::InvalidControlId };
	string sTriggerName[5];
	//
	IEntityAudioComponent *pAudioComponent = nullptr;
	CryAudio::AuxObjectId auxObjectId = CryAudio::InvalidAuxObjectId;
};