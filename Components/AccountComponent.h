/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : This entity component is the first appearance of client on the server. It handles EVERYTHING that belongs to client BEFORE he joins the game

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "NetPlayer.h"

class CLoginMenu;
class CMainMenu;
class CLobbyMenu;
class CGameTeam;

class CAccountComponent : public IEntityComponent
{
	const EEntityAspects kAccountData = eEA_GameServerA;
public:
	CAccountComponent() = default;
	CAccountComponent::~CAccountComponent();

	virtual void Initialize() override;
	static void ReflectType(Schematyc::CTypeDesc<CAccountComponent>& desc);
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	//networking
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kAccountData; };
	void ServerUpdateAccountData() { NetMarkAspectsDirty(kAccountData); }
	//server methods
	//client methods
	void SetLogin(string newLogin);
	void SetTeam(CGameTeam *pTeamToSet, int index);
	//others
	string GetLogin() { return sLogin; }
	int GetTeamIndex() { return iTeam; }
	CGameTeam *GetTeam();
	int GetIndexInTeam() { return iIndex; }
	bool InLobby() { return bIsInLobby; }
	void SetPlayerClass(string player_class) { sPlayerClass = player_class; }
	void SetPlayer(CPlayerComponent *pNewPlayer);
	CPlayerComponent *GetAssociatedPlayer() { return pAssociatedPlayer; }
	static CAccountComponent *GetLocalAccount() { return pLocalAccount; }
	void SetLobbyOpened(bool open) { bIsLobbyOpened = open; }
	string GetPlayerClass() { return sPlayerClass; }

	CLoginMenu *GetLoginMenu() { return pLoginMenu; }
	CMainMenu *GetMainMenu() { return pMainMenu; }
	CLobbyMenu *GetLobbyMenu() { return pLobbyMenu; }
	
private:
	string sLogin;
	int iTeam = -1;
	int iIndex = -1;
	bool bIsInLobby = false;
	string sPlayerClass;

	bool bIsLocal = false;

	static CAccountComponent *pLocalAccount;

	bool bIsLobbyOpened = false;

	//ui 
	CLoginMenu *pLoginMenu = nullptr;
	CMainMenu *pMainMenu = nullptr;
	CLobbyMenu *pLobbyMenu = nullptr;

	CPlayerComponent *pAssociatedPlayer = nullptr;
	EntityId associatedPlayerId = 0;
	EntityId associatedPlayerIdComp = 0;

};
