/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores player component properties exposed and unexposed for editor

--------------------------------------------------------------------------------- */

#pragma once
#include "Components/CharacterControllerComponent.h"
#include "Components/AdvancedAnimationComponent.h"
#include "Components/CameraComponent.h"

class CAmmoPackComponent;
class CPlayerComponent;
struct SItemComponent;

struct SAnimationProperties
{
	inline bool operator==(const SAnimationProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SAnimationProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	Cry::DefaultComponents::EMeshType rType;
	Schematyc::CharacterFileName sCharacterFileFP;
	Schematyc::CharacterFileName sCharacterFileTP;
	Cry::DefaultComponents::SRenderParameters pRenderParams;
	Schematyc::MannequinAnimationDatabasePath sDatabaseFile;
	SAdvancedAnimationComponent::SDefaultScopeSettings sDefaultScopeSettings;
	bool bAnimationDrivenMotion;
	bool bGroundAlignment;
	Cry::DefaultComponents::SPhysicsParameters pPhysicsParams;
};
static void ReflectType(Schematyc::CTypeDesc<SAnimationProperties>& desc)
{
	desc.SetGUID("{370E1874-4CB8-4979-84D8-A2C78F7C3B54}"_cry_guid);
	desc.AddMember(&SAnimationProperties::rType, 'type', "Type", "Type", "Determines the behavior of the static mesh", Cry::DefaultComponents::EMeshType::RenderAndCollider);
	desc.AddMember(&SAnimationProperties::sCharacterFileFP, 'filf', "CharacterFirstPerson", "Character First Person", "Determines the character to load", "");
	desc.AddMember(&SAnimationProperties::sCharacterFileTP, 'filt', "CharacterThirdPerson", "Character Third Person", "Determines the character to load", "");
	desc.AddMember(&SAnimationProperties::sDatabaseFile, 'dbpa', "DatabasePath", "Animation Database", "Path to the Mannequin .adb file", "");
	desc.AddMember(&SAnimationProperties::sDefaultScopeSettings, 'defs', "DefaultScope", "Default Scope Context Name", "Default Mannequin scope settings", SAdvancedAnimationComponent::SDefaultScopeSettings());
	desc.AddMember(&SAnimationProperties::bAnimationDrivenMotion, 'andr', "AnimDriven", "Animation Driven Motion", "Whether or not to use root motion in the animations", false);
	desc.AddMember(&SAnimationProperties::bGroundAlignment, 'grou', "GroundAlign", "Use Ground Alignment", "Enables adjustment of leg positions to align to the ground surface", false);
}

//GAMEPLAY PLAYER SETTINGS!!
struct SGameplayProperties
{
	inline bool operator==(const SGameplayProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SGameplayProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	float fHealth, fMaxHealth, fHealthReg, fStamina, fMaxStamina, fStaminaReg, fMoveSpeed, fSprintSpeed, fCurrentMoveSpeed, fAdditionalWeight;

	int iStrength;
};
static void ReflectType(Schematyc::CTypeDesc<SGameplayProperties>& desc)
{
	desc.SetGUID("{EB2723EF-BB65-44D0-98B7-67523269C868}"_cry_guid);
	desc.AddMember(&SGameplayProperties::fMoveSpeed, 'msp', "MoveSpeed", "Run speed", "Player movement speed", 20.5f);
	desc.AddMember(&SGameplayProperties::fSprintSpeed, 'spd', "SprintSpeed", "Sprint speed", "Player sprint speed", 50.f);
	desc.AddMember(&SGameplayProperties::fMaxHealth, 'mhlt', "MaxHealth", "Health", "Player's max health", 100.f);
	desc.AddMember(&SGameplayProperties::fHealthReg, 'hrgn', "HealthRegen", "Health regeneration ratio", "Player health regeneration ratio", 0.02f);
	desc.AddMember(&SGameplayProperties::fMaxStamina, 'mstm', "MaxStamina", "Stamina", "Player's max stamina", 100.f);
	desc.AddMember(&SGameplayProperties::fStaminaReg, 'srgn', "StaminaRegen", "Stamina regeneration ratio", "Player stamina regeneration ratio", 0.05f);
	desc.AddMember(&SGameplayProperties::iStrength, 'strg', "Strength", "Strength", "Player strength", 10);

}
/////////////////////////////////

struct SCameraProperties
{
	inline bool operator==(const SCameraProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SCameraProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	bool bActivateOnCreate;
	Schematyc::Range<0, 32768> rNearPlane;
	CryTransform::CClampedAngle<20, 360> cFieldOfView;
};
static void ReflectType(Schematyc::CTypeDesc<SCameraProperties>& desc)
{
	desc.SetGUID("{090C30C6-F61E-45C7-B0E3-FDA133CBEB98}"_cry_guid);
	desc.AddMember(&SCameraProperties::bActivateOnCreate, 'actv', "Active", "Active", "Whether or not this camera should be activated on component creation", true);
	desc.AddMember(&SCameraProperties::rNearPlane, 'near', "NearPlane", "Near Plane", nullptr, 0.25f);
	desc.AddMember(&SCameraProperties::cFieldOfView, 'fov', "FieldOfView", "Field of View", nullptr, 75.0_degrees);
}
struct SPlayerProperties
{
	inline bool operator==(const SPlayerProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SPlayerProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }
	//CONTROLLER
	SPhysics pPhys;
	SMovement pMove;
	//Methods and properties
	float GetHealth(bool max) { return max ? sGameplayProperties.fMaxHealth : sGameplayProperties.fHealth; }
	float GetStamina(bool max) { return max ? sGameplayProperties.fMaxStamina : sGameplayProperties.fStamina; }
	void SetHealth(bool max, float value) { max ? sGameplayProperties.fMaxHealth = value : sGameplayProperties.fHealth = value; }
	void SetHealthReg(float reg) { sGameplayProperties.fHealthReg = reg; }
	void SetStamina(bool max, float value) { max ? sGameplayProperties.fMaxStamina = value : sGameplayProperties.fStamina = value; }
	void SetStaminaReg(float reg) { sGameplayProperties.fStaminaReg = reg; }
	void Refresher()
	{
		RegenerateStamina();
		RegenerateHealth();
	}

	//USAGE
	void Init()
	{
		SetHealth(false, sGameplayProperties.fMaxHealth);
		SetStamina(false, sGameplayProperties.fMaxStamina);
		sGameplayProperties.fCurrentMoveSpeed = sGameplayProperties.fMoveSpeed;
		sGameplayProperties.fAdditionalWeight = 0.f;
	}
	void UseStamina(float cost) { bStaminaInUse = true; SetStamina(false, sGameplayProperties.fStamina - cost); }
	void StopUseStamina() { bStaminaInUse = false; }
	void UseHealth(float cost) { SetHealth(false, sGameplayProperties.fHealth - cost); }
	void RegenerateStamina()
	{
		if (bStaminaInUse)
			return;

		if (sGameplayProperties.fStamina < sGameplayProperties.fMaxStamina)
		{
			SetStamina(false, sGameplayProperties.fStamina + sGameplayProperties.fStaminaReg);
			if (sGameplayProperties.fStamina > sGameplayProperties.fMaxStamina)
				SetStamina(false, sGameplayProperties.fMaxStamina);
		}
	}
	void RegenerateHealth()
	{
		if (sGameplayProperties.fHealth < sGameplayProperties.fMaxHealth)
		{
			SetHealth(false, sGameplayProperties.fHealth + sGameplayProperties.fHealthReg);
			if (sGameplayProperties.fHealth > sGameplayProperties.fMaxHealth)
				SetHealth(false, sGameplayProperties.fMaxHealth);
		}
	}
	void UpdateHud()
	{
		bHudChanged = true;
	}
	bool bHudChanged = false;
	bool bStaminaInUse = false;
	//GAMEPLAY PROPERTIES
	SGameplayProperties sGameplayProperties;
	//ANIMATION PROPERTIES
	SAnimationProperties sAnimationProperties;
	//CAMERA PROPERTIES
	SCameraProperties sCameraProperties;

	//Non editor
	EntityId vehicle = 0;
	EntityId vehicleComp = 0;
	bool bIsDriver = false;
	bool bUnlockCamera = false;
	bool bCameraTimerSet = false;
	EntityId accountId = 0;
	EntityId accountIdComp = 0;
	bool bIsAlive = true;
	int iKills = 0;
	int iDeaths = 0;
	bool bCanSprint = true;
	CPlayerComponent *pTeamMateToFollow[32] = { nullptr };
	int iCurTeammateFollowed = 0;
	int iDeathCamIndex = 0;
	Vec3 vTargetPosition = ZERO;
	Vec3 vTargetPositionLast = ZERO;
	Vec3 vCameraDir = ZERO;
	EntityId visitedAmmoPackId = 0;
	EntityId visitedAmmoPackIdComp = 0;
	//target item
	SItemComponent *pTargetItem = nullptr;
	SItemComponent *pLastTargetItem = nullptr;
	EntityId targetEntityId = 0;
	EntityId targetEntityIdComp = 0;
	bool bIsJumping = false;
	Vec3 lastGroundContactPoint = ZERO;
	Vec3 lastGroundContactPointSaved = ZERO;
};
static void ReflectType(Schematyc::CTypeDesc<SPlayerProperties>& desc)
{
	desc.SetGUID("{0759772D-4ECC-435B-A7DE-8AEE2659D2D0}"_cry_guid);
	desc.SetLabel("Player settings");
	desc.SetDescription("Player properties");
	//GAMEPLAY
	desc.AddMember(&SPlayerProperties::sGameplayProperties, 'gmpl', "GameplayProperties", "Gameplay settings", "Player initial gameplay settings", SGameplayProperties());
	//PHYSICS
	desc.AddMember(&SPlayerProperties::pPhys, 'pphy', "Physics", "Physical settings", "Player physics properties", SPhysics());
	//MOVEMENT
	desc.AddMember(&SPlayerProperties::pMove, 'pmov', "Movement", "Movement settings", "Player movement properties", SMovement());
	//ANIMATIONS
	desc.AddMember(&SPlayerProperties::sAnimationProperties, 'sani', "AnimationProperties", "Animation settings", "Setting of player animation system", SAnimationProperties());
	//CAMERA
	desc.AddMember(&SPlayerProperties::sCameraProperties, 'scam', "CameraProperties", "Camera settings", "Setting of player camera system", SCameraProperties());
}