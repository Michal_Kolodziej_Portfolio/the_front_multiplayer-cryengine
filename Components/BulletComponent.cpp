#include "StdAfx.h"
#include "BulletComponent.h"
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "PlayerComponent.h"

static void RegisterBullet(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CBulletComponent));
		{

		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterBullet);

void CBulletComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (sBulletProps != sPrevBulletProps)
		{
			sPrevBulletProps = sBulletProps;
			//ReLoadBullet();
		}
	}
	break;
	case ENTITY_EVENT_COLLISION:
	{
		//simple collision event catch
		EventPhysCollision *physCollision = reinterpret_cast<EventPhysCollision *>(event.nParam[0]);
		if (physCollision)
		{
			IPhysicalEntity *pThisEntityPhysics = physCollision->pEntity[0];
			IEntity *pThisEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pThisEntityPhysics);
			IPhysicalEntity *pColliderPhysics = physCollision->pEntity[1];
			IEntity *pCollider = gEnv->pEntitySystem->GetEntityFromPhysics(pColliderPhysics);
			//if collider is a valid entity
			if (pCollider)
			{
				if (CPlayerComponent *pVictim = pCollider->GetComponent<CPlayerComponent>())
				{	
					//and if collider is player, then send info to the collider player(victim) that he was hit
					pVictim->ServerOnHit(this, pOwner, pThisEntity->GetForwardDir(), pOwner->GetEntity()->GetWorldPos());
				}
				else
				{
					//if it was any other entity push it slightly
					pe_action_impulse impulse;
					impulse.impulse = GetEntity()->GetForwardDir() * 6.f;
					pCollider->GetPhysicalEntity()->Action(&impulse);
				}
			}
			//in the end remove this entity from the stage
			gEnv->pEntitySystem->RemoveEntity(GetEntityId());
		}
	}
	break;
	}
}

uint64 CBulletComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED) | BIT64(ENTITY_EVENT_COLLISION);
}

void CBulletComponent::ReflectType(Schematyc::CTypeDesc<CBulletComponent>& desc)
{
	desc.SetGUID("{59FA9AD8-A4DB-444D-8663-A6C646C2799A}"_cry_guid);
	desc.SetEditorCategory("Items");
	desc.SetLabel("Bullet");
	desc.SetDescription("Test bullet system - FURY");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CBulletComponent::sBulletProps, 'bpr', "BulletProperties", "BulletProperties", "BulletProperties", SBulletProperties());
}

void CBulletComponent::ReLoadBullet(Vec3 bulletDirection)
{
	//it pretty much does all the job that bullet should do; it pushes bullet forward to defined direction (bulletDirection)
	//all properties like mass, size, etc are set in schematyc
	GetEntity()->LoadGeometry(0, sBulletProps.sMesh.value.c_str());
	pe_params_particle partParams;
	partParams.flags = particle_traceable | particle_no_roll | pef_log_collisions;
	string mass = ToString(sBulletProps.fMass);
	mass = "0.00000" + mass;
	sBulletProps.fMass = strtof(mass, nullptr);
	string size = ToString(sBulletProps.fSize);
	size = "0.000" + size;
	sBulletProps.fSize = strtof(size, nullptr);
	partParams.mass = sBulletProps.fMass;
	partParams.size = sBulletProps.fSize;
	partParams.thickness = 0.1f;
	partParams.gravity = Vec3(0, 0, 0);
	partParams.kAirResistance = 0.0f;
	partParams.surface_idx = 0;
	partParams.velocity = sBulletProps.fSpeed;
	partParams.heading = bulletDirection;
	SEntityPhysicalizeParams params;
	params.pParticle = &partParams;
	params.type = PE_PARTICLE;
	GetEntity()->Physicalize(params);
}

float CBulletComponent::GetDamage()
{
	//damage calculation is pretty cool; depending on mass, size and speed of a bullet
	return ((sBulletProps.fMass + sBulletProps.fSize) * sBulletProps.fSpeed) * 100.f;
}

float CBulletComponent::GetBulletRecoil()
{
	//recoil is counted similar way to damage
	return sBulletProps.fMass + sBulletProps.fSize + sBulletProps.fSpeed * 0.01f;
}
