#include "StdAfx.h"
#include "AmmoPackComponent.h"


void CAmmoPackComponent::ClientActionRegister()
{
	pInput->RegisterAction("item", "place", [this](int activationMode, float value) { Client_Action_RenderPlacement(activationMode); HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_Place, activationMode); });
	pInput->BindAction("item", "place", eAID_KeyboardMouse, EKeyId::eKI_Mouse1);
}
//ACTIONS HAPPEN ONLY ON SERVER SIDE
void CAmmoPackComponent::ServerActionRegister()
{
	using std::placeholders::_1;

	SERVER_REGISTER_ACTION((ItemInputFlags)IEInputFlag::Action_Place, std::bind(&CAmmoPackComponent::Action_Place, this, _1));
}

void CAmmoPackComponent::Action_Place(int activationMode)
{
	if (activationMode == eIS_Released)
	{
		ServerPlace();
	}
}
