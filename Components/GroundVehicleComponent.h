/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : represents all ground vehicles, it is not virtual anymore

--------------------------------------------------------------------------------- */

#pragma once

#include "VehicleComponent.h"

class CGroundVehicleComponent : public SVehicleComponent
{
public:
	CGroundVehicleComponent() = default;
	CGroundVehicleComponent::~CGroundVehicleComponent() {}

	static void ReflectType(Schematyc::CTypeDesc<CGroundVehicleComponent>& desc);
};
