#include "StdAfx.h"
#include "GroundVehicleComponent.h"
#include "CrySchematyc/Env/Elements/EnvComponent.h"
#include "CrySchematyc/Env/IEnvRegistrar.h"

static void RegisterGroundVehicle(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CGroundVehicleComponent));
		{

		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterGroundVehicle);

void CGroundVehicleComponent::ReflectType(Schematyc::CTypeDesc<CGroundVehicleComponent>& desc)
{
	desc.SetGUID("{58E8DCC9-CB17-4771-B14F-A77F2107DC43}"_cry_guid);
	desc.AddBase<SItemComponent>();
	desc.AddBase<SVehicleComponent>();
	desc.SetEditorCategory("Vehicles");
	desc.SetLabel("Ground vehicle");
	desc.SetDescription("Ground vehicle component");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CGroundVehicleComponent::sItemProperties, 'ipr', "ItemProperties", "Item properties", "ItemProperties", SItemProperties());
	desc.AddMember(&CGroundVehicleComponent::sVehicleProperties, 'vpr', "VehicleProperties", "Vehicle properties", "Properties of this vehicle", SVehicleProperties());
}
