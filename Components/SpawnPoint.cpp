#include "StdAfx.h"
#include "SpawnPoint.h"

#include <CrySchematyc/Reflection/TypeDesc.h>
#include <CrySchematyc/Utils/EnumFlags.h>
#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include <CrySchematyc/Env/Elements/EnvFunction.h>
#include <CrySchematyc/Env/Elements/EnvSignal.h>
#include <CrySchematyc/ResourceTypes.h>
#include <CrySchematyc/MathTypes.h>
#include <CrySchematyc/Utils/SharedString.h>

static void RegisterSpawnPoint(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CSpawnPointComponent));
		{

		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterSpawnPoint);

void CSpawnPointComponent::ProcessEvent(SEntityEvent & event)
{
	if (event.event == ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED)
	{
	}
}

uint64 CSpawnPointComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);
}

void CSpawnPointComponent::ReflectType(Schematyc::CTypeDesc<CSpawnPointComponent>& desc)
{
	desc.SetGUID("{118D66DD-6909-4468-B73A-4D48F5FE6AC8}"_cry_guid);
	desc.SetEditorCategory("Game");
	desc.SetLabel("SpawnPoint");
	desc.SetDescription("Used to spawn entities. Choose team to assign it to");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CSpawnPointComponent::iTeam, 'itea', "Team", "Team", "Team of the etities that will use this spawn point to spawn", TeamEnum());
}

void CSpawnPointComponent::SpawnEntity(IEntity* otherEntity)
{
	otherEntity->SetWorldTM(m_pEntity->GetWorldTM());
}
