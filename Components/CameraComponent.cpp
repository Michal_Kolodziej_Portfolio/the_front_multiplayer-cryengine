#include "StdAfx.h"
#include "CameraComponent.h"

SCameraComponent* SCameraComponent::s_pActiveCamera = nullptr;

SCameraComponent::~SCameraComponent()
{
	if (s_pActiveCamera == this)
	{
		s_pActiveCamera = nullptr;

		if (IHmdDevice* pDevice = gEnv->pSystem->GetHmdManager()->GetHmdDevice())
		{
			pDevice->SetAsynCameraCallback(nullptr);
		}
	}
}

void SCameraComponent::Initialize()
{
	if (m_bActivateOnCreate)
	{
		Activate();
	}

	m_pAudioListener = m_pEntity->GetOrCreateComponent<Cry::Audio::DefaultComponents::CListenerComponent>();
	CRY_ASSERT(m_pAudioListener != nullptr);
	m_pAudioListener->SetComponentFlags(m_pAudioListener->GetComponentFlags() | IEntityComponent::EFlags::UserAdded);
}

void SCameraComponent::ProcessEvent(SEntityEvent& event)
{
	if (event.event == ENTITY_EVENT_UPDATE)
	{
		const CCamera& systemCamera = gEnv->pSystem->GetViewCamera();

		const float farPlane = gEnv->p3DEngine->GetMaxViewDistance();

		m_camera.SetFrustum(systemCamera.GetViewSurfaceX(), systemCamera.GetViewSurfaceZ(), m_fieldOfView.ToRadians(), m_nearPlane, farPlane, systemCamera.GetPixelAspectRatio());
		m_camera.SetMatrix(GetWorldTransformMatrix());

		gEnv->pSystem->SetViewCamera(m_camera);
	}
	else if (event.event == ENTITY_EVENT_START_GAME || event.event == ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED)
	{
		if (m_bActivateOnCreate && !IsActive())
		{
			Activate();
		}
	}
}

uint64 SCameraComponent::GetEventMask() const
{
	uint64 bitFlags = IsActive() ? BIT64(ENTITY_EVENT_UPDATE) : 0;
	bitFlags |= BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);

	return bitFlags;
}

bool SCameraComponent::OnAsyncCameraCallback(const HmdTrackingState& sensorState, IHmdDevice::AsyncCameraContext& context)
{
	context.outputCameraMatrix = GetWorldTransformMatrix();

	Matrix33 orientation = Matrix33(context.outputCameraMatrix);
	Vec3 position = context.outputCameraMatrix.GetTranslation();

	context.outputCameraMatrix.AddTranslation(orientation * sensorState.pose.position);
	context.outputCameraMatrix.SetRotation33(orientation * Matrix33(sensorState.pose.orientation));

	return true;
}