#include "StdAfx.h"
#include "PlayerComponent.h"
#include "SpawnPoint.h"
#include "CrySchematyc/Env/Elements/EnvComponent.h"
#include <CryRenderer/IRenderAuxGeom.h>

#include <CryNetwork/Rmi.h>
#include "FlashUI/FlashUI.h"
#include "ItemComponent.h"
#include "UISystem/HudComponent.h"
#include "BulletComponent.h"
#include "RagdollComponent.h"
#include "GameRules.h"

#define MOUSE_DELTA_TRESHOLD 0.0001f
CPlayerComponent *CPlayerComponent::pLocalPlayer = nullptr;

static void RegisterPlayer(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CPlayerComponent));
		// Functions
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterPlayer)

void CPlayerComponent::ReflectType(Schematyc::CTypeDesc<CPlayerComponent>& desc)
{
	desc.SetGUID("{CEA3997B-37C9-497F-ADF5-C52A6FF95AB1}"_cry_guid);
	desc.SetEditorCategory("Player");
	desc.SetLabel("Player component");
	desc.SetDescription("Component that defines player class and client properties with networking");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	//EDITOR MEMBERS
	desc.AddMember(&CPlayerComponent::sPlayerProperties, 'pprs', "PlayerProperties", "Player settings", "Properties of player class", SPlayerProperties());
}

CPlayerComponent::~CPlayerComponent()
{	
	if (gEnv->IsEditor())
		return;

	if (gEnv->bServer)
	{
		GetServer()->ServerRemovePlayer(this);
	}
}

void CPlayerComponent::Initialize()
{
	CreatePlayerShortName();
	pController = m_pEntity->GetOrCreateComponentClass<SCharacterControllerComponent>();
	pController->SetTransformMatrix(Matrix34::Create(Vec3(1.f), IDENTITY, Vec3(0, 0, 1.f)));
	pAnimations = m_pEntity->GetOrCreateComponentClass<SAdvancedAnimationComponent>();
	pAnimations->SetCharacterFile(sPlayerProperties.sAnimationProperties.sCharacterFileTP.value.c_str());
	pAnimations->SetMannequinAnimationDatabaseFile(sPlayerProperties.sAnimationProperties.sDatabaseFile.value.c_str());
	pAnimations->SetControllerDefinitionFile(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_controllerDefinitionPath);
	pAnimations->SetDefaultScopeContextName(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_contextName);
	pAnimations->SetAnimationDrivenMotion(sPlayerProperties.sAnimationProperties.bAnimationDrivenMotion);
	pAnimations->SetDefaultFragmentName(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
	pAnimations->LoadFromDisk();
	m_idleFragmentId = pAnimations->GetFragmentId(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
	m_walkFragmentId = pAnimations->GetFragmentId("Walk");
	m_rotateTagId = pAnimations->GetTagId("Rotate");
	m_weaponTagId = pAnimations->GetTagId("Weapon");
	pInventory = m_pEntity->CreateComponentClass<CInventoryComponent>();
	pAttachment = m_pEntity->CreateComponentClass<CAttachmentComponent>();
	Revive();
	m_pEntity->GetNetEntity()->EnableDelegatableAspect(eEA_Physics, false);
	InitServerMethods();
	InitClientMethods();
	//this is server heartbeat, which is method updated on every 300 miliseconds, which is used for things, that are not needed to be udpated so rapidly and every frame, it saves packets
	if (gEnv->bServer)
	{
		m_pEntity->SetTimer(87, 300);
		GetServer()->ServerAddPlayer(this);
	}
	//set up reset properties
	sPlayerStartupProperties = sPlayerProperties;
	qStartRotation = m_pEntity->GetWorldRotation();

	//below will show message that new player joined game
	//we don't need networking for this, cause this component will be automatically spawned on all teams anyway, and this will be calles
	//make sure it is not server, and pLocalPlayer makes it impossible to be called on THIS client, cause this pointer is initialized later
	if (!gEnv->bServer && pLocalPlayer)
	{
		SUIArguments args;
		args.AddArgument<string>(GetLogin());
		//GAMERULES this player team name
		args.AddArgument<string>("Red");
		GetUISystem()->JoinMessage(args);
	}
}

uint64 CPlayerComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_NET_BECOME_LOCAL_PLAYER) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED) | BIT64(ENTITY_EVENT_TIMER);
}

void CPlayerComponent::ProcessEvent(SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_START_GAME:
	{
		Revive();
	}
	break;
	case ENTITY_EVENT_NET_BECOME_LOCAL_PLAYER:
	{		
		LocalPlayerInitialize();
	}
	break;
	case ENTITY_EVENT_TIMER:
	{
		if (gEnv->bServer)
		{
			if (event.nParam[0] >= Timer_First && event.nParam[0] <= Timer_Last)
				GetServer()->GetGameRules()->TimerEvent(this, int(event.nParam[0]));
		}
		if (event.nParam[0] == 99)
		{
			sPlayerProperties.bUnlockCamera = false;
			sPlayerProperties.bCameraTimerSet = false;
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		SEntityUpdateContext* pCtx = (SEntityUpdateContext*)event.nParam[0];
		//Some things shouldn't be called when player is ragdolized
		if (IsPlayerAlive())
		{
			if (IEntity *pVehicleEntity = gEnv->pEntitySystem->GetEntity(sPlayerProperties.vehicle))
			{
				SVehicleComponent *pVehicleIn = pVehicleEntity->GetComponent<SVehicleComponent>();

				QuatTS attWorldAbsolute = pVehicleIn->GetAttachmentWorldAbsolute(GetEntityId());
				m_pEntity->SetPos(attWorldAbsolute.t);
				m_pEntity->SetRotation(attWorldAbsolute.q);
			}
			if (gEnv->bServer)
			{
				UpdateMovementRequest(pCtx->fFrameTime);
			}

			if (!gEnv->bServer)
			{
				ClientUpdate(pCtx->fFrameTime);
				UpdateLookDirectionRequest(pCtx->fFrameTime);
			}
		}
		if (gEnv->bServer)
			ServerUpdate(pCtx->fFrameTime);

		if (IsPlayerAlive())
		{
			UpdateAnimation(pCtx->fFrameTime);
		}
		if (pCamera)
		{
			UpdateCamera(pCtx->fFrameTime);
		}
	}
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (sPlayerProperties != sPrevPlayerProperties)
		{
			sPrevPlayerProperties = sPlayerProperties;
			pController->SetUp(sPlayerProperties.pPhys, sPlayerProperties.pMove);
			pAnimations->SetType(sPlayerProperties.sAnimationProperties.rType);
			pAnimations->SetCharacterFile(sPlayerProperties.sAnimationProperties.sCharacterFileTP.value.c_str());
			pAnimations->SetMannequinAnimationDatabaseFile(sPlayerProperties.sAnimationProperties.sDatabaseFile.value.c_str());
			pAnimations->SetControllerDefinitionFile(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_controllerDefinitionPath);
			pAnimations->SetDefaultScopeContextName(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_contextName);
			pAnimations->SetDefaultFragmentName(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
			pAnimations->SetAnimationDrivenMotion(sPlayerProperties.sAnimationProperties.bAnimationDrivenMotion);
			pAnimations->EnableGroundAlignment(sPlayerProperties.sAnimationProperties.bGroundAlignment);
			pAnimations->LoadFromDisk();
			m_idleFragmentId = pAnimations->GetFragmentId(sPlayerProperties.sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
			m_walkFragmentId = pAnimations->GetFragmentId("Walk");
			m_rotateTagId = pAnimations->GetTagId("Rotate");
			m_weaponTagId = pAnimations->GetTagId("Weapon");
			pAnimations->ResetCharacter();
			if (pCamera)
			{
				pCamera->EnableAutomaticActivation(sPlayerProperties.sCameraProperties.bActivateOnCreate);
				pCamera->SetNearPlane(sPlayerProperties.sCameraProperties.rNearPlane.value);
				pCamera->SetFieldOfView(sPlayerProperties.sCameraProperties.cFieldOfView);
				pCamera->Activate();
			}
		}
	}
	break;
	}
}

void CPlayerComponent::PlayerOnEvent(uint8 playerFlag)
{
	switch (playerFlag)
	{
		//HUD updates happen only on local player
	case PLAYER_EVENT_HUD_UPDATE_WEAPON_INFO:
	{
		if (bIsLocal)
			GetHudComponent()->UpdateWeaponInfo();
	}
	break;
	}
}

Vec3 CPlayerComponent::GetCameraForwardDir()
{
	return gEnv->pSystem->GetViewCamera().GetViewdir();
}

void CPlayerComponent::SetAccount(CAccountComponent * pAssociatedAccount)
{
	if (!pAssociatedAccount)
		return;

	pAccount = pAssociatedAccount; 
	sPlayerProperties.accountId = pAccount->GetEntityId();
	sPlayerProperties.accountIdComp = sPlayerProperties.accountId;
	ServerUpdatePlayerData();
}

Vec3 CPlayerComponent::GetAttachmentCurrentPosition(string attName)
{
	if (ICharacterInstance *pChar = GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attName))
			{
				return pAttachment->GetAttWorldAbsolute().t + Vec3(0.f, 0.f, 1.2f);
			}
		}
	}
	return ZERO;
}

void CPlayerComponent::Reset()
{	
	//clear inventory
	GetInventory()->ClearInventory();
	//
	sPlayerProperties = sPlayerStartupProperties;
	MoveToSpawnPoint();
	m_pEntity->SetRotation(qStartRotation);
	m_pEntity->Invisible(false);
	//we also need to make sure that is not ragdolized on the start of new round
	sPlayerProperties.bIsAlive = true;
	//also when we reset player, his deathcam must be disabled and array must be cleared out
	for (int i = 0; i < 32; i++)
	{
		//if (pTeamMateToFollow[i])
		{
		//	pTeamMateToFollow[i] = nullptr;
		}
	}

	//also update hud on local player
	if (bIsLocal)
	{
		GetHudComponent()->UpdateWeaponInfo();
		GetHudComponent()->UpdateCompleteProgressBars();
	}
}

void CPlayerComponent::Revive()
{
	GetEntity()->Hide(false);
	GetEntity()->SetWorldTM(Matrix34::Create(Vec3(1, 1, 1), IDENTITY, GetEntity()->GetWorldPos()));
	if (pAnimations && pController)
	{
		pAnimations->ResetCharacter();
		pController->Physicalize();
	}
	m_inputFlags = 0;
	m_mouseDeltaRotation = ZERO;
	m_lookOrientation = IDENTITY;
	m_mouseDeltaSmoothingFilter.Reset();
	m_activeFragmentId = FRAGMENT_ID_INVALID;
	m_horizontalAngularVelocity = 0.0f;
	m_averagedHorizontalAngularVelocity.Reset();
	if (pAnimations)
	{
		if (ICharacterInstance *pCharacter = pAnimations->GetCharacter())
		{
			m_cameraJointId = pCharacter->GetIDefaultSkeleton().GetJointIDByName("head");
		}
	}
	sPlayerProperties.Init();
}

void CPlayerComponent::MoveToSpawnPoint()
{
	if (gEnv->IsEditor())
		return;

	auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
	pEntityIterator->MoveFirst();

	while (!pEntityIterator->IsEnd())
	{
		IEntity *pEntity = pEntityIterator->Next();

		if (CSpawnPointComponent* pSpawner = pEntity->GetComponent<CSpawnPointComponent>())
		{
			if (GetTeamIndex() == Team_Lobby_01)
			{
				if (pSpawner->GetTeam() == TeamEnum::Team_01)
				{
					pSpawner->SpawnEntity(m_pEntity);
					break;
				}
			}
			else if (GetTeamIndex() == Team_Lobby_02)
			{
				if (pSpawner->GetTeam() == TeamEnum::Team_02)
				{
					pSpawner->SpawnEntity(m_pEntity);
					break;
				}
			}
		}
	}
	if (!gEnv->bServer)
	{
		NetMarkAspectsDirty(kMovementAspect);
	}
}

void CPlayerComponent::Ragdolize(Vec3 pushDir)
{
	if (gEnv->IsEditor())
		return;
	//SEntitySpawnParams params;
	//params.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
	//params.sName = "RAGDOLL";
	//params.nFlags |= ENTITY_FLAG_NEVER_NETWORK_STATIC;
	//params.vPosition = m_pEntity->GetWorldPos();
	//params.qRotation = m_pEntity->GetWorldRotation();
	//IEntity *pRagdoll = gEnv->pEntitySystem->SpawnEntity(params);
	//pRagdoll->GetOrCreateComponentClass<CRagdollComponent>();
}

CGameTeam *CPlayerComponent::GetTeam()
{
	if (pAccount)
		return pAccount->GetTeam();

	return nullptr;
}

int CPlayerComponent::GetTeamIndex()
{
	if (pAccount)
		return pAccount->GetTeamIndex();

	return -1;
}

int CPlayerComponent::GetIndexInTeam()
{
	if (pAccount)
		return pAccount->GetIndexInTeam();

	return -1;
}

void CPlayerComponent::SetAnimationTagID(TagID tag, bool bSet)
{
	pAnimations->SetTagWithId(tag, bSet);
}

string CPlayerComponent::GetLogin()
{
	return string(m_pEntity->GetName());
}

void CPlayerComponent::Aim(float x, float y, float z, float nearPlane)
{
	fAimOffset.x = x; fAimOffset.y = y; fAimOffset.z = z; sPlayerProperties.sCameraProperties.rNearPlane = nearPlane; pCamera->SetNearPlane(nearPlane);
}

Vec3 CPlayerComponent::GetCameraLocalJointPos()
{
	if (ICharacterInstance *pCharacter = pAnimations->GetCharacter())
	{
		const QuatT &cameraOrientation = pCharacter->GetISkeletonPose()->GetAbsJointByID(m_cameraJointId);
		return cameraOrientation.t;
	}
	return Vec3();
}

void CPlayerComponent::ApplyRecoil(float strength)
{
	m_mouseDeltaRotation = m_mouseDeltaSmoothingFilter.Push(m_mouseDeltaRotation).Get();
	Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_lookOrientation));
	ypr.y += strength;
	m_lookOrientation = Quat(CCamera::CreateOrientationYPR(ypr));
	m_mouseDeltaRotation = ZERO;
}

void CPlayerComponent::HandleInputFlagChange(TInputFlags flags, int activationMode, EInputFlagType type)
{
	switch (type)
	{
	case EInputFlagType::Hold:
	{
		if (activationMode == eIS_Released)
		{
			m_inputFlags &= ~flags;
		}
		else
		{
			m_inputFlags |= flags;
		}
	}
	break;
	case EInputFlagType::Toggle:
	{
		if (activationMode == eIS_Released)
		{
			m_inputFlags ^= flags;
		}
	}
	break;
	}

	if (!gEnv->bServer)
	{
		NetMarkAspectsDirty(kMovementAspect);
	}
}

void CPlayerComponent::CreatePlayerShortName()
{
	string sClassName = m_pEntity->GetClass()->GetName();
	sClassName.erase(0, sClassName.find_last_of(":") + 1);
	string sClassNameProper = sClassName;
	sClassNameProper.erase(0, 7);
	sClassName.erase(6, sClassName.length());
	string sFirst;
	sFirst.SetAt(0, sClassNameProper[0]);
	sFirst.MakeUpper();
	sClassNameProper.erase(0, 1);
	sClassNameProper = sFirst + sClassNameProper;

	sClassShortName = sClassNameProper;
	CryLogAlways("Creating short name for player : %s", sClassNameProper);
}

void CPlayerComponent::ShowPickUpMessage(string name)
{
	Vec2 screenMid = { gEnv->pRenderer->GetWidth() / 2.f, gEnv->pRenderer->GetHeight() / 2.f };
	ColorF pfWhiteColor = { 1.f, 1.f, 1.f, 1.f };
	if (!gEnv->IsEditor())
	{
		string sLabel = sPlayerProperties.pTargetItem->GetPickupLabel(this);
		gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenMid.x, screenMid.y, 1.5f, pfWhiteColor, true, sLabel);
	}
	else
	{
		string sLabel = "ITEMS ARE NOT PICKABLE IN EDITOR";
		gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenMid.x, screenMid.y, 1.5f, pfWhiteColor, true, sLabel);
	}
	//label with ammo type that ammo pack contains
	if (CAmmoPackComponent *pAmmoPack = sPlayerProperties.pTargetItem->IsAmmoPack())
	{
		IEntity *pItemEntity = pAmmoPack->GetEntity();
		AABB bbox;
		pItemEntity->GetWorldBounds(bbox);
		float ammoPackHeight = bbox.GetSize().z;
		Vec3 screenPos(ZERO);
		gEnv->pRenderer->ProjectToScreen(pItemEntity->GetWorldPos().x, pItemEntity->GetWorldPos().y, pItemEntity->GetWorldPos().z + ammoPackHeight, &screenPos.x, &screenPos.y, &screenPos.z);
		screenPos.x = screenPos.x * 0.01f * gEnv->pRenderer->GetWidth();
		screenPos.y = screenPos.y * 0.01f * gEnv->pRenderer->GetHeight();
		ColorF pfWhiteColor = { 1.f, 1.f, 1.f, 1.f };

		string sLabel = "Ammo pack with bullets for " + pAmmoPack->GetBulletShortName();
		gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenPos.x, screenPos.y, 1.5f, pfWhiteColor, true, sLabel);
	}
}
CPlayerUIComponent * CPlayerComponent::GetUISystem()
{
	return GetLocalPlayer()->pUI;
}
SPlayerProperties * CPlayerComponent::GetProperties()
{
	return &sPlayerProperties;
}