#include "StdAfx.h"
#include "EntityAudioComponent.h"

void SEntityAudioComponent::Initialize()
{
	pAudioComponent = m_pEntity->CreateComponent<IEntityAudioComponent>();
	CRY_ASSERT(auxObjectId == CryAudio::InvalidAuxObjectId);
	auxObjectId = CryAudio::DefaultAuxObjectId;
}

void SEntityAudioComponent::Play(int index)
{
	if (pAudioComponent != nullptr && sTrigger[index] != CryAudio::InvalidControlId)
	{
		CryAudio::SRequestUserData const userData(CryAudio::ERequestFlags::DoneCallbackOnExternalThread | CryAudio::ERequestFlags::DoneCallbackOnExternalThread, this);
		pAudioComponent->ExecuteTrigger(sTrigger[index], auxObjectId, userData);
	}
}

void SEntityAudioComponent::Stop(int index)
{
	if (pAudioComponent != nullptr && sTrigger[index] != CryAudio::InvalidControlId)
	{
		CryAudio::SRequestUserData const userData(CryAudio::ERequestFlags::DoneCallbackOnExternalThread | CryAudio::ERequestFlags::DoneCallbackOnExternalThread, this);
		pAudioComponent->StopTrigger(sTrigger[index], auxObjectId, userData);
	}
}

void SEntityAudioComponent::SetNameAndId(string n1, string n2, string n3, string n4, string n5)
{
	sTriggerName[0] = n1;
	sTriggerName[1] = n2;
	sTriggerName[2] = n3;
	sTriggerName[3] = n4;
	sTriggerName[4] = n5;
	sTrigger[0] = sTriggerName[0].empty() ? CryAudio::InvalidControlId : CryAudio::StringToId(sTriggerName[0].c_str());
	sTrigger[1] = sTriggerName[1].empty() ? CryAudio::InvalidControlId : CryAudio::StringToId(sTriggerName[1].c_str());
	sTrigger[2] = sTriggerName[2].empty() ? CryAudio::InvalidControlId : CryAudio::StringToId(sTriggerName[2].c_str());
	sTrigger[3] = sTriggerName[3].empty() ? CryAudio::InvalidControlId : CryAudio::StringToId(sTriggerName[3].c_str());
	sTrigger[4] = sTriggerName[4].empty() ? CryAudio::InvalidControlId : CryAudio::StringToId(sTriggerName[4].c_str());
}
