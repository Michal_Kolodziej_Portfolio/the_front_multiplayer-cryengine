#include "PlayerComponent.h"
#include "RagdollComponent.h"
#include "TestComponent.h"
#include "GameRules.h"

void CPlayerComponent::InitServerMethods()
{
	SRmi<RMI_WRAP(&CPlayerComponent::ServerAmmoPackManagement)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&CPlayerComponent::ServerUse)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&CPlayerComponent::ServerCarry)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&CPlayerComponent::ServerStopVisiting)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
}

bool CPlayerComponent::ServerAmmoPackManagement(AmmoPackManagementParams && p, INetChannel *)
{
	if (CAmmoPackComponent *pAmmoPack = GetVisitedAmmoPack())
	{				
		if (p.bDeposit)
		{
			//deposit of bullets will remove as many bullets as player requested (unless he has less); networking handled by inventory
			const int bulletsRemoved = GetInventory()->ServerAddRemoveFreeBulletAmount(pAmmoPack, (-p.bulletCount));
			//now when we have possible bullets on the player's inventory side, we can add them to ammo pack, it will also return possible amount for itself; it will automatically serialize values to clients
			const int bulletsAdded = pAmmoPack->ServerAddRemoveAmmo(bulletsRemoved);
			//now we should check if added bullets are less than removed, we will give those back to player
			//if bullets difference is positive, we have to give back this difference
			const int bulletsDifference = bulletsRemoved - bulletsAdded;
			if (bulletsDifference > 0)
				GetInventory()->ServerAddRemoveFreeBulletAmount(pAmmoPack, bulletsDifference);
		}
		else
		{
			//taking bullets from ammo pack will do opposite proccess to deposit - remove from ammo pack and add to player
			//removing bullets from ammo pack will return max amount that succeeded for it
			const int bulletsRemoved = pAmmoPack->ServerAddRemoveAmmo((-p.bulletCount));
			//now when we have real removed bullets from ammo pack, we can add them to player
			const int bulletsAdded = GetInventory()->ServerAddRemoveFreeBulletAmount(pAmmoPack, bulletsRemoved);
			//now we should check if added bullets are less than removed, we will give those back to ammo pack
			//if bullets difference is positive, we have to give back this difference
			const int bulletsDifference = bulletsRemoved - bulletsAdded;
			if (bulletsDifference > 0)
				pAmmoPack->ServerAddRemoveAmmo(bulletsDifference);
		}
	}
	return true;
}
bool CPlayerComponent::ServerStopVisiting(NoParams && p, INetChannel *)
{
	ServerSetVisitedAmmoPack(0);
	return true;
}

void CPlayerComponent::ServerUpdate(float frameTime)
{
	ServerRegeneration();
	//check distance from the ground when is not on the ground
	if (pController)
	{
		if (!pController->IsOnGround())
		{
			if (sPlayerProperties.lastGroundContactPointSaved == ZERO)
			{
				sPlayerProperties.lastGroundContactPointSaved = sPlayerProperties.lastGroundContactPoint;
			}
		}
		else
		{
			//if last ground contact point wasn't zero, it means that we have new ground contact now and we just came back from the air
			if (sPlayerProperties.lastGroundContactPointSaved != ZERO)
			{
				//at the moment we will just check the z axis difference, but in the future we will raycast or just calculate from pitagoras
				const float heightDifference = sPlayerProperties.lastGroundContactPointSaved.z - m_pEntity->GetWorldPos().z;
				sPlayerProperties.lastGroundContactPointSaved = ZERO;
				//if height difference is positive, it means we have fallen down 
				if (heightDifference >= 6.f)
				{
					const float hdRatio = heightDifference - 5.f;
					const float damage = hdRatio * 15.f;
					ServerOnHit(damage);
				}
			}
			//this is updated on every frame to check the last ground contact point and later compare it to the new contack point
			sPlayerProperties.lastGroundContactPoint = m_pEntity->GetWorldPos();
		}
	}
}

void CPlayerComponent::ServerOnHit(CBulletComponent * pBullet, CPlayerComponent * pAttacker, Vec3 vPushDir, Vec3 vAttackerPosOnShoot)
{
	GetServer()->GetGameRules()->Hit(pBullet, pAttacker, this);
}

void CPlayerComponent::ServerOnHit(float damage)
{
	GetServer()->GetGameRules()->Hit(damage, this);
}

bool CPlayerComponent::ServerUse(NoParams && p, INetChannel *)
{
	//firstly check if player is inside the vehicle, and if he is, then ignore the rest instructions
	if (IEntity *pEntity = gEnv->pEntitySystem->GetEntity(sPlayerProperties.vehicle))
	{
		//leave the vehicle then
		SVehicleComponent *vehiclePt = pEntity->GetComponent<SVehicleComponent>();
		vehiclePt->Leave(GetEntityId());
		//return here to prevent next instructions from happening
		return true;
	}
	// we will check what item is player targeting and then call pickup method on specific item
	if (sPlayerProperties.pTargetItem)
	{
		//all item pickup functionality is handled by item itself
		sPlayerProperties.pTargetItem->ServerPickup(this);
		//nullify target item to prevent fake double pickups(they wouldn't happen anyways cause once they are picked up, they're not pickable anymore)
		sPlayerProperties.pTargetItem = nullptr;
	}

	return true;
}

bool CPlayerComponent::ServerCarry(NoParams && p, INetChannel *)
{				
	//player will be able to carry certain items
	if (sPlayerProperties.pTargetItem)
		sPlayerProperties.pTargetItem->ServerCarry(this);
	
	return true;
}

void CPlayerComponent::ServerSetVisitedAmmoPack(EntityId packId)
{
	sPlayerProperties.visitedAmmoPackId = packId;
	sPlayerProperties.visitedAmmoPackIdComp = packId;
	ServerUpdatePlayerData();
}

void CPlayerComponent::ServerRegeneration()
{
	sPlayerProperties.Refresher();

	//serialize this info
	ServerUpdateVitalData();
}

void CPlayerComponent::ServerReset()
{
	sPlayerProperties = sPlayerStartupProperties;
	MoveToSpawnPoint();

	GetInventory()->ServerReset();

	ServerUpdateGameplayData();
	ServerUpdatePlayerData();
	ServerUpdateVitalData();
}
