/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Spawn point must have specified team. It is accessed from editor. 

--------------------------------------------------------------------------------- */

#pragma once
#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntity.h>

////////////////////////////////////////////////////////
// Spawn point
////////////////////////////////////////////////////////
enum class TeamEnum
{
	Team_01 = 1, 
	Team_02
};
static void ReflectType(Schematyc::CTypeDesc<TeamEnum>& desc)
{
	desc.SetGUID("{DD763B30-A646-4822-B504-92B5A93BAF27}"_cry_guid);
	desc.SetLabel("Team");
	desc.SetDescription("Team enum");
	desc.AddConstant(TeamEnum::Team_01, "Team 1", "First Team");
	desc.AddConstant(TeamEnum::Team_02, "Team 2", "Second Team");
}

class CSpawnPointComponent : public IEntityComponent
{
public:
	CSpawnPointComponent() = default;
	virtual ~CSpawnPointComponent() {}
	virtual void  ProcessEvent(SEntityEvent& event);
	virtual uint64 GetEventMask() const override;
	// Reflect type to set a unique identifier for this component
	// and provide additional information to expose it in the sandbox
	static void ReflectType(Schematyc::CTypeDesc<CSpawnPointComponent>& desc);
	TeamEnum GetTeam() { return iTeam; }

public:
	void SpawnEntity(IEntity* otherEntity);
private:
	TeamEnum iTeam;
};
