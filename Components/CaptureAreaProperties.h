/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores all capture area properties exposed and unexposed for editor

--------------------------------------------------------------------------------- */

#pragma once

enum class AreaShape
{
	AREA_TYPE_CYLINDER = 0,
	AREA_TYPE_BOX
};
static void ReflectType(Schematyc::CTypeDesc<AreaShape>& desc)
{
	desc.SetGUID("{133BB257-DC36-4840-9060-B73FD69DAF34}"_cry_guid);
	desc.SetLabel("Area shape");
	desc.SetDescription("Available area types");
	desc.AddConstant(AreaShape::AREA_TYPE_BOX, "BoxArea", "Box shape");
	desc.AddConstant(AreaShape::AREA_TYPE_CYLINDER, "CylinderArea", "Circle shape");
}
struct SCaptureAreaProperties
{
	inline bool operator==(const SCaptureAreaProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SCaptureAreaProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	AreaShape m_AreaShape;
	float fRadius;
	//height is only for cylinder
	float fHeight;
	//box dimensions minimum is determined by entity placement
	Vec3 vMax;
};
static void ReflectType(Schematyc::CTypeDesc<SCaptureAreaProperties>& desc)
{
	desc.SetGUID("{A4560E6D-9B35-403C-8450-EE761341A6AA}"_cry_guid);
	desc.SetLabel("Capture area properties");
	desc.SetDescription("Capture area properties");
	desc.AddMember(&SCaptureAreaProperties::m_AreaShape, 'cas', "AreaShape", "Area shape", "Capture area shape", AreaShape());
	desc.AddMember(&SCaptureAreaProperties::fRadius, 'frad', "AreaRadius", "Area radius", "Capture area radius", 5.f);
	desc.AddMember(&SCaptureAreaProperties::fHeight, 'fhei', "AreaHeight", "Area height(circle shape only)", "Capture area height", 1.f);
	desc.AddMember(&SCaptureAreaProperties::vMax, 'vmax', "Maximum", "Box size", "Max", Vec3());
}