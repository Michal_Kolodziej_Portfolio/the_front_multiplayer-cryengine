/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : It handles fire modes for weapons. Switching them and each mode usage. Every projectile weapon should have it.

--------------------------------------------------------------------------------- */

#pragma once
#include "WeaponComponent.h"
#include "PlayerComponent.h"
#include "CryMath/Random.h"

class CFireModeComponent final : public IEntityComponent
{
public:
	enum EFireModes
	{
		FM_First = 1 << 0,
		FM_Single = FM_First,
		FM_Serie            = 1 << 1,
		FM_Auto             = 1 << 2,
		FM_Last = FM_Auto
	};
	CFireModeComponent::CFireModeComponent(CWeaponComponent *pweapon, int efiremodesAv)
	{
		//fire mode component is unseparatable part of each projectile weapon, so it sticks with certain weapon for its lifetime
		SetAvailableModes(efiremodesAv);
		iCurrentMode = FM_First;
		NextMode();
		pWeapon = pweapon;
	}
	virtual ~CFireModeComponent() {}
	static void ReflectType(Schematyc::CTypeDesc<CFireModeComponent>& desc)
	{
		desc.SetGUID("{16310D9E-00F4-41D2-8096-EF8010ED2F3A}"_cry_guid);
	}
	//FIRE MODE HANDLING
	virtual bool CanShot()
	{
		//return true;
		//this is local player method; it does only checks that are possible on local player which doesn't include bullet count
		bool serieCond = true;
		if (IsSerie())
			serieCond = pWeapon->sWeaponProperties.iSerieCur < pWeapon->sWeaponProperties.iSerieMax;

		CAmmoClipComponent *pAmmoClip = pWeapon->GetCurrentAmmoClip();
		bool ammoCond = false;
		if (pAmmoClip)
		{
			ammoCond = !pAmmoClip->IsEmpty();

		}

		return bBulletReady && serieCond && ammoCond;
	}
	void StartFire()
	{
		//pretty self-explainatory, different logic for various fire modes
		if (CanShot())
		{
			//this is flag that handles bullet reload (not weapon reload), which applies shot speed 
			//each fire mode must mark bullet as unready on the beginning, so we can do it here for all
			bBulletReady = false;
			if (IsSingle())
			{
				//single fire mode only launches bullet, it does not need special logic; only way to reload bullet is to release trigger
				pWeapon->ServerLaunch(true);
				return;
			}
			else if (IsSerie())
			{
				//serie fire mode allows shooter to shoot in short series only;
				//amount of shots in one serie for this weapon is defined in schematyc
				//series fire mode must have this special logic, that allows it to actually count number of bullets possible for one full serie
				pWeapon->sWeaponProperties.iSerieCur += 1;
			}
			//difference between serie and automatic mode you can see above; 
			//that's why we don't need IsAuto here, because the rest of logic for serie and auto is the same
			pWeapon->ServerLaunch(true);
			//this is calculation done for the sake of randomizing shots from weapon
			//I noticed that when no randomizing is applied, weapon sounds too "perfect", unnatural, so here we make spaces between actual shots a little bit more random (80%)
			int fullTime = int((500000 / pWeapon->sWeaponProperties.fSpeed) + 0.5);
			int percentage = int(fullTime * 0.8);
			int randomTime = cry_random(percentage, fullTime);
			//timer that resets bullet and allows for next shoot without releasing trigger
			GetEntity()->SetTimer(27, randomTime);
		}
		//this happend when we try to shoot when no bullet is ready or when serie is over
		else if (bBulletReady)
		{
			bBulletReady = false;
			//fire dry sound
			pWeapon->ServerLaunch(false);
		}
	}
	void StopFire()
	{
		//stop firing has to have the same calculation for randomizing, to keep timer naturally stopping the fire action
		int fullTime = int((500000 / pWeapon->sWeaponProperties.fSpeed) + 0.5);
		int percentage = int(fullTime * 0.8);
		int randomTime = cry_random(percentage, fullTime);
		GetEntity()->SetTimer(27, randomTime);
	}
	//QUEUE
	void SetAvailableModes(int modeflags)
	{
		//set possible fire modes for this weapon
		if (modeflags){
			iAvailableModes = modeflags;
		}
	}
	void SetCurrentMode(int mode) { iCurrentMode = mode; }
	int GetCurrentMode() { return iCurrentMode; }
	void NextMode()
	{
		//simple switching between modes

		for (int i = iCurrentMode << 1; i != iCurrentMode; i <<= 1) {

			//rap around
			if (i > FM_Last) {
				i = FM_First;
			}

			if (iAvailableModes & i) {
				iCurrentMode = i;
				break;
			}

		}

	}
	bool IsSingle() { bool bOk; (iCurrentMode & FM_Single) ? bOk = true : bOk = false; return bOk; }
	bool IsSerie() { bool bOk; (iCurrentMode & FM_Serie) ? bOk = true : bOk = false; return bOk; }
	bool IsAuto() { bool bOk; (iCurrentMode & FM_Auto) ? bOk = true : bOk = false; return bOk; }
	//VARIABLES
	int iCurrentMode = FM_First;
	int iAvailableModes = 0;
	bool bBulletReady = true;
	CWeaponComponent *pWeapon;
};

