/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores all weapon properties exposed and unexposed for editor

--------------------------------------------------------------------------------- */

#pragma once
#include "FuryResources.h"
#include "Components/EntityAudioComponent.h"

class CFireModeComponent;

struct SAudioTriggerProperties
{
	void Serialize(Serialization::IArchive& archive);
	bool operator==(SAudioTriggerProperties const& other) const { return m_name == other.m_name; }

	CryAudio::ControlId m_id = CryAudio::InvalidControlId;
	string              m_name;
};
static void ReflectType(Schematyc::CTypeDesc<SAudioTriggerProperties>& desc)
{
	desc.SetGUID("{DBAED987-B2F8-4179-B9F4-61098CE4DDBA}"_cry_guid);
}
inline void SAudioTriggerProperties::Serialize(Serialization::IArchive& archive)
{
	archive(Serialization::AudioTrigger<string>(m_name), "triggerName", "^");

	if (archive.isInput())
	{
		m_id = CryAudio::StringToId(m_name.c_str());
	}
}
//
enum class EFireModes
{
	FM_Single = 1 << 0,
	FM_Serie  = 1 << 1,
	FM_Auto   = 1 << 2,
	FM_SingleAndSerie = FM_Single | FM_Serie,
	FM_SingleAndAuto = FM_Single | FM_Auto,
	FM_SerieAndAuto = FM_Serie | FM_Auto,
	FM_All = FM_Single | FM_Serie | FM_Auto
};
static void ReflectType(Schematyc::CTypeDesc<EFireModes>& desc)
{
	desc.SetGUID("{3CAB0717-FC90-4033-9BFE-A6B8468FFB8D}"_cry_guid);
	desc.SetLabel("FireModes");
	desc.SetDescription("Fire mode types - FURY");
	desc.AddConstant(EFireModes::FM_Single, "Single", "Single mode");
	desc.AddConstant(EFireModes::FM_Serie, "Serie", "Series mode");
	desc.AddConstant(EFireModes::FM_Auto, "Auto", "Automatic mode");
	desc.AddConstant(EFireModes::FM_SingleAndSerie, "SingleSerie", "Single/Serie mode");
	desc.AddConstant(EFireModes::FM_SingleAndAuto, "SingleAuto", "Single/Auto mode");
	desc.AddConstant(EFireModes::FM_SerieAndAuto, "SerieAuto", "Serie/Auto mode");
	desc.AddConstant(EFireModes::FM_All, "All", "All modes available");
}

struct SWeaponProperties
{
	inline bool operator==(const SWeaponProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SWeaponProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	SAudioTriggerProperties sFireTrigger;
	SAudioTriggerProperties sReloadTrigger;
	SAudioTriggerProperties sDryFireTrigger;
	Schematyc::EntityClassName sAmmoClipName;
	float fSpeed;
	EFireModes eFireMode;
	int iSerieMax;
	//NON EDITOR
	float fRecoilAdd = 0.f;
	int iSerieCur = 0;
	EntityId currentAmmoClip = 0;
	EntityId currentAmmoClipComp = 0;
	bool bFire = false;
};
static void ReflectType(Schematyc::CTypeDesc<SWeaponProperties>& desc)
{
	desc.SetGUID("{C0AE7554-BA14-4050-90D8-FD70827CA694}"_cry_guid);
	desc.SetLabel("Weapon properties");
	desc.SetDescription("Weapon properties - FURY");
	desc.AddMember(&SWeaponProperties::sFireTrigger, 'ftr', "FireTrigger", "Fire sound", "Sound triggered on fire", SAudioTriggerProperties());
	desc.AddMember(&SWeaponProperties::sDryFireTrigger, 'dftr', "DrrFireTrigger", "Dry fire sound", "Sound triggered on fire with no ammo", SAudioTriggerProperties());
	desc.AddMember(&SWeaponProperties::sReloadTrigger, 'rtr', "ReloadTrigger", "Reload sound", "Sound triggered on reload", SAudioTriggerProperties());
	desc.AddMember(&SWeaponProperties::sAmmoClipName, 'mnam', "AmmoClipName", "AmmoClip type", "Type of the magazine", "");
	desc.AddMember(&SWeaponProperties::fSpeed, 'spd', "ReloadSpeed", "Reload speed", "Reload speed for automatic modes", 0.f);
	desc.AddMember(&SWeaponProperties::eFireMode, 'efrm', "FireMode", "Fire modes", "Choose available firemodes for this weapon", EFireModes());
	desc.AddMember(&SWeaponProperties::iSerieMax, 'iser', "SerieMax", "Serie length(in bullets)", "Length of single series in serie fire mode", 0);
}
/////////////////////////////////////////////////////////////COMPONENT////////////////////////////////////////////////////////////////////////