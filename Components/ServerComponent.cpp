#include "StdAfx.h"
#include "ServerComponent.h"
#include "CryEntitySystem/IEntity.h"
#include "FlashUI/FlashUI.h"
#include "AccountComponent.h"
#include "UISystem/LoginMenu.h"
#include "UISystem/MainMenu.h"
#include "UISystem/LobbyMenu.h"
#include "GameTeam.h"
#include "PlayerUIComponent.h"
#include "GameRules.h"
#include "GamePlugin.h"
#include "GameConsoleVariables.h"

static void RegisterServer(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(SServerComponent));
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterServer)

SServerComponent *SServerComponent::SERVER = nullptr;

void SServerComponent::Initialize()
{
	SERVER = this; 

	if (gEnv->bServer)
		pRules = new SGameRules();

	pGameTeam.push_back(m_pEntity->CreateComponentClass<CGameTeam>());
	pGameTeam.push_back(m_pEntity->CreateComponentClass<CGameTeam>());
	pGameTeam.push_back(m_pEntity->CreateComponentClass<CGameTeam>());

	if (gEnv->bServer)
	{
		if (XmlNodeRef teamSetup = gEnv->pSystem->LoadXmlFromFile("ServerDatabase/TeamSetup.xml"))
		{
			for (int index = 0; index < LOBBY_TEAM_COUNT; index++)
			{
				if (XmlNodeRef team = teamSetup->getChild(index))
				{
					string name = team->getAttr("name");
					string s_size = team->getAttr("size");
					int i_size = atoi(s_size);

					pGameTeam[index]->Set(name, index);
				}
			}
		}
	}
	m_pEntity->GetNetEntity()->BindToNetwork();

	SRmi<RMI_WRAP(&SServerComponent::ServerAttemptLogin)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ClientAccess)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ServerAddToLobby)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ServerMoveToTeam)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ServerJoinGame)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ClientMessage)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ServerCommand)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ServerCVarString)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&SServerComponent::ServerCVarInt)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
}

void SServerComponent::ReflectType(Schematyc::CTypeDesc<SServerComponent>& desc)
{
	desc.SetGUID("{B99AAC02-8E40-488C-812F-13869D4F9D39}"_cry_guid);
	desc.SetEditorCategory("DO NOT USE");
	desc.SetLabel("Server");
	desc.SetDescription("Server component");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
}

uint64 SServerComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_TIMER);
}

void SServerComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		if (pRules)
			pRules->Update();
	}
	break;
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == 22)
		{
		}
	}
	break;
	}
}

bool SServerComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kServerDynamicData)
	{
		ser.BeginGroup("Dynamic");

		ser.EndGroup();
	}
	if (aspect == kServerRegularData)
	{
		ser.BeginGroup("Regular");

		ser.EndGroup();
	}
	if (aspect == kServerStaticData)
	{
		ser.BeginGroup("Static");

		ser.EndGroup();
	}
	return true;
}

void SServerComponent::ClientAttemptLogin(string requestedLogin, string password, EntityId id)
{
	SRmi<RMI_WRAP(&SServerComponent::ServerAttemptLogin)>::InvokeOnServer(this, LoginParamsWithId{ requestedLogin, password, false, id, id });
}

bool SServerComponent::ClientAccess(LoginParamsWithId && p, INetChannel *)
{
	if (p.access)
	{
		GetLocalAccount()->GetLoginMenu()->Hide();
		GetLocalAccount()->GetMainMenu()->Show();
	}
	else
		GetLocalAccount()->GetLoginMenu()->NoAccessShow();

	return true;
}

void SServerComponent::ClientJoinLobby(string player_class)
{
	SRmi<RMI_WRAP(&SServerComponent::ServerAddToLobby)>::InvokeOnServer(this, EntityIdWithString{ pLocalAccount->GetEntityId(), pLocalAccount->GetEntityId(), player_class });
}

bool SServerComponent::ServerAddToLobby(EntityIdWithString && p, INetChannel *)
{
	IEntity *pEntity = Server_GetEntityById(p.entityId, p.entityIdComp);
	if (!pEntity)
		return true;

	CAccountComponent *pRequestorAccount = pEntity->GetComponent<CAccountComponent>();
	pRequestorAccount->SetPlayerClass(p.str);
	GetTeam(Team_Lobby_Waiting)->AddAccount(pRequestorAccount);
	return true;
}

void SServerComponent::ClientMoveToTeam(int team)
{
	if (team < 0)
		return;

	SRmi<RMI_WRAP(&SServerComponent::ServerMoveToTeam)>::InvokeOnServer(this, TeamParams{ pLocalAccount->GetEntityId(), pLocalAccount->GetEntityId(), team });
}

bool SServerComponent::ServerMoveToTeam(TeamParams && p, INetChannel *)
{
	IEntity *pEntity = Server_GetEntityById(p.entityId, p.entityIdComp);
	if (p.team < 0 || !pEntity)
		return true;

	CAccountComponent *pRequestingAccount = pEntity->GetComponent<CAccountComponent>();

	GetTeam(pRequestingAccount->GetTeamIndex())->RemoveAccount(pRequestingAccount);
	GetTeam(p.team)->AddAccount(pRequestingAccount);
	return true;
}

void SServerComponent::ClientJoinGame()
{
	SRmi<RMI_WRAP(&SServerComponent::ServerJoinGame)>::InvokeOnServer(this, EntityIdPass{ pLocalAccount->GetEntityId(), pLocalAccount->GetEntityId() });
}

bool SServerComponent::ServerJoinGame(EntityIdPass && p, INetChannel *)
{
	IEntity *pEntity = Server_GetEntityById(p.entityId, p.entityIdComp);
	if (!pEntity)
		return true;

	CAccountComponent *pRequestorAccount = pEntity->GetComponent<CAccountComponent>();

	SEntitySpawnParams params;
	params.pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(pRequestorAccount->GetPlayerClass());
	params.sName = pRequestorAccount->GetLogin();
	params.nFlags |= ENTITY_FLAG_NEVER_NETWORK_STATIC;
	IEntity *pNewPlayerEntity = gEnv->pEntitySystem->SpawnEntity(params);
	INetChannel *net = gEnv->pGameFramework->GetNetChannel(pEntity->GetNetEntity()->GetChannelId());
	gEnv->pNetContext->DelegateAuthority(pNewPlayerEntity->GetId(), net);
	pNewPlayerEntity->GetNetEntity()->SetChannelId(pEntity->GetNetEntity()->GetChannelId());
	pNewPlayerEntity->GetNetEntity()->BindToNetwork();

	Server_AddPlayer(pNewPlayerEntity->GetNetEntity()->GetChannelId(), pNewPlayerEntity->GetId());

	if (CPlayerComponent *player = pNewPlayerEntity->GetComponent<CPlayerComponent>())
	{
		CryLogAlways("%s JOINED THE GAME", pRequestorAccount->GetLogin());
		pRequestorAccount->SetPlayer(player);
		player->SetAccount(pRequestorAccount);
		player->MoveToSpawnPoint();
	}
	return true;
}

void SServerComponent::ServerKillDeath(CPlayerComponent * pAttacker, CPlayerComponent * pVictim)
{
	if (!pAttacker || !pVictim)
		return;

	const string attackerName = pAttacker->GetEntity()->GetName();
	const string victimName = pVictim->GetEntity()->GetName();
	const int attackerTeam = pAttacker->GetTeamIndex();
	const int victimTeam = pVictim->GetTeamIndex();

	SRmi<RMI_WRAP(&SServerComponent::ClientMessage)>::InvokeOnAllClients(this, MessageParams{ "Kill", attackerName, victimName, attackerTeam, victimTeam });
}

bool SServerComponent::ClientMessage(MessageParams && p, INetChannel *)
{
	if (p.message == "Kill")
	{
		if (pLocalPlayer)
		{
			const bool killerIsMyTeam = (p.int1 == pLocalPlayer->GetTeamIndex());
			const bool victimIsMyTeam = (p.int2 == pLocalPlayer->GetTeamIndex());

			SUIArguments args;
			args.AddArgument<string>(p.str1);
			args.AddArgument<bool>(killerIsMyTeam);
			args.AddArgument<string>(p.str2);
			args.AddArgument<bool>(victimIsMyTeam);
			pLocalPlayer->GetUISystem()->KillMessage(args);
		}
	}
	return true;
}

bool SServerComponent::ServerAttemptLogin(LoginParamsWithId&& p, INetChannel *)
{
	IEntity *pEntity = Server_GetEntityById(p.entityId, p.entityIdComp);
	if (!pEntity)
		return true;

	bool bAccessGranted = false;
	//server accounts database is currently stored in xml
	XmlNodeRef accounts = gEnv->pSystem->LoadXmlFromFile("ServerDatabase/Accounts.xml");
	if (accounts)
	{
		for (int i = 0; i < accounts->getChildCount(); i++)
		{
			if (XmlNodeRef child = accounts->getChild(i))
			{
				//check if login and password is found in database
				if ((child->getAttr("login") == p.login) && (child->getAttr("pass") == p.password))
				{
					CryLogAlways("CLIENT SUCCESFULY LOGGING INTO ACCOUNT %s", p.login);
					bAccessGranted = true;
					break;
				}
			}
		}
	}
	if (!bAccessGranted)
		CryLogAlways("CLIENT TRIED TO ENTER LOGIN %s AND PASSWORD %s, BUT NO ACCOUNT WAS FOUND FOR IT", p.login, p.password);
	//if access is granted, we should set up login for this account 
	else if (CAccountComponent *pAccount = pEntity->GetComponent<CAccountComponent>())
		pAccount->SetLogin(p.login);

	SRmi<RMI_WRAP(&SServerComponent::ClientAccess)>::InvokeOnClient(this, LoginParamsWithId{ p.login, "", bAccessGranted, p.entityId, p.entityIdComp }, pEntity->GetNetEntity()->GetChannelId());
	return true;
}
//actual command handling
void SServerComponent::ServerCommandHandler(IConsoleCmdArgs * pArgs)
{
	const string commandName = pArgs->GetArg(0);
	string args[10] = { "" };
	for (int i = 0; i < pArgs->GetArgCount(); i++)
	{
		args[i] = pArgs->GetArg(i);
	}
	const string commandLogIn = "sv_admin_log_in";

	if (commandName == commandLogIn)
	{
		const int newAdminId = atoi(args[2]);
		if (IEntity *pEntity = gEnv->pEntitySystem->GetEntity(newAdminId))
		{
			if (CPlayerComponent *playerRequestor = pEntity->GetComponent<CPlayerComponent>())
			{
				if (args[1] == GetPlugin()->GetGameCVars()->pAdminPass->GetString())
				{
					CryLogAlways("PLAYER %s IS ADMIN NOW", pEntity->GetName());
					playerRequestor->IsAdmin(true);
				}
				else
				{
					CryLogAlways("PLAYER %s TRIED TO LOG IN TO ADMIN BUT FAILED WITH PASSWORD %s", pEntity->GetName(), args[1]);
				}
			}
		}
		return;
	}
	if (commandName == "sv_restart_match")
	{
		SERVER->ServerRestartMatch();
	}
	else if (commandName == "sv_player_move_to_player")
	{
		IEntity *playerToMove = nullptr;
		IEntity *playerDestination = nullptr;
		auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
		pEntityIterator->MoveFirst();

		while (!pEntityIterator->IsEnd())
		{
			IEntity *pEntity = pEntityIterator->Next();
			if (pEntity->GetName() == args[1])
			{
				playerToMove = pEntity;
			}
			else if (pEntity->GetName() == args[2])
			{
				playerDestination = pEntity;
			}

			if (playerToMove && playerDestination)
			{
				Vec3 destinationPoint = playerDestination->GetWorldPos();
				playerToMove->SetPos(destinationPoint);
				break;
			}
		}
	}
	else if (commandName == "sv_kick")
	{
		auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
		pEntityIterator->MoveFirst();

		while (!pEntityIterator->IsEnd())
		{
			IEntity *pEntity = pEntityIterator->Next();
			if (pEntity->GetName() == args[1])
			{
				gEnv->pGameFramework->GetNetChannel(pEntity->GetNetEntity()->GetChannelId())->Disconnect(EDisconnectionCause::eDC_Kicked, "KICKED OUT");
				break;
			}
		}
	}
	else if (commandName == "sv_team_name")
	{
		if (CGameTeam *teamToModify = SERVER->GetTeam(args[1]))
		{
			teamToModify->SetTeamName(args[2]);
		}
	}
	else if (commandName == "sv_player_set_team")
	{
		auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
		pEntityIterator->MoveFirst();

		while (!pEntityIterator->IsEnd())
		{
			IEntity *pEntity = pEntityIterator->Next();
			if (pEntity->GetName() == args[1])
			{
				CPlayerComponent *player = pEntity->GetComponent<CPlayerComponent>();
				CAccountComponent *acc = player->GetAssociatedAccount();
				if (CGameTeam *teamTarget = SERVER->GetTeam(args[2]))
				{
					if (acc)
					{
						SERVER->GetTeam(acc->GetTeamIndex())->RemoveAccount(acc);
						teamTarget->AddAccount(acc);
						player->MoveToSpawnPoint();
					}
				}
				break;
			}
		}
	}
	else if (commandName == "sv_player_kill")
	{
		auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
		pEntityIterator->MoveFirst();

		while (!pEntityIterator->IsEnd())
		{
			IEntity *pEntity = pEntityIterator->Next();
			if (pEntity->GetName() == args[1])
			{
				CPlayerComponent *player = pEntity->GetComponent<CPlayerComponent>();
				SERVER->GetGameRules()->KillPlayer(player);
				break;
			}
		}
	}
	else if (commandName == "sv_player_respawn")
	{
		auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
		pEntityIterator->MoveFirst();

		while (!pEntityIterator->IsEnd())
		{
			IEntity *pEntity = pEntityIterator->Next();
			if (pEntity->GetName() == args[1])
			{
				CPlayerComponent *player = pEntity->GetComponent<CPlayerComponent>();
				if (!player->IsPlayerAlive())
					player->ServerReset();
				break;
			}
		}
	}
	else if (commandName == "sv_give_admin")
	{
		auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
		pEntityIterator->MoveFirst();

		while (!pEntityIterator->IsEnd())
		{
			IEntity *pEntity = pEntityIterator->Next();
			if (pEntity->GetName() == args[1])
			{
				CPlayerComponent *player = pEntity->GetComponent<CPlayerComponent>();
				player->IsAdmin(true);

				break;
			}
		}
	}
}
//CVar RMIs
bool SServerComponent::ServerCommand(EntityIdWithString && p, INetChannel *)
{
	IEntity *pEntity = Server_GetEntityById(p.entityId, p.entityIdComp);

	if (pEntity)
	{
		if (CPlayerComponent *playerRequestor = pEntity->GetComponent<CPlayerComponent>())
		{
			const string commandName = p.str;
			const string commandLogIn = "sv_admin_log_in";

			bool bLogIn = false;
			for (int i = 0; i < commandLogIn.length(); i++)
			{
				if (commandName.length() >= i+1)
				{
					if (commandName[i] == commandLogIn[i])
					{
						bLogIn = true;
					}
					else
					{
						bLogIn = false;
						break;
					}
				}
			}
			if (bLogIn)
			{
				gEnv->pConsole->ExecuteString(commandName + " " + ToString(pEntity->GetId()), false, true);
			}
			else if (playerRequestor->IsAdmin())
			{
				gEnv->pConsole->ExecuteString(commandName, false, true);
				CryLogAlways("SERVER COMMAND %s TRIGGERED BY %s", p.str, pEntity->GetName());
			}
		}
	}
	return true;
}

bool SServerComponent::ServerCVarString(CVarString && p, INetChannel *)
{
	return true;
}

bool SServerComponent::ServerCVarInt(CVarInt && p, INetChannel *)
{
	return true;
}
CGameTeam * SServerComponent::GetTeam(string name)
{
	for (int i = 0; i < LOBBY_TEAM_COUNT; i++)
	{
		if (CGameTeam *team = GetTeam(i))
		{
			if (team->GetTeamName() == name)
			{
				return team;
			}
		}
	}
	return nullptr;
}
//~CVar RMIs
//
void SServerComponent::ClientCommandHandler(IConsoleCmdArgs * pArgs)
{
	SRmi<RMI_WRAP(&SServerComponent::ServerCommand)>::InvokeOnServer(SERVER, EntityIdWithString{ SERVER->pLocalPlayer->GetEntityId(), SERVER->pLocalPlayer->GetEntityId(), pArgs->GetCommandLine() });
}
//
void SServerComponent::ServerAddItem(SItemComponent * pItem)
{
	if (!pItem || gEnv->IsEditor())
		return;

	pItems.push_back(pItem);
}

void SServerComponent::ServerAddPlayer(CPlayerComponent * pPlayer)
{
	if (!pPlayer || gEnv->IsEditor())
		return;

	pPlayers.push_back(pPlayer);
}

void SServerComponent::ServerRemovePlayer(CPlayerComponent * pPlayer)
{
	if (!pPlayer)
		return;

	pPlayers.erase(std::remove(pPlayers.begin(), pPlayers.end(), pPlayer), pPlayers.end());
}

void SServerComponent::ServerRestartMatch()
{		
	//to restart entire match, we need to restart all items and players and put them back to their start positions, let's start from players, so their attachments will release items
		//PLAYERS RESET
	for (int players = 0; players < ServerGetPlayersCount(); players++)
	{
		if (CPlayerComponent *player = pPlayers[players])
			player->ServerReset();
	}
	//ITEMS RESET
	for (int items = 0; items < ServerGetItemsCount(); items++)
	{
		if (SItemComponent *item = pItems[items])
			item->ServerReset();
	}
	//TEAMS SCORE RESTART
	for (int teams = 0; teams < LOBBY_TEAM_COUNT; teams++)
	{
		if (CGameTeam *team = GetTeam(teams))
			team->ServerReset();
	}
	CryLogAlways("MATCH RESTARTED");
}
