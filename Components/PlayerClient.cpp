#include "PlayerComponent.h"
#include "UISystem/HudComponent.h"
#include "CaptureArea.h"
#include "GlobalResources.h"
#include "Cry3DEngine/ITimeOfDay.h"

void CPlayerComponent::InitClientMethods()
{
}

void CPlayerComponent::LocalPlayerInitialize()
{
	//This happens only on local player, so local player specific stuff needs to be specified here
	//we initialize static pointer to local player on this machine - very useful - we don't have to iterate through all entities to find local
	//we also initialize local client, cause it's easier than always getting client from local player
	pLocalPlayer = this;
	//this bool is just if you need to check if the player is local 
	bIsLocal = true;
	pAttachment->bIsLocal = true;
	pInventory->bIsLocal = true;
	if (GetServer())
		GetServer()->SetLocalPlayer(this);

	pUI = m_pEntity->GetOrCreateComponentClass<CPlayerUIComponent>();
	pAnimations->SetCharacterFile(sPlayerProperties.sAnimationProperties.sCharacterFileFP.value.c_str());
	pCamera = m_pEntity->GetOrCreateComponentClass<SCameraComponent>();
	pInput = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CInputComponent>();
	pHudComponent = m_pEntity->GetOrCreateComponentClass<CHudComponent>();
	InitInputActions();
}