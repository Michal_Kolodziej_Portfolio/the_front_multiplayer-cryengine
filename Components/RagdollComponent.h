/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Ragdoll component for the player

--------------------------------------------------------------------------------- */
#pragma once

#include "CryEntitySystem/IEntityComponent.h"

class CRagdollComponent : public IEntityComponent
{
public:
	CRagdollComponent() = default;
	virtual ~CRagdollComponent() {}


	virtual void Initialize() override;
	static void ReflectType(Schematyc::CTypeDesc<CRagdollComponent>& desc);
};