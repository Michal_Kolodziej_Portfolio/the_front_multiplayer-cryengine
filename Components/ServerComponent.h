/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Server component is single entity on the map that hosts all the info about players, like teams etc.

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

#define LOBBY_TEAM_COUNT 3
#define MAX_WAITING_PLAYERS 64
#define MAX_TEAM_PLAYERS 32

class CGameTeam;
struct SGameRules;
struct SItemComponent;

struct SServerComponent : public IEntityComponent
{
	const EEntityAspects kServerDynamicData = eEA_GameServerDynamic;
	const EEntityAspects kServerRegularData = eEA_GameServerD;
	const EEntityAspects kServerStaticData = eEA_GameServerStatic;
public:
	SServerComponent() = default;
	SServerComponent::~SServerComponent() {}
	virtual void Initialize() override; 
	static void ReflectType(Schematyc::CTypeDesc<SServerComponent>& desc);
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kServerDynamicData | kServerRegularData | kServerStaticData; };
	void UpdateDynamicData() { NetMarkAspectsDirty(kServerDynamicData); }

	bool ServerAttemptLogin(LoginParamsWithId&& p, INetChannel *);
	void ClientAttemptLogin(string requestedLogin, string password, EntityId id);

	bool ClientAccess(LoginParamsWithId&& p, INetChannel *);

	void ClientJoinLobby(string player_class);
	bool ServerAddToLobby(EntityIdWithString&& p, INetChannel *);

	void ClientMoveToTeam(int team);
	bool ServerMoveToTeam(TeamParams&& p, INetChannel *);

	void ClientJoinGame();
	bool ServerJoinGame(EntityIdPass&& p, INetChannel *);

	void SetLocalAccount(CAccountComponent *_localAccount) { pLocalAccount = _localAccount; }
	void SetLocalPlayer(CPlayerComponent *_localPlayer) { pLocalPlayer = _localPlayer; }

	void ServerKillDeath(CPlayerComponent *pAttacker, CPlayerComponent *pVictim);
	bool ClientMessage(MessageParams&& p, INetChannel *);

	CAccountComponent *GetLocalAccount() { return pLocalAccount; }
	CPlayerComponent *GetLocalPlayer() { return pLocalPlayer; }

	CGameTeam *GetTeam(int index) { return pGameTeam[index]; }
	CGameTeam *GetTeam(string name);

	SGameRules *GetGameRules() { return pRules; }

	static void ClientCommandHandler(IConsoleCmdArgs *pArgs);
	static void ServerCommandHandler(IConsoleCmdArgs *pArgs);
	bool ServerCommand(EntityIdWithString&& p, INetChannel *);
	bool ServerCVarString(CVarString&& p, INetChannel *);
	bool ServerCVarInt(CVarInt&& p, INetChannel *);

	void ServerAddItem(SItemComponent *pItem);
	int ServerGetItemsCount() { return pItems.size(); }
	void ServerAddPlayer(CPlayerComponent *pPlayer);
	void ServerRemovePlayer(CPlayerComponent *pPlayer);
	int ServerGetPlayersCount() { return pPlayers.size(); }
	void ServerRestartMatch();
private:
	//SERVER VARIABLES

	static SServerComponent *SERVER;

	CAccountComponent *pLocalAccount = nullptr;
	CPlayerComponent *pLocalPlayer = nullptr;

	std::vector<CGameTeam *> pGameTeam;

	SGameRules *pRules = nullptr;

	//ONLY SERVER variable storing all items 
	std::vector<SItemComponent *> pItems;
	//variable storing all players
	std::vector<CPlayerComponent *> pPlayers;
};
