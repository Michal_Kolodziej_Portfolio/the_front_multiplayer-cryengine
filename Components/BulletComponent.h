/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : It defines bullet. All properties are available from within the editor.

--------------------------------------------------------------------------------- */

#pragma once
#include "BulletProperties.h"

class CBulletComponent final : public IEntityComponent
{
public:
	CBulletComponent() = default;
	virtual ~CBulletComponent() {}
	virtual void Initialize() override {}
	virtual void   ProcessEvent(SEntityEvent& event);
	virtual uint64 GetEventMask() const override;
	static void ReflectType(Schematyc::CTypeDesc<CBulletComponent>& desc);
	virtual void SetOwner(CPlayerComponent *pNewOwner) { pOwner = pNewOwner; }
	virtual CPlayerComponent *GetOwner() { return pOwner; }
	virtual void ReLoadBullet(Vec3 bulletDirection);
	virtual float GetDamage();
	virtual float GetBulletRecoil();
	CPlayerComponent *pOwner = nullptr;
	SBulletProperties sBulletProps;
	SBulletProperties sPrevBulletProps;
};