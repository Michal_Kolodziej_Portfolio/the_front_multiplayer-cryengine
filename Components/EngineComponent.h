/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Attaching this component to the vehicle will give it "life" with all engine adjustments that we desire, like gears, torque etc

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>
#include "EngineProperties.h"

struct SVehicleComponent;

class CEngineComponent : public IEntityComponent
{
	const EEntityAspects kEngineAspect = eEA_GameServerDynamic;
public:
	CEngineComponent() = default;
	CEngineComponent::~CEngineComponent() {}

	// IEntityComponent
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	// ~IEntityComponent
	static void ReflectType(Schematyc::CTypeDesc<CEngineComponent>& desc);
	//NETWORKING
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kEngineAspect; };
	//~NETWORKING
	//ENGINE COMPONENT
	void Start();
	void AbortStarting();
	void Stop();
	void StartNow();
	bool IsWorking() { return sEngineProperties.bIsWorking; }
	SEngineProperties *GetProperties() { return &sEngineProperties; }
	void OpenThrottle();
	void CloseThrottle();
	void SetGear(int gear);
	void NextGear();
	void PreviousGear();
	int GetCurrentGear() { return sEngineProperties.iActiveGear; }
	int GetLastGear() { return sEngineProperties.iForwardGears; }
	int GetFirstGear() { return -(sEngineProperties.iBackwardGears); }
	bool GearIs(int gear) { return GetCurrentGear() == gear; }
	float GetCurrentHorsePower();
	float GetHorsePowerAtRpm(int rpm);
	virtual int GetCurrentGearTypeGearCount() { if (GetCurrentGear() < 0) return GetProperties()->iBackwardGears; else if (GetCurrentGear() > 0) return GetProperties()->iForwardGears; return -1; }
	void ServerReset();
	//~ENGINE COMPONENT
protected:
	void Startup();
protected:
	SEngineProperties sEngineProperties, sEnginePrevProperties, sEngineStartupProperties;
	SVehicleComponent *pVehicle = nullptr;
};
