/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Base vehicle class. It derives from SItem structure, because it actually shares most of the functionality

--------------------------------------------------------------------------------- */

#pragma once

#include <array>
#include <numeric>

#include "ItemComponent.h"
#include "VehicleProperties.h"
#include "NetPlayer.h"

class CEngineComponent;

struct SVehicleComponent : public SItemComponent
{
	const EEntityAspects kVehicleAspect = eEA_GameServerDynamic;
public:

	SVehicleComponent() = default;
	virtual ~SVehicleComponent() {}
	//IEntityComponent
	virtual void InitClass();
	virtual void ClassProcessEvent(SEntityEvent& event);
	//~IEntityComponent
	//input
	virtual void ClientActionRegister() override;
	virtual void ServerActionRegister() override;
	void Action_EngineToggle(int activationMode);
	void Action_GearUp(int activationMode);
	void Action_GearDown(int activationMode);
	//NETWORKING
	virtual void ServerPickup(CPlayerComponent *pNewOwner) override;
	//~NETWORKING
	virtual bool ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetClassAspectMask() const override { return kVehicleAspect; }
	//Vehicle system
	virtual bool CanEnter(int cabine_slot);
	virtual void Enter(CPlayerComponent *pEnteringPlayer, int cabine_slot);
	virtual void Leave(EntityId id);
	virtual void Leave(int cabine_slot);
	virtual Vec3 GetCameraWorldPos();
	virtual Vec3 GetCameraLocalPos();
	virtual void ServerMove();
	virtual bool IsEngineWorking();
	virtual void ServerShutdownEngine();
	virtual void ServerStartupEngine();
	virtual void ServerStartupEngineAbort();
	virtual void FireEngineNow();
	virtual void Accelerate();
	virtual void NextGear();
	virtual void PreviousGear();
	virtual void SetGear(int gear);
	virtual int GetActiveGear();
	virtual int GetNextEmptyCabineSlot();
	virtual CPlayerComponent *GetDriverPlayer();
	virtual CEngineComponent *GetEngine() { return pEngine; }
	virtual void SetPhysicsSpeed(float speed) { sVehicleProperties.physicsSpeed = speed; }
	virtual float GetPhysicsSpeed();
	virtual int GetCurrentGearTypeGearCount();
	virtual int GetCurrentRpm();
	virtual QuatTS GetAttachmentWorldAbsolute(EntityId id);
	virtual Quat GetCameraRotation();
	virtual QuatTS GetCameraWorldAbsolute();
	//~Vehicle system

	static void ReflectType(Schematyc::CTypeDesc<SVehicleComponent>& desc);
	virtual SVehicleProperties *GetVehicleProperties() { return &sVehicleProperties; }
	//redefinitions for picking and dropping
	//vehicles won't be handled by inventory at any point, so we're going to disable all of those methods
	virtual bool IsSelected() override { return false; }
	virtual void IsSelected(bool is) override {}
	virtual void ReLoadPhysics() override;
	virtual void ServerResetClass() override;
protected:
	SAdvancedAnimationComponent *pAnimations = nullptr;
	FragmentID m_movementFragmentID;
	FragmentID m_activeFragment;
	TagID m_Backward;
	SVehicleProperties sVehicleProperties, sVehiclePrevProperties, sVehicleStartupProperties;
	IAttachment *pDriverAttachment = nullptr;
	CEngineComponent *pEngine = nullptr;
};