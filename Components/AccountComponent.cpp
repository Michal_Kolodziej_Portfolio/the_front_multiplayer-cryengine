#include "StdAfx.h"
#include "AccountComponent.h"
#include "CrySchematyc/Env/Elements/EnvComponent.h"
#include "CrySchematyc/Env/Elements/EnvFunction.h"
#include "CrySchematyc/Env/IEnvRegistrar.h"
#include "UISystem/MainMenu.h"
#include "UISystem/LoginMenu.h"
#include "UISystem/LobbyMenu.h"
#include "GlobalResources.h"
#include <CryEntitySystem//IEntityClass.h>
#include <CryEntitySystem/IEntitySystem.h>
#include "PlayerComponent.h"
#include "ServerComponent.h"
#include "GameTeam.h"

CAccountComponent *CAccountComponent::pLocalAccount = nullptr;

static void RegisterAccount(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAccountComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterAccount)

CAccountComponent::~CAccountComponent()
{
	if (gEnv->IsEditor())
		return;

	if (!gEnv->bServer)
	{
		//when this account is destroyed on clients remove from lobby ui
		//never call it on local machine, cause no need any logic anymore
		if (!bIsLocal)
		{
			if (GetLocalAccount())
			{
				pLocalAccount->GetLobbyMenu()->RemoveAccount(GetLogin());
			}
		}
	}
}

void CAccountComponent::Initialize()
{
}

void CAccountComponent::ReflectType(Schematyc::CTypeDesc<CAccountComponent>& desc)
{
	desc.SetGUID("{DF573A2A-2069-48DD-81F2-E590B432C72B}"_cry_guid);
}

uint64 CAccountComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_NET_BECOME_LOCAL_PLAYER) | BIT64(ENTITY_EVENT_UPDATE);
}

void CAccountComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_NET_BECOME_LOCAL_PLAYER:
	{
		if (!gEnv->IsEditor())
		{
			//account will handle login menu and we will initialize it on local client
			bIsLocal = true;
			pLocalAccount = this;
			GetServer()->SetLocalAccount(this);
			//initialize login menu on the very beginning and show it; login menu is the first menu that client sees
			pLoginMenu = new CLoginMenu("LoginMenu", this);
			pMainMenu = new CMainMenu("MainMenu", this);
			pLobbyMenu = new CLobbyMenu("LobbyMenu", this);
			pLoginMenu->Show();
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (gEnv->bServer)
		{
			if (bIsInLobby != (iTeam > -1 && iIndex > -1))
			{
				bIsInLobby = (iTeam > -1 && iIndex > -1);
				ServerUpdateAccountData();
			}
		}
	}
	break;
	}
}

bool CAccountComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kAccountData)
	{
		ser.BeginGroup("AccountData");

		ser.Value("login", sLogin, 'stab');
		ser.Value("team", iTeam);
		ser.Value("index", iIndex);
		ser.Value("inlobby", bIsInLobby, 'bool');
		ser.Value("associatedId", associatedPlayerId);
		ser.Value("associatedIdComp", associatedPlayerIdComp, 'eid');

		if (ser.IsReading() && !gEnv->bServer)
		{
			if (GetLocalAccount())
			{
				if (bIsInLobby)
					pLocalAccount->GetLobbyMenu()->SetAccount(sLogin, iTeam, iIndex);

				if (bIsLocal)
					GetLobbyMenu()->AllowJoin((iTeam != Team_Lobby_Waiting));
			}
			IEntity *playerEnt = gEnv->pEntitySystem->GetEntity(associatedPlayerId);
			IEntity *playerEntComp = gEnv->pEntitySystem->GetEntity(associatedPlayerIdComp);
			if (playerEnt)
			{
				if (CPlayerComponent *pl = playerEnt->GetComponent<CPlayerComponent>())
				{
					pAssociatedPlayer = pl;
					return true;
				}
			}
			if (playerEntComp)
			{
				if (CPlayerComponent *pl = playerEntComp->GetComponent<CPlayerComponent>())
				{
					pAssociatedPlayer = pl;
					return true;
				}
			}
		}

		ser.EndGroup();
	}
	return true;
}


void CAccountComponent::SetLogin(string newLogin)
{
	sLogin = newLogin;
	ServerUpdateAccountData();
}

void CAccountComponent::SetTeam(CGameTeam *pTeamToSet, int index)
{
	if (!pTeamToSet)
		return;

	iTeam = pTeamToSet->GetIndex();
	iIndex = index;
	ServerUpdateAccountData();
}

CGameTeam * CAccountComponent::GetTeam()
{
	return GetServer()->GetTeam(iTeam);
}

void CAccountComponent::SetPlayer(CPlayerComponent * pNewPlayer)
{
	pAssociatedPlayer = pNewPlayer;
	if (pNewPlayer)
	{
		associatedPlayerId = pNewPlayer->GetEntityId(); associatedPlayerIdComp = associatedPlayerId;
	}
	else
	{
		associatedPlayerId = 0; associatedPlayerIdComp = 0;
	}
	ServerUpdateAccountData();
}
