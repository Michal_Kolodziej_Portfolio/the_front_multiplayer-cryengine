/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Ammo pack is a package with bullets. It is not picked up. Player just takes bullets from this box and stores it in "pockets".

--------------------------------------------------------------------------------- */

#pragma once
#include "AmmoPackProperties.h"

class CCaptureAreaComponent;

class CAmmoPackComponent final : public SItemComponent
{
public:
	CAmmoPackComponent();
	virtual ~CAmmoPackComponent() {}
	static void ReflectType(Schematyc::CTypeDesc<CAmmoPackComponent>& desc);
	virtual void InitClass();
	virtual void ClassProcessEvent(SEntityEvent& event) override;
	//input
	virtual void ClientActionRegister() override;
	virtual void ServerActionRegister() override;
	void Action_Place(int activationMode);
	//NETWORKING
	virtual bool ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual void ServerPickup(CPlayerComponent *pNewOwner) override;
	//~NETWORKING
	//AMMO PACK USAGE
	virtual int GetCurrentAmmo() { return sAmmoPackProperties.iCurrentAmmo; }
	virtual int GetMaxAmmo() { return sAmmoPackProperties.iMaxAmmo; }
	virtual void DecreaseAmmo() { sAmmoPackProperties.iCurrentAmmo -= 1; }
	//Positive values add, negative values remove
	virtual int ServerAddRemoveAmmo(int ammo);
	virtual bool IsEmpty() { return sAmmoPackProperties.iCurrentAmmo < 1; }
	virtual void SetCaptureArea(CCaptureAreaComponent *pCap) { if (!pCap) return; pCaptureArea = pCap; }
	virtual int GetTeam() { return sAmmoPackProperties.iTeam; }
	virtual string GetPickupLabel(CPlayerComponent *pPicker) override;
	virtual string GetBulletName() { return sAmmoPackProperties.sBulletName.value.c_str(); }
	virtual string GetBulletShortName()
	{
		string sLongName = sAmmoPackProperties.sBulletName.value.c_str();
		sLongName.erase(0, sLongName.find_last_of(':') + 1);
		string sFirst;
		sFirst.SetAt(0, sLongName[0]);
		sFirst.MakeUpper();
		sLongName.erase(0, 1);
		sItemName = sFirst + sLongName;
		return sItemName;
	}
	virtual bool IsBulletCorrect(string bulletName) { return GetBulletName() == bulletName; }
	virtual void ServerResetClass() override;
	//VARIABLES
	SAmmoPackProperties sAmmoPackProperties;
	SAmmoPackProperties sAmmoPackPrevProperties;
	SAmmoPackProperties sAmmoPackStartupProperties;
	//capture properties
	CCaptureAreaComponent *pCaptureArea = nullptr;
};