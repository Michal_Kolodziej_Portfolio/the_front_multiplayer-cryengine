#include "StdAfx.h"
#include "GameTeam.h"
#include "CrySchematyc/Env/IEnvRegistrar.h"
#include "CrySchematyc/Env/Elements/EnvComponent.h"
#include "AccountComponent.h"
#include "CrySystem/ISystem.h"
#include "UISystem/HudComponent.h"

static void RegisterTeam(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CGameTeam));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterTeam)

uint64 CGameTeam::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE);
}

void CGameTeam::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		if (gEnv->bServer)
		{
			//if changes appear
			if (pList_last != pList)
			{
				//iterate through all accounts in this list
				for (int index = 0; index < iListLength; index++)
				{
					if (pList[index])
					{
						list_serializer[index] = pList[index]->GetEntityId();
						list_serializerComp[index] = list_serializer[index];
					}
					else
					{
						list_serializer[index] = 0;
						list_serializerComp[index] = 0;
					}

					//if index is still within last state size; prevents from vector being out of range
					if (index < pList_last.size())
					{
						//now find which exactly player position has changed to only apply changes to players that positions are affected; it saves resources
						if (pList_last[index] != pList[index])
						{
							//set account team and index
							//check if it's valid account
							if (pList[index])
								pList[index]->SetTeam(this, index);
						}
					}
					//if index is higher than size of last state size
					else
					{
						if (pList[index])
							pList[index]->SetTeam(this, index);
					}
				}
				//after accounts loop ended, update last state vector to match current
				pList_last = pList;
				NetMarkAspectsDirty(kTeamStaticData);
			}
		}
	}
	break;
	}
}

void CGameTeam::ReflectType(Schematyc::CTypeDesc<CGameTeam>& desc)
{
	desc.SetGUID("{61848DF6-3FBD-445A-AFDC-93EFF932BAB8}"_cry_guid);
	desc.SetEditorCategory("DO NOT USE");
	desc.SetLabel("Team(DO NOT USE)");
	desc.SetDescription("Component manages teams");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
}

bool CGameTeam::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kTeamInitialData)
	{
		ser.BeginGroup("TeamInitialData");

		ser.Value("name", sTeamName, 'stab');
		ser.Value("index", iIndex);
		ser.Value("score", iScore);

		if (!gEnv->bServer)
		{
			if (CPlayerComponent *localPlayer = GetServer()->GetLocalPlayer())
			{
				localPlayer->GetHudComponent()->UpdateTeamInfo();
			}
		}

		ser.EndGroup();
	}
	if (aspect == kTeamStaticData)
	{
		ser.BeginGroup("StaticData");

		for (int i = 0; i < MAX_TEAM_PLAYERS; i++)
		{
			ser.Value("list_serializer" + ToString(i), list_serializer[i]);
			ser.Value("list_serializerComp" + ToString(i), list_serializerComp[i], 'eid');

		}

		ser.EndGroup();
	}
	return true;
}

void CGameTeam::AddAccount(CAccountComponent * pAccountToAdd)
{
	if (!pAccountToAdd)
		return;

	pList.push_back(pAccountToAdd);
	iListLength = pList.size();
	pAccountToAdd->SetTeam(this, iListLength - 1);
}

void CGameTeam::RemoveAccount(CAccountComponent * pAccountToRemove)
{
	if (!pAccountToRemove)
		return;

	pList.erase(std::remove(pList.begin(), pList.end(), pAccountToRemove), pList.end());
	iListLength = pList.size();
}

void CGameTeam::Set(string sNewName, int iNewIndex)
{
	sTeamName = sNewName;
	iIndex = iNewIndex;
	NetMarkAspectsDirty(kTeamInitialData);
}

CAccountComponent * CGameTeam::GetAccountAtIndex(int index)
{
	if (index < 0)
		return nullptr;

	if (IEntity *pEnt = gEnv->pEntitySystem->GetEntity(list_serializer[index]))
	{
		if (CAccountComponent *acc = pEnt->GetComponent<CAccountComponent>())
			return acc;
	}
	if (IEntity *pEnt = gEnv->pEntitySystem->GetEntity(list_serializerComp[index]))
	{
		if (CAccountComponent *acc = pEnt->GetComponent<CAccountComponent>())
			return acc;
	}
	
	return nullptr;
}
