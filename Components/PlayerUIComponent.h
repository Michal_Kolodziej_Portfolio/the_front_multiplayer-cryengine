/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Player UI system for UI elements specific for the player in game

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

struct IUIElement;
struct IUIAction;
struct IUIActionManager;
struct SUIArguments;

class CPlayerUIComponent : public IEntityComponent
{
public:
	CPlayerUIComponent() = default;
	CPlayerUIComponent::~CPlayerUIComponent() {}
	virtual void Initialize() override;
	static void ReflectType(Schematyc::CTypeDesc<CPlayerUIComponent>& desc)
	{
		desc.SetGUID("{9D1899B4-2E61-4C45-9138-5975D3B0DB8C}"_cry_guid);
	}
	//
	void ShowSummary(bool show = true);
	void ShowExitDialog(bool show = true);
	void ShowNotifications(bool show = true);
	void KillMessage(SUIArguments args);
	void JoinMessage(SUIArguments args);
	void SummarySetPlayer(SUIArguments args);
	void SummaryClear();
	void ShowAmmoPackDialog(bool show = true);
	void SetAmmoPackDialog(SUIArguments args);
	void ShowRoundLoadingDialog(bool show = true);
	void ShowInventory();
	void AddItem(string name, int slot, int type, int bullets = -1);
	void RemoveActiveItem();
	void RemoveItem(int slot, int type);
	void Activate(int slot);
	void Deactivate();
	void SetBullet(string name, int line, int count);
	void ClearInventory();

public:
	bool bIsAmmoPackDialogOpened = false;
	bool bIsRoundLoading = false;
	bool bIsInventoryOpened = false;
private:
	//summary dialog
	IUIElement *pResultMenu = nullptr;
	IUIAction *pResultShow = nullptr;
	IUIAction *pResultHide = nullptr;
	//exit dialog
	IUIElement *pExitDialog = nullptr;
	IUIAction *pExitShow = nullptr;
	IUIAction *pExitHide = nullptr;
	//kill messages
	IUIElement *pNotifications = nullptr;
	IUIAction *pNotificationsShow = nullptr;
	IUIAction *pNotificationsHide = nullptr;
	//ammo pack dialog
	IUIElement *pAmmoPackDialog = nullptr;
	IUIAction *pAmmoPackDialogShow = nullptr;
	IUIAction *pAmmoPackDialogHide = nullptr;
	//ammo pack dialog
	IUIElement *pRoundLoadingDialog = nullptr;
	IUIAction *pRoundLoadingDialogShow = nullptr;
	IUIAction *pRoundLoadingDialogHide = nullptr;
	//InventoryUI
	IUIElement *pInventoryMenu = nullptr;
	IUIAction *pInventoryShow = nullptr;
	IUIAction *pInventoryHide = nullptr;
	//manager
	IUIActionManager *pMan = nullptr;
};
