#pragma once

#include "PlayerAreaTrigger.h"

class CCaptureAreaComponent : public CPlayerAreaTriggerComponent {

public:

	enum CaptureType {
		AMMO = 1 << 0,
		FUEL = 1 << 1,
		REINFORCMENTS = 1 << 2
	};


public:

	CCaptureAreaComponent() = default;
	virtual ~CCaptureAreaComponent() { }

	virtual void Initialize() override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CCaptureAreaComponent>& desc);
	void SetCaptureType(int16 flags) { m_fCaptureTypeFlags |= flags; }
	int8 GetCaptureTypeFlags() { return m_fCaptureTypeFlags; }

	bool GetIsCaptured(){ return m_bCaptured; }
	bool GetIsContested() { return m_bContested; }
	int GetCapturingTeam() { return m_CapturingTeam; }

	void SetCapturingTeam(int iTeam) { m_CapturingTeam = iTeam; }
	void SetIsCaptured(bool captured) { m_bCaptured = captured; }
	void SetIsContested(bool contested) { m_bContested = contested; }


protected:
	
	int8 m_fCaptureTypeFlags;

	bool m_bCaptured;
	bool m_bContested;
	int m_iCaptureTicker;
	

	int m_CapturingTeam;

	//last values to see if they changed to prevent sending RMI every frame
	bool m_bCaptured_last;
	bool m_bContested_last;
	int m_CapturingTeam_last;
	//

	int m_iGermanCount;
	int m_iRussianCount;
	
	bool bIsInitializedLocally = false;

	ColorB m_cColor;
};