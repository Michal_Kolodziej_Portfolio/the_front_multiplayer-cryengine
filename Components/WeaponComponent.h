/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : It is a weapon type of item.

--------------------------------------------------------------------------------- */

#ifndef _WEAPONCOMPONENT_
#define _WEAPONCOMPONENT_
#pragma once
#include "ItemComponent.h"
#include "EntityAudioComponent.h"
#include "WeaponProperties.h"

class CWeaponComponent final : public SItemComponent
{
	enum ESound
	{
		SOUND_Fire = 0, 
		SOUND_Fire_dry,
		SOUND_Reload
	};
public:
	CWeaponComponent() = default;
	virtual ~CWeaponComponent() {}
	static void ReflectType(Schematyc::CTypeDesc<CWeaponComponent>& desc);
	virtual void InitClass();
	virtual void ClassProcessEvent(SEntityEvent& event) override;
	//input
	virtual void ClientActionRegister() override;
	virtual void ServerActionRegister() override;
	void Action_Reload(int activationMode);
	void Action_Shoot(int activationMode);
	void Action_SwitchFiremode(int activationMode);
	//NETWORKING
	virtual bool ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	//RMI
	bool ClientLaunch(BooleanPass&& p, INetChannel *);
	//~NETWORKING
	//WEAPON COMPONENT
	void ServerFire();
	void ServerStopFire();
	virtual void SpawnBullet();
	virtual void StopFire();
	virtual void ServerLaunch(bool real = true); 
	virtual void LaunchDry();
	virtual void ReloadBullet();
	virtual void StartAim();
	virtual void StopAim();
	virtual void ServerReload();
	virtual int GetMaxAmmo();
	virtual int GetCurrentAmmo();
	virtual bool IsWeaponReadyToShoot();
	virtual bool IsSingle();
	void SetCurrentAmmoClip(EntityId id) { sWeaponProperties.currentAmmoClip = id; sWeaponProperties.currentAmmoClipComp = id; }
	void ReleaseAmmoClipAttachment();
	void AttachAmmoClip();
	CAmmoClipComponent * GetCurrentAmmoClip();
	EntityId GetCurAmmoClipId() { return sWeaponProperties.currentAmmoClip; }
	bool HasAmmo();
	CFireModeComponent *pFireModes = nullptr;
	virtual void NextFireMode();
	virtual string GetAmmoClipName() { return sWeaponProperties.sAmmoClipName.value.c_str(); }
	virtual bool IsAmmoClipCorrect(string magName) { return GetAmmoClipName() == magName; }
	virtual Quat GetBulletOutputOrientation();
	virtual void ServerResetClass() override;
	//VARIABLES
	SWeaponProperties sWeaponProperties;
	SWeaponProperties sPrevWeaponProperties;
	SWeaponProperties sWeaponStartupProperties;
	//AUDIO TEST
	SEntityAudioComponent *pAudio = nullptr;
};
#endif