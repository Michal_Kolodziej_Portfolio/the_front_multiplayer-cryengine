/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : New EntityComponent

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

class CTestComponent : public IEntityComponent
{
	const EEntityAspects kServerToClient = eEA_GameServerD;
	const EEntityAspects kClientToServer = eEA_GameClientD;
public:
	CTestComponent() = default;
	CTestComponent::~CTestComponent() {}
	virtual void Initialize() override;
	static void ReflectType(Schematyc::CTypeDesc<CTestComponent>& desc);
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kServerToClient | kClientToServer; };
	void UpdateClientAspect() { NetMarkAspectsDirty(kClientToServer); }
	void UpdateServerAspect() { NetMarkAspectsDirty(kServerToClient); }

	int clientInt = 0;
	int serverInt = 0;
};
