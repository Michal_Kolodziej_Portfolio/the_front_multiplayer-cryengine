/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores all items. Attached to player entity on initialize

--------------------------------------------------------------------------------- */

#pragma once
#include <CryEntitySystem/IEntityComponent.h>

#define WEAPON_CAPACITY 3
#define AMMOCLIP_CAPACITY 10
#define BULLET_CAPACITY 100
#define BULLET_TYPE_CAPACITY 10

struct SItemComponent;
class CAmmoClipComponent;
class CWeaponComponent;
class CAmmoPackComponent;

class CInventoryComponent final : public IEntityComponent
{
	const EEntityAspects kInventoryStaticData = eEA_GameServerStatic;
public:
	enum Type
	{
		NONE = -1, 
		WEAPON_PROJECTILE, 
		WEAPON_MELEE, 
		WEAPON_THROW
	};
	CInventoryComponent() = default;
	virtual ~CInventoryComponent() {}
	//ComponentSystem
	virtual void Initialize() override;
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent &event) override {}
	static void ReflectType(Schematyc::CTypeDesc<CInventoryComponent>& desc);
	//NETWORKING
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kInventoryStaticData; }
	virtual void ServerUpdateStaticData() { NetMarkAspectsDirty(kInventoryStaticData); }
	//rmi
	bool ServerSelectSlot(SlotPass&& p, INetChannel *);
	bool ServerSwapSlots(SwapSlotsParams&& p, INetChannel *);
	//~NETWORKING
	//Inventory
	bool ServerAddItem(SItemComponent *pNewItem);
	bool ServerAddWeapon(CWeaponComponent *pNewWeapon);
	bool ServerAddAmmoClip(CAmmoClipComponent *pNewAmmoClip);
	void ServerRemoveItem(SItemComponent *pItemToRemove);
	void ServerRemoveWeapon(CWeaponComponent *pNewWeapon);
	void ServerRemoveAmmoClip(CAmmoClipComponent *pNewAmmoClip);
	//Positive values add, negative values remove
	int ServerAddRemoveFreeBulletAmount(CAmmoPackComponent *pAmmoPack, int ammount);
	int ServerAddRemoveFreeBulletAmount(string bulletName, int ammount);
	int ServerGetOrCreateBulletGroup(string bulletName);
	int GetBulletsCount();
	string GetBulletGroupName(int index) { return bullet[index].first; }
	int GetBulletGroupAmmount(int index) { return bullet[index].second; }
	void ServerRemoveWeapon(int slotId) { weaponId[slotId] = 0; }
	void ServerRemoveAmmoClip(int slotId) { ammoClip[slotId] = 0; }
	void ClientSelectSlot(int slotId);
	int GetItemSlot(SItemComponent *pGetItem);
	int GetItemSlot(EntityId itemId);
	void SetCurrentItem(EntityId id);
	EntityId GetItemId(int slotId);
	SItemComponent *GetWeaponItem(int slotId);
	SItemComponent *GetAmmoClipItem(int slotId);
	EntityId GetAmmoClipItemId(int slotId);
	EntityId GetSelectedItemId() { if (activeSlot < 0) return 0; return weaponId[activeSlot]; }
	SItemComponent *GetSelectedItem();
	CWeaponComponent *HasWeaponSelected();
	CAmmoPackComponent *GetCarriedAmmoPack();
	void SetActiveSlot(int slot) { activeSlot = slot; }
	int GetActiveSlot() { return activeSlot; }
	int GetAmmoClipNumForCurrentWeapon();
	CAmmoClipComponent *TakeNextAmmoClip(CWeaponComponent *pWeaponRequestor);
	CAmmoClipComponent *TakeNextEmptyAmmoClip(CWeaponComponent *pWeaponRequestor);
	int FillAmmoClip(CAmmoClipComponent *pCurrentAmmoClip);
	string GetCurrentItemName();
	CWeaponComponent *GetSelectedWeapon();
	void ClearInventory();
	string SchematycEntityToShortName(string schematycName);
	void SelectSlot(int slotId);
	void SelectForeignItem(EntityId id);
	EntityId GetForeignItemId() { return foreignItem; }
	void ServerDropEverything();
	void ServerReset();
private:
	EntityId weaponId[WEAPON_CAPACITY], weaponIdComp[WEAPON_CAPACITY] = { 0 };
	EntityId ammoClip[AMMOCLIP_CAPACITY], ammoClipComp[AMMOCLIP_CAPACITY] = { 0 };
	std::pair<string, int> bullet[BULLET_TYPE_CAPACITY];
	int activeSlot = -1;
	//client only variable
	SItemComponent *pCurrentItem = nullptr;
	EntityId foreignItem = 0;
	EntityId foreignItemComp = 0;
public:
	bool bIsLocal = false;
};