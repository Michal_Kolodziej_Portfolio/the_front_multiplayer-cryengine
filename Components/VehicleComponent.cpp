#include "StdAfx.h"
#include "VehicleComponent.h"
#include "PlayerComponent.h"
#include "EngineComponent.h"
#include "UISystem/HudComponent.h"
#include "CryNetwork/Rmi.h"
#include "AccountComponent.h"
#include "GlobalResources.h"

void SVehicleComponent::InitClass()
{
	sItemProperties.sPickupLabel = "Press F to enter " + GetItemName();
	//reset properties
	sVehicleStartupProperties = sVehicleProperties;

	pEngine = m_pEntity->GetComponent<CEngineComponent>();
	pAnimations = m_pEntity->GetOrCreateComponentClass<SAdvancedAnimationComponent>();
	pAnimations->SetCharacterFile(GetProperties()->sRenderProperties.sCharPath.value.c_str());
	pAnimations->SetMannequinAnimationDatabaseFile(GetVehicleProperties()->sAnimationProperties.sDatabaseFile.value.c_str());
	pAnimations->SetControllerDefinitionFile(GetVehicleProperties()->sAnimationProperties.sDefaultScopeSettings.m_controllerDefinitionPath);
	pAnimations->SetDefaultScopeContextName(GetVehicleProperties()->sAnimationProperties.sDefaultScopeSettings.m_contextName);
	pAnimations->SetAnimationDrivenMotion(false);
	pAnimations->SetDefaultFragmentName(GetVehicleProperties()->sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
	pAnimations->LoadFromDisk();
	m_movementFragmentID = pAnimations->GetFragmentId(GetVehicleProperties()->sAnimationProperties.sDefaultScopeSettings.m_fragmentName);
	m_Backward = pAnimations->GetTagId("Backward");
}

void SVehicleComponent::ClassProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (sVehicleProperties != sVehiclePrevProperties)
		{
			sVehiclePrevProperties = sVehicleProperties;
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (!gEnv->bServer)
		{
			pAnimations->SetMotionParameter(eMotionParamID_TravelSpeed, sVehicleProperties.physicsSpeed);
			pAnimations->SetTagWithId(m_Backward, (GetActiveGear() < 0));
			const auto& desiredFragmentId = m_movementFragmentID;
			if (m_activeFragment != desiredFragmentId)
			{
				m_activeFragment = desiredFragmentId;
				pAnimations->QueueFragmentWithId(m_activeFragment);
			}
		}
		else
		{
			ServerMove();
			sVehicleProperties.physicsSpeed = GetPhysicsSpeed();
			NetMarkAspectsDirty(kVehicleAspect);
		}
		if (!pDriverAttachment)
		{
			if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
			{
				if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
				{
					string attName = "cabine_att_0" + ToString(0);
					pDriverAttachment = pMan->GetInterfaceByName(attName);
				}
			}
		}
		if (IEntity *pDriverEnt = gEnv->pEntitySystem->GetEntity(GetVehicleProperties()->cabineSlot[0]))
		{
			if (CPlayerComponent *pDriver = pDriverEnt->GetComponent<CPlayerComponent>())
			{
				if ((!(pDriver->GetInputFlags() & (ItemInputFlags)IEInputFlag::Action_MoveForward)) && (!(pDriver->GetInputFlags() & (ItemInputFlags)IEInputFlag::Action_MoveLeft)) && (!(pDriver->GetInputFlags() & (ItemInputFlags)IEInputFlag::Action_MoveRight)) && (!(pDriver->GetInputFlags() & (ItemInputFlags)IEInputFlag::Action_MoveBack)))
				{
					if (GetVehicleProperties()->fCurSpeed > 0.f)
						GetVehicleProperties()->fCurSpeed -= 0.1f;
					if (GetVehicleProperties()->fCurSpeed < 0.f)
						GetVehicleProperties()->fCurSpeed = 0.f;
				}
			}
		}
	}
	break;
	}
}

void SVehicleComponent::ServerPickup(CPlayerComponent * pNewOwner)
{	
	//vehicles are specific type of item and while it is considered pickable, it won't be attached to player at all, therefore physics is not disabled and extra logic is needed here
	if (!pNewOwner || !IsPickable())
		return;

	//it is pretty much handled by Enter method
	Enter(pNewOwner, GetNextEmptyCabineSlot());
}

bool SVehicleComponent::ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kVehicleAspect)
	{
		ser.BeginGroup("VehicleSynchronization");

		ser.Value("physicsSpeed", sVehicleProperties.physicsSpeed);

		ser.EndGroup();
	}
	if (aspect == kItemStaticData)
	{
		ser.BeginGroup("StaticData");

		for (int i = 0; i < 10; i++)
		{
			ser.Value("cabineSlot" + ToString(i), sVehicleProperties.cabineSlot[i]);
			ser.Value("cabineSlotComp" + ToString(i), sVehicleProperties.cabineSlotComp[i], 'eid');
		}

		ser.EndGroup();
	}
	return true;
}

bool SVehicleComponent::CanEnter(int cabine_slot)
{
	return true;
}

void SVehicleComponent::Enter(CPlayerComponent * pEnteringPlayer, int cabine_slot)
{
	if (!pEnteringPlayer || cabine_slot < 0)
		return;

	//if requested cabine slot is empty
	if (GetVehicleProperties()->cabineSlot[cabine_slot] <= 0)
	{
		//assign new player to it
		GetVehicleProperties()->cabineSlot[cabine_slot] = pEnteringPlayer->GetEntityId();
		GetVehicleProperties()->cabineSlotComp[cabine_slot] = pEnteringPlayer->GetEntityId();
		//set vehicle for player, for easy access
		pEnteringPlayer->SetVehicle(GetEntityId());
		if (cabine_slot == 0)
		{
			//if player is driver then give him authority over this vehicle
			DelegateAuthority(pEnteringPlayer->GetEntity()->GetNetEntity()->GetChannelId());
			//set flag is driver for player
			pEnteringPlayer->IsDriver(true);
		}
	}
	ServerUpdateStaticData();
}

void SVehicleComponent::Leave(EntityId id)
{
	if (id <= 0)
		return;

	for (int i = 0; i < GetVehicleProperties()->iCabineSlotsCount; i++)
	{
		if (GetVehicleProperties()->cabineSlot[i] == id)
		{
			Leave(i);
			return;
		}
	}
}

void SVehicleComponent::Leave(int cabine_slot)
{
	if (cabine_slot < 0)
		return;

	if (IEntity *pLeavingEntity = gEnv->pEntitySystem->GetEntity(GetVehicleProperties()->cabineSlot[cabine_slot]))
	{
		CPlayerComponent *pLeavingPlayer = pLeavingEntity->GetComponent<CPlayerComponent>();

		//zero out cabine array where it belongs
		GetVehicleProperties()->cabineSlot[cabine_slot] = 0;
		GetVehicleProperties()->cabineSlotComp[cabine_slot] = 0;
		//zero out vehicle and driver property; no matter what slot we're leaving, player is not driver anymore
		pLeavingPlayer->SetVehicle(0);
		pLeavingPlayer->IsDriver(false);
	}
	ServerUpdateStaticData();
}

Vec3 SVehicleComponent::GetCameraWorldPos()
{
	if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			string attName = "camera";
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attName))
			{
				return pAttachment->GetAttWorldAbsolute().t;
			}
		}
	}
	return Vec3();
}

Vec3 SVehicleComponent::GetCameraLocalPos()
{
	if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			string attName = "camera";
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attName))
			{
				return pAttachment->GetAttRelativeDefault().t;
			}
		}
	}
	return Vec3();
}

Quat SVehicleComponent::GetCameraRotation()
{
	if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			string attName = "camera";
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attName))
			{
				return pAttachment->GetAttWorldAbsolute().q;
			}
		}
	}
	return Quat();
}

QuatTS SVehicleComponent::GetCameraWorldAbsolute()
{
	if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			string attName = "camera";
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attName))
			{
				return pAttachment->GetAttWorldAbsolute();
			}
		}
	}
	return QuatTS();
}

void SVehicleComponent::ServerMove()
{
	if (pEngine)
	{
		if (sItemProperties.m_inputFlags & (ItemInputFlags)IEInputFlag::Action_MoveForward)
		{
			pEngine->OpenThrottle();
		}
		else if (!(sItemProperties.m_inputFlags & (ItemInputFlags)IEInputFlag::Action_MoveForward))
		{
			pEngine->CloseThrottle();
		}
		if (pEngine->IsWorking())
		{
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_pEntity->GetWorldRotation()));
			if (sItemProperties.m_inputFlags & (ItemInputFlags)IEInputFlag::Action_MoveLeft)
			{
				ypr.x += 0.01f;
			}
			if (sItemProperties.m_inputFlags & (ItemInputFlags)IEInputFlag::Action_MoveRight)
			{
				ypr.x -= 0.01f;
			}
			if ((sItemProperties.m_inputFlags & (ItemInputFlags)IEInputFlag::Action_MoveLeft) || (sItemProperties.m_inputFlags & (ItemInputFlags)IEInputFlag::Action_MoveRight))
			{
				Quat newOrient = Quat(CCamera::CreateOrientationYPR(ypr));
				m_pEntity->SetRotation(newOrient);
				sItemProperties.currentOrientation = newOrient;
				SvUpdatePosition();
			}
		}
		if (sItemProperties.m_inputFlags & (ItemInputFlags)IEInputFlag::Action_MoveBack)
		{

		}
	}
}

bool SVehicleComponent::IsEngineWorking()
{
	if (pEngine && pEngine->IsWorking())
		return true;
	return false;
}

void SVehicleComponent::ServerShutdownEngine()
{
	if (pEngine)
		pEngine->Stop();
}

void SVehicleComponent::ServerStartupEngine()
{
	if (pEngine)
		pEngine->Start();
}

void SVehicleComponent::ServerStartupEngineAbort()
{
	if (pEngine)
		pEngine->AbortStarting();
}

void SVehicleComponent::FireEngineNow()
{
	if (pEngine)
		pEngine->StartNow();
}

void SVehicleComponent::Accelerate()
{
	if (pEngine)
	{
		if (pEngine->GetProperties()->iCurrentRpm < pEngine->GetProperties()->iMaxRpm)
		{
			Vec3 vMovementDir = (pEngine->GetCurrentGear() < 0) ? (-m_pEntity->GetForwardDir()) : m_pEntity->GetForwardDir();
			
			const float fCurrentHorsePower = pEngine->GetCurrentHorsePower();
			Vec3 impulseDir = impulseDir = vMovementDir * (fCurrentHorsePower * 20.f);

			pe_action_impulse move_action;
			move_action.impulse = impulseDir;
			m_pEntity->GetPhysicalEntity()->Action(&move_action);
		}
	}
}

void SVehicleComponent::NextGear()
{
	if (pEngine)
		pEngine->NextGear();
}

void SVehicleComponent::PreviousGear()
{
	if (pEngine)
		pEngine->PreviousGear();
}

void SVehicleComponent::SetGear(int gear)
{
	if (pEngine)
		pEngine->SetGear(gear);

}

int SVehicleComponent::GetActiveGear()
{
	if (pEngine)
		return pEngine->GetCurrentGear();
	return -199;
}

int SVehicleComponent::GetNextEmptyCabineSlot()
{
	for (int i = 0; i < GetVehicleProperties()->iCabineSlotsCount; i++)
	{
		if (GetVehicleProperties()->cabineSlot[i] <= 0)
			return i;
	}
	return -1;
}

CPlayerComponent *SVehicleComponent::GetDriverPlayer()
{
	if (IEntity *pEntity = gEnv->pEntitySystem->GetEntity(GetVehicleProperties()->cabineSlot[0]))
		return pEntity->GetComponent<CPlayerComponent>();

	return nullptr;
}

float SVehicleComponent::GetPhysicsSpeed()
{
	if (IPhysicalEntity* pPhysicalEntity = m_pEntity->GetPhysicalEntity())
	{
		pe_status_dynamics dynStatus;
		if (pPhysicalEntity->GetStatus(&dynStatus))
		{
			return dynStatus.v.GetLength2D();
		}
	}
	return 0.f;
}

int SVehicleComponent::GetCurrentGearTypeGearCount()
{
	if (pEngine)
		return pEngine->GetCurrentGearTypeGearCount();

	return -1;
}

int SVehicleComponent::GetCurrentRpm()
{
	if (pEngine)
		return pEngine->GetProperties()->iCurrentRpm;

	return -1;
}

QuatTS SVehicleComponent::GetAttachmentWorldAbsolute(EntityId id)
{
	for (int i = 0; i < GetVehicleProperties()->iCabineSlotsCount; i++)
	{
		if (GetVehicleProperties()->cabineSlot[i] == id)
		{
			if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
			{
				if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
				{
					string attName = "cabine_att_0" + ToString(i);
					if (IAttachment *pAtt = pMan->GetInterfaceByName(attName))
					{
						return pAtt->GetAttWorldAbsolute();
					}
				}
			}
		}
	}
	return QuatTS();
}

void SVehicleComponent::ReflectType(Schematyc::CTypeDesc<SVehicleComponent>& desc)
{
	desc.SetGUID("{29548CAD-D95D-4391-905F-091946E8E373}"_cry_guid);
	desc.AddBase<SItemComponent>();
}

void SVehicleComponent::ReLoadPhysics()
{
	pe_player_dynamics playerDynamics;
	playerDynamics.mass = sItemProperties.sPhysicsProperties.fMass;
	playerDynamics.kAirControl = 1.f;
	playerDynamics.kAirResistance = 0.2f;
	playerDynamics.kInertia = 8.f;
	playerDynamics.kInertiaAccel = 8.f;
	//just physicalizing
	SEntityPhysicalizeParams params;
	params.type = PE_RIGID;
	params.mass = sItemProperties.sPhysicsProperties.fMass;
	params.pPlayerDynamics = &playerDynamics;
	GetEntity()->Physicalize(params);

}

void SVehicleComponent::ServerResetClass()
{
	sVehicleProperties = sVehicleStartupProperties;
	if (pEngine)
		pEngine->ServerReset();

	NetMarkAspectsDirty(kVehicleAspect);
}
