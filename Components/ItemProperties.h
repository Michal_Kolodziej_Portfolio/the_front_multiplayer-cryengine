/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores all item specific properties exposed or unexposed for editor

--------------------------------------------------------------------------------- */

#pragma once
#include <CrySchematyc/ResourceTypes.h>
#include <CrySchematyc/Reflection/TypeDesc.h>
#include "DefaultComponents/Input/InputComponent.h"

class CPlayerComponent;

typedef uint16 ItemInputFlags;
enum class IEInputFlag
	: ItemInputFlags
{
	Action_Shoot = 1 << 0,
	Action_Reload = 1 << 1,
	Action_SwitchFiremode = 1 << 2,
	Action_Place = 1 << 3,
	Action_EngineToggle = 1 << 4,
	Action_MoveForward = 1 << 5,
	Action_MoveBack = 1 << 6,
	Action_MoveLeft = 1 << 7,
	Action_MoveRight = 1 << 8,
	Action_GearUp = 1 << 9,
	Action_GearDown = 1 << 10,
};

enum ItemMeshSlot
{
	Geometry_slot = 0,
	Character_slot,
	Ragdoll_slot,
};
enum ItemInventoryUIType
{
	ITEM_SELECTABLE = 0,
	ITEM_AMMOCLIP
};
struct SRenderProperties
{
	inline bool operator==(const SRenderProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SRenderProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	Schematyc::GeomFileName sGeomPath;
	Schematyc::CharacterFileName sCharPath;
	Schematyc::MaterialFileName sMaterial;
};
static void ReflectType(Schematyc::CTypeDesc<SRenderProperties>& desc)
{
	desc.SetGUID("{9664D389-A32E-4FB1-9068-00CFD4A2ECB0}"_cry_guid);
	desc.SetLabel("Render properties");
	desc.SetDescription("Render properties");
	desc.AddMember(&SRenderProperties::sGeomPath, 'geom', "GeometryPath", "Geometry path", "Geometry is used when item is not selected", "");
	desc.AddMember(&SRenderProperties::sCharPath, 'char', "CharacterPath", "Character path", "Character is used when item is selected", "");
	desc.AddMember(&SRenderProperties::sMaterial, 'smat', "MaterialPath", "Material path", "Material that will be assigned to character and geometry", "");
}
struct SPhysicsProperties
{
	inline bool operator==(const SPhysicsProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SPhysicsProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	float fMass;
};
static void ReflectType(Schematyc::CTypeDesc<SPhysicsProperties>& desc)
{
	desc.SetGUID("{672721D1-73FF-48CF-9638-1742C2543597}"_cry_guid);
	desc.SetLabel("Physic properties");
	desc.SetDescription("Physic properties");
	desc.AddMember(&SPhysicsProperties::fMass, 'mass', "Mass", "Mass", "Weight of the weapon in kg", 0.f);
}
struct SItemProperties
{
	inline bool operator==(const SItemProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SItemProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	bool bIsPickable;
	SRenderProperties sRenderProperties, sPrevRenderProperties;
	SPhysicsProperties sPhysicsProperties, sPrevPhysicsProperties;
	//NON-EDITOR PROPERTIES
	bool bIsSelected = false;
	string sPickupLabel;
	bool bPhysics = true;
	Vec3 currentPosition = ZERO;
	Quat currentOrientation = IDENTITY;
	CPlayerComponent *pPlayerCarrier = nullptr;
	EntityId ownerId = 0;
	EntityId ownerIdComp = 0;
	bool bIsAttached = false;
	ItemInputFlags m_inputFlags, m_prevInputFlags;
};
static void ReflectType(Schematyc::CTypeDesc<SItemProperties>& desc)
{
	desc.SetGUID("{38CE3352-40E9-4007-A1BB-A0E7FB1E72AE}"_cry_guid);
	desc.SetLabel("Item properties");
	desc.SetDescription("Item properties");
	desc.AddMember(&SItemProperties::bIsPickable, 'pick', "Pickable", "Pickable", "Are players able to pick this item", true);
	desc.AddMember(&SItemProperties::sRenderProperties, 'rpr', "RenderProperties", "Render properties", "RenderProperties", SRenderProperties());
	desc.AddMember(&SItemProperties::sPhysicsProperties, 'ppr', "PhysicProperties", "Physic properties", "PhysicProperties", SPhysicsProperties());
}