#include "StdAfx.h"
#include "AmmoClipComponent.h"
#include "WeaponComponent.h"
#include "PlayerComponent.h"
#include "UISystem/HudComponent.h"
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include "CryNetwork/Rmi.h"

static void RegisterAmmoClip(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAmmoClipComponent));
		{

		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterAmmoClip);

void CAmmoClipComponent::ReflectType(Schematyc::CTypeDesc<CAmmoClipComponent>& desc)
{
	desc.SetGUID("{38D4A2AE-7F32-4457-878D-5D480E7D396D}"_cry_guid);
	desc.AddBase<SItemComponent>();
	desc.SetEditorCategory("Items");
	desc.SetLabel("Ammo Clip Component");
	desc.SetDescription("Weapon ammo clip");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CAmmoClipComponent::sItemProperties, 'ipr', "ItemProperties", "Item properties", "ItemProperties", SItemProperties());
	desc.AddMember(&CAmmoClipComponent::sAmmoClipProperties, 'apr', "AmmoClipProperties", "AmmoClip properties", "AmmoClip propereties", SAmmoClipProperties());
}

void CAmmoClipComponent::InitClass()
{
	sItemProperties.sPickupLabel = "Press F to pick up ammo clip for " + GetWeaponTypeName();
	//resetting
	sAmmoClipStartupProperties = sAmmoClipProperties;
	type_invUI = ITEM_AMMOCLIP;
}

void CAmmoClipComponent::ClassProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (sAmmoClipProperties != sAmmoClipPrevProperties)
		{
			sAmmoClipPrevProperties = sAmmoClipProperties;
		}
	}
	break;
	}
}

bool CAmmoClipComponent::ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kItemStaticData)
	{
		ser.BeginGroup("StaticData");

		//ser.Value("parentWeapon", parentWeapon);
		//ser.Value("parentWeaponComp", parentWeaponComp, 'eid');
		ser.Value("visible", sAmmoClipProperties.bVisible, 'bool');
		ser.Value("currentAmmo", sAmmoClipProperties.iCurrentAmmo);

		m_pEntity->Invisible(!sAmmoClipProperties.bVisible);

		if (!gEnv->bServer)
		{
			if (CPlayerComponent *pOwner = GetOwner())
			{
				if (pOwner->bIsLocal)
				{
					//we just update HUD ammo
					if (pOwner->GetHudComponent())
						pOwner->GetHudComponent()->UpdateAmmo();
				}
			}
		}

		ser.EndGroup();
	}
	return true;
}

int CAmmoClipComponent::ServerAddRemoveAmmo(int ammo)
{
	if (ammo == 0)
		return 0;

	int ammoReturn = 0;

	//ADDING
	if (ammo > 0)
	{
		//firstly we have to add current ammo to the ammo we want to add
		const int newAmmo = sAmmoClipProperties.iCurrentAmmo + ammo;
		//then remove it from max ammo; this will handle possibility that if we try to add more than its capacity, we will reduce it and grab max that we could possibly add
		int ammoAboveMax = sAmmoClipProperties.iMaxAmmo - newAmmo;
		//if ammo above the maximum is not negative value, then make it 0, so it doesn't increase amount of bullets added
		if (ammoAboveMax > 0)
			ammoAboveMax = 0;
		//now we add ammo that we want to add, to ammoAboveMax value to get value of possible amount to add; ammoAboveMax is negative if we try to add too much
		const int possibleAmmoAdd = ammo + ammoAboveMax;
		//now we got the ammo amount that ammo pack can handle at max
		//we will apply this ammo amount
		sAmmoClipProperties.iCurrentAmmo += possibleAmmoAdd;
		ammoReturn = possibleAmmoAdd;
	}
	//REMOVING
	else
	{
		//firstly we will add current ammo to the (negative) ammo amount that we want to remove
		const int newAmmo = sAmmoClipProperties.iCurrentAmmo + ammo;
		//then we have to check, if value is positive or zero, then newAmmo represents new ammo amount
		if (newAmmo >= 0)
		{
			sAmmoClipProperties.iCurrentAmmo = newAmmo;
			ammoReturn = (-ammo);
		}
		//but if it's negative, it means we are trying to remove more ammo than this ammo pack currently has, so in this case we will make current ammo 0, and return current ammo state(before removing)
		else
		{
			const int removedAmmo = sAmmoClipProperties.iCurrentAmmo;
			sAmmoClipProperties.iCurrentAmmo = 0;
			ammoReturn = removedAmmo;
		}
	}
	//serialize values for ammo pack
	ServerUpdateStaticData();
	return ammoReturn;
}

void CAmmoClipComponent::DropFromWeapon(CWeaponComponent *pWeaponParent)
{
	if (!pWeaponParent)
		return;

	//if ammo clip is attached to weapon
	IEntity *pParentWeaponEntity = pWeaponParent->GetEntity();

	if (ICharacterInstance *pChar = pParentWeaponEntity->GetCharacter(Character_slot))
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			//find attachment of this weapon
			if (IAttachment *pAttachment = pMan->GetInterfaceByName("mag_att"))
			{
				//we have to set up position for this ammo clip to attachent above, so it will look like it dropped from weapon
				m_pEntity->SetPos(pAttachment->GetAttWorldAbsolute().t + Vec3(0.f, 0.f, pAttachment->GetAttAbsoluteDefault().t.z));
				//obviously make it visible
				sAmmoClipProperties.bVisible = true;
				//it dropped, so let's make it pickable
				sItemProperties.bIsPickable = true;
				//enable physics and re-spawn (re-physicalizes this ammo clip)
				sItemProperties.bPhysics = true;
				//send over to clients
				ServerUpdateStaticData();
				//on server side, enable physics now, so we can apply some tiny impulse
				GetEntity()->EnablePhysics(true);
				//apply immpulse

			}
		}
	}
}

void CAmmoClipComponent::FillUp()
{
	//this will be used with ammo packs, but not implemented for multiplayer yet
	if (sAmmoClipProperties.iCurrentAmmo < sAmmoClipProperties.iMaxAmmo)
	{
		sAmmoClipProperties.iCurrentAmmo++;
	}
}

void CAmmoClipComponent::ServerResetClass()
{
	sAmmoClipProperties = sAmmoClipStartupProperties;
}
