#include "StdAfx.h"
#include "ItemComponent.h"


void SItemComponent::RegisterActions()
{

}

void SItemComponent::HandleInputFlagChange(ItemInputFlags flags, int activationMode, EInputFlagType type)
{
	if (!gEnv->bServer)
	{
		if (CPlayerComponent *pOwner = GetOwner())
		{
			if (pOwner->bFreezePlayer)
				return;
		}
	}

	switch (type)
	{
	case EInputFlagType::Hold:
	{
		if (activationMode == eIS_Released)
		{
			sItemProperties.m_inputFlags &= ~flags;
		}
		else
		{
			sItemProperties.m_inputFlags |= flags;
		}
	}
	break;
	case EInputFlagType::Toggle:
	{
		if (activationMode == eIS_Released)
		{
			sItemProperties.m_inputFlags ^= flags;
		}
	}
	break;
	}

	if (!gEnv->bServer)
		NetMarkAspectsDirty(kItemInput);
}
//THIS IS HANDLED ON THE SERVER SIDE ONLY

void SItemComponent::SERVER_REGISTER_ACTION(uint16 flag, std::function<void(int)> f)
{
	if ((sItemProperties.m_prevInputFlags & flag && (!(sItemProperties.m_inputFlags & flag))) || ((!(sItemProperties.m_prevInputFlags & flag)) && sItemProperties.m_inputFlags & flag))
	{
		sItemProperties.m_prevInputFlags ^= flag;
		if (sItemProperties.m_inputFlags & flag) f(eIS_Pressed);
		else f(eIS_Released);
	}
}

void SItemComponent::Client_Action_RenderPlacement(int activationMode)
{
	if (activationMode == eIS_Down)
	{
		if (sItemProperties.pPlayerCarrier)
		{
			IRenderAuxGeom* render = gEnv->pRenderer->GetIRenderAuxGeom();
			const Vec3 peakPos = sItemProperties.pPlayerCarrier->GetTargetPosition();
			render->DrawSphere(peakPos, 0.2f, ColorB(Col_Blue));
		}
	}
}
