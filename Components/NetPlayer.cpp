#include "PlayerComponent.h"
#include "WeaponComponent.h"
#include "GlobalResources.h"
#include "EngineComponent.h"

bool CPlayerComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kMovementAspect)
	{
		ser.BeginGroup("PlayerInput");

		auto inputs = m_inputFlags;
		auto prevState = m_inputFlags;

		ser.Value("sPlayerProperties.targetEntityId", sPlayerProperties.targetEntityId);
		ser.Value("sPlayerProperties.targetEntityIdComp", sPlayerProperties.targetEntityIdComp, 'eid');
		ser.Value("targetPosition", sPlayerProperties.vTargetPosition, 'wrld');

		if (gEnv->bServer)
		{
			IEntity *pItemEnt = Server_GetEntityById(sPlayerProperties.targetEntityId, sPlayerProperties.targetEntityIdComp);
			if (pItemEnt)
				sPlayerProperties.pTargetItem = pItemEnt->GetComponent<SItemComponent>();
			else
				sPlayerProperties.pTargetItem = nullptr;
		}


		ser.Value("m_inputFlags", m_inputFlags, 'ui8');

		if (ser.IsReading())
		{
			auto changedKeys = inputs ^ m_inputFlags;

			auto pressedKeys = changedKeys & inputs;
			if (pressedKeys != 0)
			{
				HandleInputFlagChange(pressedKeys, eIS_Pressed);
			}

			auto releasedKeys = changedKeys & prevState;
			if (releasedKeys != 0)
			{
				HandleInputFlagChange(pressedKeys, eIS_Released);
			}
		}

		ser.Value("m_lookOrientation", m_lookOrientation, 'ori3');
		ser.Value("cameraDir", sPlayerProperties.vCameraDir, 'dir3');

		ser.EndGroup();
	}
	if (aspect == kPlayerData)
	{
		//SERVER TO CLIENT DATA
		ser.BeginGroup("PlayerData");

		//account 
		ser.Value("accountId", sPlayerProperties.accountId);
		ser.Value("accountIdComp", sPlayerProperties.accountIdComp, 'eid');

		if (!pAccount)
		{
			IEntity *pEntity = Server_GetEntityById(sPlayerProperties.accountId, sPlayerProperties.accountIdComp);
			if (pEntity)
			{
				pAccount = pEntity->GetComponent<CAccountComponent>();
			}
		}

		//other
		ser.Value("visitedAmmoPackId", sPlayerProperties.visitedAmmoPackId);
		ser.Value("visitedAmmoPackIdComp", sPlayerProperties.visitedAmmoPackIdComp, 'eid');
		ser.Value("sPlayerProperties.vehicle", sPlayerProperties.vehicle);
		ser.Value("sPlayerProperties.vehicleComp", sPlayerProperties.vehicleComp, 'eid');
		ser.Value("isDriver", sPlayerProperties.bIsDriver, 'bool');

		//if player is in sPlayerProperties.vehicle, disable his physics
		m_pEntity->EnablePhysics(!(sPlayerProperties.vehicle > 0 || sPlayerProperties.vehicleComp > 0));

		//also if player is local, then update sPlayerProperties.vehicle info for him, and show the info if is in sPlayerProperties.vehicle and hide if he isn't
		if (IEntity *pVehicleEntity = Server_GetEntityById(sPlayerProperties.vehicle, sPlayerProperties.vehicleComp))
		{
			if (!gEnv->bServer && bIsLocal)
			{
				SVehicleComponent *vehiclePt = pVehicleEntity->GetComponent<SVehicleComponent>();
				GetHudComponent()->SetVehicle(vehiclePt->GetItemName(), vehiclePt->GetEngine()->GetProperties()->iCurrentRpm, vehiclePt->GetActiveGear(), vehiclePt->GetEngine()->GetCurrentGearTypeGearCount());
				GetHudComponent()->VehicleInfoShow(true);
			}
		}
		else
		{
			if(!gEnv->bServer && bIsLocal)
				GetHudComponent()->VehicleInfoShow(false);
		}

			if (!gEnv->bServer)
			{
				if (bIsLocal)
				{
					if (CAmmoPackComponent *ammoPack = GetVisitedAmmoPack())
					{
						//so now we will show ammo pack dialog if client asking is local client
						GetUISystem()->ShowAmmoPackDialog(true);
						SUIArguments args;
						args.AddArgument<int>(ammoPack->GetMaxAmmo());
						args.AddArgument<int>(ammoPack->GetCurrentAmmo());
						args.AddArgument<string>(ammoPack->GetBulletShortName());
						GetUISystem()->SetAmmoPackDialog(args);
						bFreezePlayer = true;
					}
					else
					{
						bFreezePlayer = false;
						GetUISystem()->ShowAmmoPackDialog(false);
					}
				}
			}
		ser.EndGroup();
	}
	if (aspect == kGameplayData)
	{
		//SERVER TO CLIENT DATA
		ser.BeginGroup("GameplayData");

		//gameplay
		ser.Value("iKills", sPlayerProperties.iKills);
		ser.Value("iDeaths", sPlayerProperties.iDeaths);

		ser.Value("isAlive", sPlayerProperties.bIsAlive, 'bool');

		m_pEntity->Invisible(!sPlayerProperties.bIsAlive);

		ser.EndGroup();
	}
	if (aspect == kVitalData)
	{
		//SERVER TO CLIENT DATA
		ser.BeginGroup("VitalData");

		//gameplay
		ser.Value("health", sPlayerProperties.sGameplayProperties.fHealth);
		ser.Value("stamina", sPlayerProperties.sGameplayProperties.fStamina);

		ser.Value("isAlive", sPlayerProperties.bIsAlive, 'bool');

		m_pEntity->Invisible(!sPlayerProperties.bIsAlive);

		if (!gEnv->bServer)
		{
			if (bIsLocal)
				GetHudComponent()->UpdateCurrentProgressBars();
		}

		ser.EndGroup();
	}
	return true;
}


