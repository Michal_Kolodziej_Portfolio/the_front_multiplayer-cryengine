#include "City.h"

#define MAX_CAPTURES 5

void CCityComponent::Initialize()
{
	m_pCaptureAreas = (CCaptureAreaComponent**)malloc(MAX_CAPTURES * sizeof(CCaptureAreaComponent*));
	for (int i = 0; i < MAX_CAPTURES; i++) {
		m_pCaptureAreas[i] = (CCaptureAreaComponent*)malloc(sizeof(CCaptureAreaComponent));
	}
}

void CCityComponent::OnShutDown()
{
	free(m_pCaptureAreas);
}

uint64 CCityComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE);
}

void CCityComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{

	}
	}
}
