#include "StdAfx.h"
#include "PlayerAreaTrigger.h"
#include "AmmoPackComponent.h"

#define MAX_PLAYERS 16

void CPlayerAreaTriggerComponent::Initialize() {

	//some of the lines below are server only properties; the reason for this is that only server stores info about players in area etc. that's why we don't need to bother players machines

	m_pAreaBox = (AABB*)malloc(sizeof(AABB));
	m_pAreaCylinder = (Cylinder*)malloc(sizeof(Cylinder));

	if (gEnv->bServer)
	{
		m_pPlayersInArea = (CPlayerComponent**)malloc(MAX_PLAYERS * sizeof(CPlayerComponent*));
		for (int i = 0; i < MAX_PLAYERS; i++) {
			m_pPlayersInArea[i] = (CPlayerComponent*)malloc(sizeof(CPlayerComponent));
		}
	}
	*m_pAreaBox = AABB(Vec3(0, 0, 0), Vec3(0, 0, 0));
	*m_pAreaCylinder = Cylinder();

	if (gEnv->bServer)
	{
		RemovePlayers();

		m_iPlayersInArea = 0;
	}

	if (m_CaptureAreaProperties.m_AreaShape == AreaShape::AREA_TYPE_BOX)
		SetBox(&AABB(GetEntity()->GetPos(), GetEntity()->GetPos() + m_CaptureAreaProperties.vMax));
	if (m_CaptureAreaProperties.m_AreaShape == AreaShape::AREA_TYPE_CYLINDER)
		SetCylinder(GetEntity()->GetWorldPos(), m_CaptureAreaProperties.fRadius, m_CaptureAreaProperties.fHeight);

#if DEBUG
	if (gEnv->bServer)
	{		
		//this is only test code, but it still needs to happen only on server
		//Test Code
		CPlayerComponent* testPlayer1 = &CPlayerComponent();
		CPlayerComponent* testPlayer2 = &CPlayerComponent();
		CPlayerComponent* testPlayer3 = &CPlayerComponent();
		AddPlayer(testPlayer1);
		AddPlayer(testPlayer2);
		AddPlayer(testPlayer2);
		RemovePlayer(testPlayer2);
		AddPlayer(testPlayer3);
		AddPlayer(testPlayer2);
		RemovePlayers();
		AddPlayer(testPlayer1);

		if (m_iPlayersInArea == 1) {
			CryLog("test success");
		}
		RemovePlayers();
	}
#endif
}

void CPlayerAreaTriggerComponent::OnShutDown() {

	//free(m_pAreaBox);
	//free(m_pAreaCylinder);
	//free(m_pPlayersInArea);

}

uint64 CPlayerAreaTriggerComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED);
}

void CPlayerAreaTriggerComponent::ProcessEvent(SEntityEvent& event) {

	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{

			//get all of the players in the area and add them to the list
			//server specific code
			if (gEnv->bServer)
			{
				auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
				pEntityIterator->MoveFirst();

				while (!pEntityIterator->IsEnd())
				{
					IEntity *pEntity = pEntityIterator->Next();

					if (CPlayerComponent* player = pEntity->GetComponent<CPlayerComponent>())
					{
						bool isInArea = false;

						switch (m_CaptureAreaProperties.m_AreaShape)
						{

						case AreaShape::AREA_TYPE_BOX:
							if (m_pAreaBox->IsContainPoint(pEntity->GetPos())) {
								isInArea = true;
							}
						case AreaShape::AREA_TYPE_CYLINDER:
							if (m_pAreaCylinder->IsContainPoint(pEntity->GetPos())) {
								isInArea = true;
							}
						}

						if (isInArea) {
							AddPlayer(player);
						}
						else {
							RemovePlayer(player);
						}
					}
				}
			}
	}
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (m_CaptureAreaProperties != m_CaptureAreaPrevProperties)
		{
			m_CaptureAreaPrevProperties = m_CaptureAreaProperties;

			if (m_CaptureAreaProperties.m_AreaShape == AreaShape::AREA_TYPE_BOX)
			{
				Vec3 vAreaMiddlePos = m_pEntity->GetWorldPos() - (m_CaptureAreaProperties.vMax * 0.5f);
				SetBox(&AABB(vAreaMiddlePos, vAreaMiddlePos + m_CaptureAreaProperties.vMax));
			}
			if (m_CaptureAreaProperties.m_AreaShape == AreaShape::AREA_TYPE_CYLINDER)
				SetCylinder(GetEntity()->GetWorldPos(), m_CaptureAreaProperties.fRadius, m_CaptureAreaProperties.fHeight);
		}
	}
	break;
	}
}

void CPlayerAreaTriggerComponent::AddPlayer(CPlayerComponent * component)
{
	//for now we're not doing separation server/client in here, because we already did it in init and update functions, 
	//but perhaps we could also here to make sure, that whenever we call it on client, it will fail
	if (m_iPlayersInArea < MAX_PLAYERS) {
		for (int i = 0; i < MAX_PLAYERS; i++) {
			
			if (!m_pPlayersInArea[i]) {

				m_pPlayersInArea[i] = component;
				m_iPlayersInArea++;

#if DEBUG
				//if (CPlayerComponent::GetLocalPlayer() && CPlayerComponent::GetLocalPlayer()->GetLogin() == "Fkhaller")
				CryLog("add " + ToString(i));
#endif
				break;
			}
			else if (m_pPlayersInArea[i] == component) {
				break;
			}
		}
	}

}
void CPlayerAreaTriggerComponent::RemovePlayer(CPlayerComponent * component)
{

	for (int i = 0; i < MAX_PLAYERS; i++) {
		if (m_pPlayersInArea[i] == component) {

			m_pPlayersInArea[i] = NULL;

			for (int j = i + 1; j < MAX_PLAYERS; j++) {
				if (m_pPlayersInArea[j]) {
					m_pPlayersInArea[j - 1] = m_pPlayersInArea[j];
					m_pPlayersInArea[j] = NULL;
				}
			}

	#if DEBUG
			//if (CPlayerComponent::GetLocalPlayer() && CPlayerComponent::GetLocalPlayer()->GetLogin() == "Fkhaller")
			CryLog("remove " + ToString(m_iPlayersInArea));
	#endif
			break;
		}
	}

}

void CPlayerAreaTriggerComponent::RemovePlayers()
{
	for (int i = 0; i < MAX_PLAYERS; i++) {
		m_pPlayersInArea[i] = NULL;
		m_iPlayersInArea = 0;
	}
}

void CPlayerAreaTriggerComponent::Render(ColorB color) const
{
	//this is client only ! Server can't render anything; also we want it to appear in editor too
	if (gEnv->IsEditor() || !gEnv->bServer)
	{
		IRenderAuxGeom* render = gEnv->pRenderer->GetIRenderAuxGeom();

		switch (m_CaptureAreaProperties.m_AreaShape)
		{

		case AreaShape::AREA_TYPE_BOX:
		{
			render->DrawAABB(*m_pAreaBox, false, color, eBBD_Faceted);
		}
		break;
		case AreaShape::AREA_TYPE_CYLINDER:
		{
			render->DrawCylinder(m_pAreaCylinder->position, Vec3(0, 0, 1), m_pAreaCylinder->radius, m_pAreaCylinder->height, color);
		}
		break;
		}
	}
}

void CPlayerAreaTriggerComponent::SetBox(const AABB * box)
{
	*m_pAreaBox = *box;
	m_CaptureAreaProperties.m_AreaShape = AreaShape::AREA_TYPE_BOX;
	if (gEnv->bServer)
		RemovePlayers();
}

void CPlayerAreaTriggerComponent::SetCylinder(Vec3 position, f32 radius, f32 height)
{

	m_pAreaCylinder->position = position;
	m_pAreaCylinder->radius = radius;
	m_pAreaCylinder->height = height;
	m_CaptureAreaProperties.m_AreaShape = AreaShape::AREA_TYPE_CYLINDER;
	if (gEnv->bServer)
		RemovePlayers();

}
