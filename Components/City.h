#pragma once

#include "CaptureArea.h"

class CCityComponent : public IEntityComponent
{

public:
	CCityComponent() = default;
	~CCityComponent() {};

	//IEntityComponent
	virtual void Initialize() override;
	virtual void OnShutDown() override;

	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	//~IEntityComponent

	static void ReflectType(Schematyc::CTypeDesc<CPlayerAreaTriggerComponent>& desc)
	{
		desc.SetGUID("{63F4C0C6-32AF-4ACB-8FB0-57D45DD62348}"_cry_guid);
	}

protected:
	CCaptureAreaComponent** m_pCaptureAreas;

};