/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores bullet properties exposed and unexposed for editor

--------------------------------------------------------------------------------- */

#pragma once
#include <CryEntitySystem/IEntityComponent.h>
#include <CryEntitySystem/IEntitySystem.h>
#include <CryPhysics/physinterface.h>
#include <CrySchematyc/ResourceTypes.h>

class CPlayerComponent;

struct SBulletProperties
{
	inline bool operator==(const SBulletProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SBulletProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	float fMass, fSize, fSpeed;
	Schematyc::GeomFileName sMesh;
};
static void ReflectType(Schematyc::CTypeDesc<SBulletProperties>& desc)
{

	desc.SetGUID("{358A16AA-B51C-4E9D-AC99-ACF9F97BF09A}"_cry_guid);
	desc.SetLabel("Bullet properties");
	desc.SetDescription("Bullet properties - FURY");
	desc.AddMember(&SBulletProperties::sMesh, 'smes', "BulletMesh", "Bullet model", "Model", "");
	desc.AddMember(&SBulletProperties::fMass, 'mass', "BulletMass", "Bullet mass", "Bullet weight in kg", 0.f);
	desc.AddMember(&SBulletProperties::fSize, 'size', "BulletSize", "Bullet size", "Bullet size", 0.f);
	desc.AddMember(&SBulletProperties::fSpeed, 'spd', "BulletSpeed", "Bullet speed", "Bullet speed", 0.f);
}