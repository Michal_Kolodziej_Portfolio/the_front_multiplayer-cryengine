#include "PlayerComponent.h"
#include "WeaponComponent.h"
#include "UISystem/HudComponent.h"
#include "AccountComponent.h"
#include "Cry3DEngine/ITimeOfDay.h"

#define MOUSE_DELTA_TRESHOLD 0.0001f
#define STAMINA_DRAIN_RATIO 0.1f



void CPlayerComponent::UpdateMovementRequest(float frameTime)
{
	if (!sPlayerProperties.bIsDriver)
	{
		if (!pController->IsOnGround())
			return;

		//jump handling
		if (m_inputFlags & (TInputFlags)EInputFlag::Jump)
		{
			if (!sPlayerProperties.bIsJumping)
			{
				if (IPhysicalEntity* pPhysicalEntity = m_pEntity->GetPhysicalEntity())
				{
					if (sPlayerProperties.GetStamina(false) >= 10.f)
					{
						sPlayerProperties.bIsJumping = true;
						pe_action_impulse impulse;
						impulse.impulse = Vec3(0, 0, 500);
						pPhysicalEntity->Action(&impulse);
						sPlayerProperties.UseStamina(10.f);
						sPlayerProperties.StopUseStamina();
					}
				}
			}
		}
		else
		{
			sPlayerProperties.bIsJumping = false;
		}

		Vec3 velocity = ZERO;
		//calculations for additional weight on player when he has items on him
		//i found 0.37 a good ratio for strength calculations; strength can still have visually nice looking numbers (like 10, 12, 15, 20 etc.) and internally be *0,37 to lower it; 0,37 makes player very weak at 10 strength, which is initial, it's good
		float fStrength = (sPlayerProperties.sGameplayProperties.iStrength * 0.37f);
		//it makes strength ratio higher, then the strength is lower and vice versa
		float fStrengthRatio = fStrength / (fStrength * fStrength);
		//we will multiply weight by strength ratio to get some real number we can remove later from the move speed
		float fAdditionalWeight = sPlayerProperties.sGameplayProperties.fAdditionalWeight * fStrengthRatio;
		//now we will need to find percentage that this additional weight is according to normal move speed; we need this to decrease sprint speed by the same percentage, because removing the same amount from sprint does not look like sprint is slower almost at all
		//this number would be simply lost in the high sprint speed, we we will use percentage instead
		float fPercentageMoveSpeed = fAdditionalWeight / sPlayerProperties.sGameplayProperties.fMoveSpeed;
		//if sprint bit is off, then current move speed is back to normal
		if (!(m_inputFlags & (TInputFlags)EInputFlag::Sprint))
		{
			sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed = sPlayerProperties.sGameplayProperties.fMoveSpeed;
			//stop use stamina causes regeneration to start
			sPlayerProperties.StopUseStamina();
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveForward)
		{
			//sprint is allowed only when running forward or forward + left/right; anytime that forward is not on, sprint is off
			if (CanSprint())
			{
				if (m_inputFlags & (TInputFlags)EInputFlag::Sprint)
				{
					//here we apply sprint speed as a normal speed for this player
					sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed = sPlayerProperties.sGameplayProperties.fSprintSpeed;
				}
				//if player can no longer sprint, we will get back to normal movement speed
				else
					sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed = sPlayerProperties.sGameplayProperties.fMoveSpeed;
			}
			else
			{
				sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed = sPlayerProperties.sGameplayProperties.fMoveSpeed;
				sPlayerProperties.bCanSprint = false;
				sPlayerProperties.StopUseStamina();
			}

			velocity.y += (sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed - (sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed * fPercentageMoveSpeed)) * frameTime;
		}
		//also there is possibility, that player releases forward button in the meantime, so we have to get back to normal move speed then
		if (!(m_inputFlags & (TInputFlags)EInputFlag::MoveForward))
		{
			sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed = sPlayerProperties.sGameplayProperties.fMoveSpeed;
			sPlayerProperties.StopUseStamina();
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveLeft)
		{
			velocity.x -= (sPlayerProperties.sGameplayProperties.fMoveSpeed - fAdditionalWeight) * frameTime;
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveRight)
		{
			velocity.x += (sPlayerProperties.sGameplayProperties.fMoveSpeed - fAdditionalWeight) * frameTime;
		}
		if (m_inputFlags & (TInputFlags)EInputFlag::MoveBack)
		{
			velocity.y -= (sPlayerProperties.sGameplayProperties.fMoveSpeed - fAdditionalWeight) * frameTime;
			sPlayerProperties.StopUseStamina();
		}
		//if outcome of that is that current speed is sprint speed, then drain stamina
		if (sPlayerProperties.sGameplayProperties.fCurrentMoveSpeed == sPlayerProperties.sGameplayProperties.fSprintSpeed)
		{
			sPlayerProperties.UseStamina(STAMINA_DRAIN_RATIO);
		}

		pController->AddVelocity(GetEntity()->GetWorldRotation() * velocity);
	}
}

void CPlayerComponent::UpdateLookDirectionRequest(float frameTime)
{
	const float rotationSpeed = 0.002f;
	const float rotationLimitsMinPitch = -0.84f;
	const float rotationLimitsMaxPitch = 1.5f;

	if (m_mouseDeltaRotation.IsEquivalent(ZERO, MOUSE_DELTA_TRESHOLD))
	{
		if (sPlayerProperties.bUnlockCamera && !sPlayerProperties.bCameraTimerSet)
		{
			sPlayerProperties.bCameraTimerSet = true;
			m_pEntity->SetTimer(99, 2000);

		}
		else if(!sPlayerProperties.bUnlockCamera)
		{
			if (IEntity *pVehicleEntity = gEnv->pEntitySystem->GetEntity(sPlayerProperties.vehicle))
			{
				SVehicleComponent *pVehicleIn = pVehicleEntity->GetComponent<SVehicleComponent>();

				Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(pVehicleIn->GetCameraRotation()));
				m_lookOrientation = Quat(CCamera::CreateOrientationYPR(ypr));
			}
		}
		return;
	}
	sPlayerProperties.bUnlockCamera = true;
	sPlayerProperties.bCameraTimerSet = false;
	m_pEntity->KillTimer(99);
	m_mouseDeltaRotation = m_mouseDeltaSmoothingFilter.Push(m_mouseDeltaRotation).Get();
	m_horizontalAngularVelocity = (m_mouseDeltaRotation.x * rotationSpeed) / frameTime;
	m_averagedHorizontalAngularVelocity.Push(m_horizontalAngularVelocity);
	Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_lookOrientation));
	ypr.x += m_mouseDeltaRotation.x * rotationSpeed;
	ypr.y = CLAMP(ypr.y + m_mouseDeltaRotation.y * rotationSpeed, rotationLimitsMinPitch, rotationLimitsMaxPitch);
	ypr.z = 0;
	m_lookOrientation = Quat(CCamera::CreateOrientationYPR(ypr));
	m_mouseDeltaRotation = ZERO;
}

void CPlayerComponent::UpdateAnimation(float frameTime)
{
	if (!gEnv->bServer)
	{
		bool bWeapon = (GetInventory()->GetSelectedItem()) ? true : false;
		SetAnimationTagID(m_weaponTagId, bWeapon);
		const float angularVelocityTurningThreshold = 0.174; 
		const bool isTurning = std::abs(m_averagedHorizontalAngularVelocity.Get()) > angularVelocityTurningThreshold;
		if (isTurning)
		{
			const float turnDuration = 1.0f;
			pAnimations->SetMotionParameter(eMotionParamID_TurnAngle, m_horizontalAngularVelocity * turnDuration);
		}
		const auto& desiredFragmentId = pController->IsWalking() ? m_walkFragmentId : m_idleFragmentId;
		if (m_activeFragmentId != desiredFragmentId)
		{
			m_activeFragmentId = desiredFragmentId;
			pAnimations->QueueFragmentWithId(m_activeFragmentId);
		}
	}
	Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_lookOrientation));
	ypr.y = 0;
	ypr.z = 0;
	const Quat correctedOrientation = Quat(CCamera::CreateOrientationYPR(ypr));
	GetEntity()->SetPosRotScale(GetEntity()->GetWorldPos(), correctedOrientation, Vec3(1, 1, 1));
}

void CPlayerComponent::UpdateCamera(float frameTime)
{
	if (IEntity *pVehicleEntity = gEnv->pEntitySystem->GetEntity(sPlayerProperties.vehicle))
	{
		if (!sPlayerProperties.bCameraTimerSet)
		{
			SVehicleComponent *pVehicleIn = pVehicleEntity->GetComponent<SVehicleComponent>();
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_lookOrientation));
			Matrix34 localTransform = IDENTITY;
			if (sPlayerProperties.bUnlockCamera)
			{
				ypr.x = 0;
				localTransform.SetRotation33(CCamera::CreateOrientationYPR(ypr));
			}

			if (ICharacterInstance *pCharacter = pVehicleEntity->GetCharacter(Character_slot))
			{
				Vec3 camPos = pVehicleIn->GetCameraLocalPos();
				localTransform.SetTranslation(camPos);
			}
			pCamera->SetTransformMatrix(localTransform);
		}
		return;
	}
	if (IsPlayerAlive())
	{
		if (!IsAiming())
		{
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_lookOrientation));
			ypr.x = 0;
			Matrix34 localTransform = IDENTITY;
			localTransform.SetRotation33(CCamera::CreateOrientationYPR(ypr));
			const float viewOffsetForward = 0.01f;
			const float viewOffsetUp = 0.26f;
			if (ICharacterInstance *pCharacter = pAnimations->GetCharacter())
			{
				const QuatT &cameraOrientation = pCharacter->GetISkeletonPose()->GetAbsJointByID(m_cameraJointId);
				localTransform.SetTranslation(cameraOrientation.t + Vec3(0, viewOffsetForward, viewOffsetUp));
			}
			pCamera->SetTransformMatrix(localTransform);
		}
		else
		{
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(m_lookOrientation));
			ypr.x = 0;
			Matrix34 localTransform = IDENTITY;
			localTransform.SetRotation33(CCamera::CreateOrientationYPR(ypr));
			const float viewOffsetRightLeft = fAimOffset.x;
			const float viewOffsetForward = 0.46f + fAimOffset.y;
			const float viewOffsetUp = fAimOffset.z;
			if (ICharacterInstance *pCharacter = pAnimations->GetCharacter())
			{
				const QuatT &cameraOrientation = pCharacter->GetISkeletonPose()->GetAbsJointByID(m_cameraJointId);
				localTransform.SetTranslation(cameraOrientation.t + Vec3(viewOffsetRightLeft, viewOffsetForward, viewOffsetUp));
			}
			pCamera->SetTransformMatrix(localTransform);
		}
	}
	//logic for camera attached to the teammates while this player is ragdolized
	else
	{
		//specific camera offset for following teammates
		const float viewOffsetForward = -1.5f;
		const float viewOffsetUp = 0.26f;
		//attaching camera to choosen teammate
		if (sPlayerProperties.pTeamMateToFollow[sPlayerProperties.iCurTeammateFollowed])
		{
			IEntity *pTeamMateEntity = sPlayerProperties.pTeamMateToFollow[sPlayerProperties.iCurTeammateFollowed]->GetEntity();
			Ang3 ypr = CCamera::CreateAnglesYPR(Matrix33(sPlayerProperties.pTeamMateToFollow[sPlayerProperties.iCurTeammateFollowed]->GetLookOrientation()));
			ypr.x = 0;
			Matrix34 localTransform = IDENTITY;
			localTransform.SetRotation33(CCamera::CreateOrientationYPR(ypr));
			if (ICharacterInstance *pCharacter = sPlayerProperties.pTeamMateToFollow[sPlayerProperties.iCurTeammateFollowed]->pAnimations->GetCharacter())
			{
				const QuatT &cameraOrientation = pCharacter->GetISkeletonPose()->GetAbsJointByID(m_cameraJointId);
				localTransform.SetTranslation(cameraOrientation.t + Vec3(0, viewOffsetForward, viewOffsetUp));
			}
			m_pEntity->SetPos(sPlayerProperties.pTeamMateToFollow[sPlayerProperties.iCurTeammateFollowed]->GetEntity()->GetWorldPos());
			m_pEntity->SetRotation(sPlayerProperties.pTeamMateToFollow[sPlayerProperties.iCurTeammateFollowed]->GetEntity()->GetWorldRotation());
			pCamera->SetTransformMatrix(localTransform);
			//label over followed teammate's head
			Vec3 screenPos(ZERO);
			gEnv->pRenderer->ProjectToScreen(pTeamMateEntity->GetWorldPos().x, pTeamMateEntity->GetWorldPos().y, pTeamMateEntity->GetWorldPos().z + 2.f, &screenPos.x, &screenPos.y, &screenPos.z);
			screenPos.x = screenPos.x * 0.01f * gEnv->pRenderer->GetWidth();
			screenPos.y = screenPos.y * 0.01f * gEnv->pRenderer->GetHeight();
			ColorF pfWhiteColor = { 1.f, 1.f, 1.f, 1.f };

			const char *label = sPlayerProperties.pTeamMateToFollow[sPlayerProperties.iCurTeammateFollowed]->GetEntity()->GetName();
			gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenPos.x, screenPos.y, 1.5f, pfWhiteColor, true, label);
		}
	}
}

bool CPlayerComponent::CanSprint()
{
	return GetProperties()->GetStamina(false) >= STAMINA_DRAIN_RATIO && sPlayerProperties.bCanSprint;
}

CAmmoPackComponent * CPlayerComponent::GetVisitedAmmoPack()
{
	if (IEntity *pAPEntity = gEnv->pEntitySystem->GetEntity(sPlayerProperties.visitedAmmoPackId))
		return pAPEntity->GetComponent<CAmmoPackComponent>();
	
	return nullptr;
}

void CPlayerComponent::ClientUpdate(float frameTime)
{
	if (bIsLocal)
	{
		ray_hit hit;
		Vec3 cameraDir = ZERO;
		if (SItemComponent *item = GetInventory()->GetSelectedItem())
		{
			if (CWeaponComponent *pWeapon = item->IsWeapon())
				cameraDir = Vec3(pWeapon->GetBulletOutputOrientation().GetFwdX(), gEnv->pSystem->GetViewCamera().GetViewdir().y, gEnv->pSystem->GetViewCamera().GetViewdir().z);
			else
				cameraDir = gEnv->pSystem->GetViewCamera().GetViewdir();
		}
		else
			cameraDir = gEnv->pSystem->GetViewCamera().GetViewdir();

		if (sPlayerProperties.vCameraDir != cameraDir)
		{
			sPlayerProperties.vCameraDir = cameraDir;
			ClientUpdateMovement();
		}

		Vec3 cameraPos = gEnv->pSystem->GetViewCamera().GetPosition() + cameraDir;
		Vec3 hitPoint = (cameraDir * 100.f);
		static const unsigned int rayflags = rwi_stop_at_pierceable | rwi_colltype_any;
		if (gEnv->pPhysicalWorld->RayWorldIntersection(cameraPos, hitPoint, ent_all, rayflags, &hit, 1, GetEntity()->GetPhysicalEntity()))
		{
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(hit.pt - Vec3(.0f, .0f, 1.0f), ColorB(255, 0, 0), hit.pt + Vec3(.0f, .0f, 1.0f), ColorB(255, 0, 0), 6.0f);
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(hit.pt - Vec3(.0f, 1.0f, .0f), ColorB(255, 0, 0), hit.pt + Vec3(.0f, 1.0f, .0f), ColorB(255, 0, 0), 6.0f);
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawLine(hit.pt - Vec3(1.0f, .0f, .0f), ColorB(255, 0, 0), hit.pt + Vec3(1.0f, .0f, .0f), ColorB(255, 0, 0), 6.0f);
			
			if (hit.dist < 2)
				sPlayerProperties.vTargetPosition = hit.pt;
			else
				sPlayerProperties.vTargetPosition = m_pEntity->GetWorldPos() + (m_pEntity->GetForwardDir() * 2.f);

			if (hit.pCollider)
			{
				if (IEntity *pColliderEntity = gEnv->pEntitySystem->GetEntityFromPhysics(hit.pCollider))
				{
					if (SItemComponent *pItem = pColliderEntity->GetComponent<SItemComponent>())
					{
						//limit how long the players arms are
						if (pItem->GetProperties()->bIsPickable && hit.dist < 2)
						{
							sPlayerProperties.pTargetItem = pItem;
							ShowPickUpMessage(pItem->GetItemName());
						}
					}
					else 
					{
						sPlayerProperties.pTargetItem = nullptr;
						//Label over teammates heads if we target them
						if (CPlayerComponent *pPl = pColliderEntity->GetComponent<CPlayerComponent>())
						{
							if (!pPl->bIsLocal)
							{
								if (pPl->GetTeam() == GetTeam())
								{
									Vec3 screenPos(ZERO);
									gEnv->pRenderer->ProjectToScreen(pColliderEntity->GetWorldPos().x, pColliderEntity->GetWorldPos().y, pColliderEntity->GetWorldPos().z + 2.f, &screenPos.x, &screenPos.y, &screenPos.z);
									screenPos.x = screenPos.x * 0.01f * gEnv->pRenderer->GetWidth();
									screenPos.y = screenPos.y * 0.01f * gEnv->pRenderer->GetHeight();
									ColorF pfWhiteColor = { 1.f, 1.f, 1.f, 1.f };

									const char *label = pColliderEntity->GetName();
									const char *label2 = pPl->GetPlayerClassName().c_str();
									gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenPos.x, screenPos.y, 1.5f, pfWhiteColor, true, label2);
									gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(screenPos.x, screenPos.y - 15.f, 1.5f, pfWhiteColor, true, label);
								}
							}
						}
					}
				}
				else
				{
					sPlayerProperties.pTargetItem = nullptr;
				}
			}
			else
			{
				sPlayerProperties.pTargetItem = nullptr;
			}
		}
		else
		{
			sPlayerProperties.vTargetPosition = m_pEntity->GetWorldPos() + (m_pEntity->GetForwardDir() * 2.f);
		}
		if (sPlayerProperties.pLastTargetItem != sPlayerProperties.pTargetItem)
		{
			sPlayerProperties.pLastTargetItem = sPlayerProperties.pTargetItem;
			if (sPlayerProperties.pTargetItem)
			{
				sPlayerProperties.targetEntityId = sPlayerProperties.pTargetItem->GetEntityId();
				sPlayerProperties.targetEntityIdComp = sPlayerProperties.targetEntityId;
			}
			else
			{
				sPlayerProperties.targetEntityId = 0;
				sPlayerProperties.targetEntityIdComp = 0;
			}
			ClientUpdateMovement();
		}
		if (sPlayerProperties.vTargetPositionLast != sPlayerProperties.vTargetPosition)
		{
			sPlayerProperties.vTargetPositionLast = sPlayerProperties.vTargetPosition;
			ClientUpdateMovement();
		}
	}
	//UPDATE HUD
	if (sPlayerProperties.bHudChanged)
	{
		sPlayerProperties.bHudChanged = false;
		if (GetHudComponent())
		{
			GetHudComponent()->UpdateCurrentProgressBars();
		}

	}
	if (GetHudComponent())
	{
		if (IEntity *pVehicleEntity = gEnv->pEntitySystem->GetEntity(sPlayerProperties.vehicle))
		{
			SVehicleComponent *pVehicleIn = pVehicleEntity->GetComponent<SVehicleComponent>();
			GetHudComponent()->UpdateVehicleInfo(pVehicleIn->GetCurrentRpm(), pVehicleIn->GetActiveGear(), pVehicleIn->GetCurrentGearTypeGearCount());
			return;
		}
		GetHudComponent()->VehicleInfoShow(false);
	}
}