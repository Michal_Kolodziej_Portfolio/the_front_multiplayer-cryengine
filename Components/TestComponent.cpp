#include "StdAfx.h"
#include "TestComponent.h"
#include "CryEntitySystem/IEntity.h"

static void RegisterTest(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CTestComponent));
		// Functions
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterTest)

void CTestComponent::Initialize()
{
	m_pEntity->LoadGeometry(0, "Objects/Default/primitive_cube.cgf");
	if (gEnv->bServer)
		gEnv->pNetContext->BindObject(GetEntityId(), 0, GetNetSerializeAspectMask(), true);
	m_pEntity->GetNetEntity()->BindToNetwork();
	m_pEntity->UpdateComponentEventMask(this);
	CryLogAlways("TestEntity bound to network");
}

void CTestComponent::ReflectType(Schematyc::CTypeDesc<CTestComponent>& desc)
{
	desc.SetGUID("{6B654CA8-126D-43C7-86FA-C1B807413004}"_cry_guid);
	desc.SetEditorCategory("Tests");
	desc.SetLabel("Test component");
	desc.SetDescription("Test networking component");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
}

uint64 CTestComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_UPDATE);
}

void CTestComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_UPDATE:
	{
		//if (gEnv->bServer)
		//{
		//	serverInt++;
		//	UpdateServerAspect();
		//}
		//else
		//{
		//	clientInt++;
		//	UpdateClientAspect();
		//}
	}
	break;
	}
}

bool CTestComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kClientToServer)
	{
		ser.BeginGroup("ClientToServer");

		ser.Value("clientInt", clientInt);

		CryLogAlways("Client to server data = %d", clientInt);

		ser.EndGroup();
	}
	if (aspect == kServerToClient)
	{
		ser.BeginGroup("ServerToClient");

		ser.Value("serverInt", serverInt);

		CryLogAlways("Server to client data = %d", serverInt);

		ser.EndGroup();
	}
	return true;
}
