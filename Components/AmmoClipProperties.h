/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores all magazine(ammo clip) properties exposed or unexposed for editor

--------------------------------------------------------------------------------- */

#include "ItemComponent.h"

struct SAmmoClipProperties
{
	inline bool operator==(const SAmmoClipProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SAmmoClipProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	Schematyc::EntityClassName sBulletName;
	Schematyc::EntityClassName sWeaponName;
	int iMaxAmmo;
	int iCurrentAmmo;
	//non editor
	bool bVisible = true;
};
static void ReflectType(Schematyc::CTypeDesc<SAmmoClipProperties>& desc)
{
	desc.SetGUID("{C5D250CE-BCF9-49A4-AA14-93789E3FD8A2}"_cry_guid);
	desc.SetLabel("AmmoClip properties");
	desc.SetDescription("AmmoClip properties - FURY");
	desc.AddMember(&SAmmoClipProperties::sBulletName, 'bnam', "BulletType", "Bullet type", "Type of the bullet", "");
	desc.AddMember(&SAmmoClipProperties::sWeaponName, 'wnam', "WeaponName", "Weapon type", "Type of the weapon", "");
	desc.AddMember(&SAmmoClipProperties::iMaxAmmo, 'imax', "MaxAmmo", "Capacity", "Max ammo amount in this magazine", 0);
	desc.AddMember(&SAmmoClipProperties::iCurrentAmmo, 'icur', "CurrentAmmo", "Current ammo", "Current ammo amount in this ammo pack", 0);
}