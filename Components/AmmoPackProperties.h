/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Stores ammo pack properties exposed and unexposed for editor

--------------------------------------------------------------------------------- */

#pragma once
#include "ItemComponent.h"

struct SAmmoPackProperties
{
	inline bool operator==(const SAmmoPackProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SAmmoPackProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	Schematyc::EntityClassName sBulletName;
	int iMaxAmmo;
	int iCurrentAmmo;
	int iTeam = -1;
};
static void ReflectType(Schematyc::CTypeDesc<SAmmoPackProperties>& desc)
{
	desc.SetGUID("{371F4AAA-298F-4DB1-AEB1-A9D8C28E5C96}"_cry_guid);
	desc.SetLabel("Ammo pack properties");
	desc.SetDescription("Ammo pack properties - FURY");
	desc.AddMember(&SAmmoPackProperties::sBulletName, 'bnam', "BulletType", "Bullet type", "Type of the bullet", "");
	desc.AddMember(&SAmmoPackProperties::iMaxAmmo, 'imax', "MaxAmmo", "Capacity", "Max ammo amount in this ammo pack", 0);
	desc.AddMember(&SAmmoPackProperties::iCurrentAmmo, 'icur', "CurrentAmmo", "Current ammo", "Current ammo amount in this ammo pack", 0);
}
