#include "StdAfx.h"
#include "WeaponComponent.h"
#include "FiremodeComponent.h"
#include "UISystem/HudComponent.h"
#include "CameraComponent.h"
#include <CryNetwork/Rmi.h>

static void RegisterWeapon(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CWeaponComponent));
		{

		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterWeapon);

void CWeaponComponent::ReflectType(Schematyc::CTypeDesc<CWeaponComponent>& desc)
{
	desc.SetGUID("{FDDA1A5D-7360-4B2F-AC2F-44D71AA48303}"_cry_guid);
	desc.AddBase<SItemComponent>();
	desc.SetEditorCategory("Items");
	desc.SetLabel("Weapon");
	desc.SetDescription("Weapon system");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CWeaponComponent::sItemProperties, 'ipr', "ItemProperties", "Item properties", "ItemProperties", SItemProperties());
	desc.AddMember(&CWeaponComponent::sWeaponProperties, 'wpr', "WeaponProperties", "Weapon properties", "WeaponProperties", SWeaponProperties());
}

void CWeaponComponent::InitClass()
{
	//each class that inherites from SItemComponent structure has to have InitClass method implemented; all it does it sets up audio, fire mode components and pickup label
	pAudio = GetEntity()->GetOrCreateComponentClass<SEntityAudioComponent>(sWeaponProperties.sFireTrigger.m_name.c_str(), sWeaponProperties.sDryFireTrigger.m_name.c_str(), sWeaponProperties.sReloadTrigger.m_name.c_str());
	pFireModes = GetEntity()->GetOrCreateComponentClass<CFireModeComponent>(this, int(sWeaponProperties.eFireMode));
	sItemProperties.sPickupLabel = "Press F to pick up " + GetItemName();
	//reset properties
	sWeaponStartupProperties = sWeaponProperties;
	type_invUI = ITEM_SELECTABLE;

	SRmi<RMI_WRAP(&CWeaponComponent::ClientLaunch)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
}

void CWeaponComponent::ClassProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == 27)
		{
			//simple bullet reloading needed for automatic fire modes
			ReloadBullet();
		}
	}
	break;
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (sWeaponProperties != sPrevWeaponProperties)
		{
			sPrevWeaponProperties = sWeaponProperties;
			pAudio->SetNameAndId(sWeaponProperties.sFireTrigger.m_name.c_str(), sWeaponProperties.sDryFireTrigger.m_name.c_str(), sWeaponProperties.sReloadTrigger.m_name.c_str());
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (sWeaponProperties.bFire)
		{
			pFireModes->StartFire();
		}
	}
	break;
	}
}

bool CWeaponComponent::ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kItemStaticData)
	{
		ser.BeginGroup("StaticData");

		EntityId prevAmmoClip = sWeaponProperties.currentAmmoClip;

		ser.Value("ammoClipId", sWeaponProperties.currentAmmoClip);
		ser.Value("ammoClipIdComp", sWeaponProperties.currentAmmoClipComp, 'eid');

		if (gEnv->bServer)
		{
			if (GetCurrentAmmoClip())
			{
				GetCurrentAmmoClip()->sAmmoClipProperties.bVisible = false;
				GetCurrentAmmoClip()->ServerUpdateStaticData();
			}
		}

		if (ser.IsReading() && !gEnv->bServer)
		{
			if (!GetCurrentAmmoClip())
			{
				ReleaseAmmoClipAttachment();
			}
			else
			{
				AttachAmmoClip();
			}

			//and update HUD; Hud will return pointer to hud component only if owner is local player so we don't have to handle bIsLocal all the time
			if (CPlayerComponent *ownerPlayer = GetOwner())
			{
				sItemProperties.bIsAttached = true;
				if (ownerPlayer->GetHudComponent())
					ownerPlayer->GetHudComponent()->UpdateAmmo();
			}
			else
				sItemProperties.bIsAttached = false;
		}

		ser.EndGroup();
	}
	return true;
}

bool CWeaponComponent::ClientLaunch(BooleanPass && p, INetChannel *)
{
	//Play shoot effects on client
	if (CPlayerComponent *pOwner = GetOwner())
	{
		if (p.boolean)
			pAudio->Play(SOUND_Fire);
		else
			pAudio->Play(SOUND_Fire_dry);
		//this is client action method to create shoot effects on client side
		//simple recoil calculation; it is based on bullet mass and other bullet properties as well as weapon mass
		//float recoil = (pBul->GetBulletRecoil() - sItemProperties.sPhysicsProperties.fMass) / 60.f;
		//sWeaponProperties.fRecoilAdd += 0.02;
		//recoil += sWeaponProperties.fRecoilAdd;
		////apply recoil to owner of this weapon; for now only camera is moving, but later we will have to hook it up to aim poses blendspace
		//pOwner->ApplyRecoil(recoil);
	}
	return true;
}

void CWeaponComponent::ServerFire()
{
	sWeaponProperties.bFire = true;
}

void CWeaponComponent::ServerStopFire()
{
	sWeaponProperties.bFire = false;
	sWeaponProperties.fRecoilAdd = 0.f;
	sWeaponProperties.iSerieCur = 0;
	pFireModes->StopFire();
}

void CWeaponComponent::SpawnBullet()
{
	CAmmoClipComponent *pCurAmmoClip = GetCurrentAmmoClip();
	if (!pCurAmmoClip)
		return;

	if (CPlayerComponent *pOwner = GetOwner())
	{
		//simple bullet spawning 
		if (ICharacterInstance *pChar = GetEntity()->GetCharacter(Character_slot))
		{
			if (auto *pBarrelOutAttachment = pChar->GetIAttachmentManager()->GetInterfaceByName("barrel_out"))
			{
				if (IEntityClass *pBulletClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(pCurAmmoClip->sAmmoClipProperties.sBulletName.value.c_str()))
				{
					if (CAttachmentComponent *pAttachments = pOwner->GetAttachmentComponent())
					{		
						//firstly we have get real barrel out attachment position, because since weapon is attached to the player, its position isn't updated
						Vec3 localBarrelOut = pBarrelOutAttachment->GetAttRelativeDefault().t;
						Vec3 playerWeaponAttWorldPos = pAttachments->GetAttachmentPositionForItem(this->IsWeapon());
						const Vec3 barrel_out_att_worldpos = localBarrelOut + playerWeaponAttWorldPos;

						//this is CORRECT direction and position for bullet
						QuatTS bulletOrigin = pBarrelOutAttachment->GetAttWorldAbsolute();
						SEntitySpawnParams params;
						params.pClass = pBulletClass;
						params.sName = "Bullet";
						params.nFlags |= ENTITY_FLAG_NEVER_NETWORK_STATIC;
						params.vPosition = barrel_out_att_worldpos;
						params.qRotation = bulletOrigin.q;
						IEntity *pBullet = gEnv->pEntitySystem->SpawnEntity(params);
						CBulletComponent *pBul = pBullet->GetComponent<CBulletComponent>();
						//we set owner for bullet, because if bullet hits anyone, we need to know who's killer, and who's victim in order to set kills/deaths
						IEntity *pOwnerEnt = gEnv->pEntitySystem->GetEntity(sItemProperties.ownerId);
						if (pOwnerEnt && pBul)
						{
							//no need to serialize owner of the bullet, cause it's important only on server side to count who killed who
							pBul->SetOwner(pOwner);
							//this is physicalizing our bullet it also give the bullet right heading direction
							//the thing about heading direction is, that at the moment we don't have aim poses, and thus we wouldn't be able to aim up and down
							//that's why we will use bullet direction from owner, but later when aim poses are made, we will have to use weapon direction
							Vec3 bulletDirection = pOwner->GetProperties()->vCameraDir;
							pBul->ReLoadBullet(bulletDirection);
						}
					}
				}
			}
		}
	}
}

void CWeaponComponent::StopFire()
{
	IEntity *pOwnerEnt = gEnv->pEntitySystem->GetEntity(sItemProperties.ownerId);
	if (pOwnerEnt)
	{
		if (CPlayerComponent *pOwner = pOwnerEnt->GetComponent<CPlayerComponent>())
		{
			//we need to call stop fire on server in order to prevent multiple dry shoots from happening
			//SRmi<RMI_WRAP(&CPlayerComponent::SvStopFire)>::InvokeOnServer(pOwner, NoParams{});
			//zero out recoil as well as serie fire mode

		}
	}
}

void CWeaponComponent::ServerLaunch(bool real)
{
	if (real)
	{
		if (CAmmoClipComponent *pCurrentAmmoClip = GetCurrentAmmoClip())
		{
			if (!pCurrentAmmoClip->IsEmpty())
			{
				SRmi<RMI_WRAP(&CWeaponComponent::ClientLaunch)>::InvokeOnAllClients(this, BooleanPass{ real });
				//spawn bullet on server side
				SpawnBullet();
				pCurrentAmmoClip->DecreaseAmmo();
				return;
			}
		}
	}
	else
	{
		SRmi<RMI_WRAP(&CWeaponComponent::ClientLaunch)>::InvokeOnAllClients(this, BooleanPass{ real });
	}
}

void CWeaponComponent::LaunchDry()
{
	//plays dry sound when server does not allow player to take a shot
	pAudio->Play(SOUND_Fire_dry);
}

void CWeaponComponent::ReloadBullet()
{
	//simply reloads bullet to make player able to take another shot
	pFireModes->bBulletReady = true;
}

void CWeaponComponent::StartAim()
{
	//aiming will be less complicated when aim poses are here; now we are using some stupid calc for offset to the aim_pos attachment;
	//when aim poses are implemented, we will be able to just stick camera onto the aim_pos attachment along with attachment's orientation
	if (GetEntity()->GetCharacter(Character_slot))
	{
		auto *pAimAttachment = GetEntity()->GetCharacter(Character_slot)->GetIAttachmentManager()->GetInterfaceByName("aim_pos");
		if (pAimAttachment)
		{
			IEntity *pOwnerEnt = gEnv->pEntitySystem->GetEntity(sItemProperties.ownerId);
			if (pOwnerEnt)
			{
				if (CPlayerComponent *pOwner = pOwnerEnt->GetComponent<CPlayerComponent>())
				{
					//get aim_pos attachment position on the model
					Vec3 attLocalPos = pAimAttachment->GetAttModelRelative().t;
					//get owner camera joint position local
					Vec3 camLocalPos = pOwner->GetCameraLocalJointPos();
					//get weapon attachment local position (relative to player character)
					Vec3 weaponOnPlayer = pOwner->GetAttachmentComponent()->GetWeaponAttachmentLocalPos();
					//calculate difference between weapon attachment and aim pos attachment
					Vec3 attOnPlayer = Vec3(weaponOnPlayer.x + attLocalPos.x, weaponOnPlayer.y - attLocalPos.y, weaponOnPlayer.z + attLocalPos.z);
					//calculate difference between attachent (related to player) and local camera position(also related to player)
					Vec3 diffOnPlayer = attOnPlayer - camLocalPos;
					//apply aim; this is stupid way of doing this, but without aim poses, we will have to make a lot of workarounds like this
					pOwner->Aim(diffOnPlayer.x, (-diffOnPlayer.y), diffOnPlayer.z, 0.02f);
					pOwner->IsAiming(true);
				}
			}
		}
	}
}

void CWeaponComponent::StopAim()
{
	IEntity *pOwnerEnt = gEnv->pEntitySystem->GetEntity(sItemProperties.ownerId);
	if (pOwnerEnt)
	{
		if (CPlayerComponent *pOwner = pOwnerEnt->GetComponent<CPlayerComponent>())
		{
			//reset offset of camera back to normal
			pOwner->IsAiming(false);
			pOwner->Aim(0.0f, 0.0f, 0.0f);
		}
	}
}

void CWeaponComponent::ServerReload()
{
	if (CPlayerComponent *pOwner = GetOwner())
	{
		CInventoryComponent *pInventory = pOwner->GetInventory();
		//if weapon has currently ammo clip attached
		if (CAmmoClipComponent *pCurrentAmmoClip = GetCurrentAmmoClip())
		{
			//then check if owner has some more ammo clipses in inventory
			if (CAmmoClipComponent *pNextAmmoClip = pInventory->TakeNextAmmoClip(this))
			{
				//if next ammo clip is found in owner inventory, check if the newer one has more ammo than current
				if (pNextAmmoClip->GetCurrentAmmo() > pCurrentAmmoClip->GetCurrentAmmo())
				{
					//if new one has more ammo than current, it will be good option to attach it instead, so drop current to the ground
					pCurrentAmmoClip->DropFromWeapon(this);
					//assign new ammo clip as the current one
					SetCurrentAmmoClip(pNextAmmoClip->GetEntityId());
					//send info over to clients
					ServerUpdateStaticData();
				}
				//return avoids next scenarios to happen; whole thing is basically made to fit the dangerous situations in game, for example when you see you have a few more enemies approaching and -
				// - and you have just a few bullets left, you are sure they aren't enough to kill enemies, so you quickly swap for the better ammo clip to survive
				return;
			}
			//now if no additional ammo clip was found in owners inventory, then check whether our current ammo clip is full, and if it isn't, attempt FillAmmoClip
			//it will use free bullets in players "pockets" to fill up the current ammo clip
			if (!GetCurrentAmmoClip()->IsFull())
			{
				//Fill magazine; Remove currently left capacity of the ammo clip from inventory of the player
				const int capacityLeft = pCurrentAmmoClip->GetMaxAmmo() - pCurrentAmmoClip->GetCurrentAmmo();
				const int bulletsRemoved = pInventory->ServerAddRemoveFreeBulletAmount(pCurrentAmmoClip->GetBulletName(), (-capacityLeft));
				//add bullets to ammo clip
				pCurrentAmmoClip->ServerAddRemoveAmmo(bulletsRemoved);
				ServerUpdateStaticData();
				return;
			}
			return;
		}
		//now if weapon has no ammo clip attached at the moment, find ammo clip in owner's inventory and attach it
		CAmmoClipComponent *nextAmmoClip = pOwner->GetInventory()->TakeNextAmmoClip(this);
		if (nextAmmoClip)
		{
			SetCurrentAmmoClip(nextAmmoClip->GetEntityId());
			ServerUpdateStaticData();
			return;
		}
		//there may occur situation, where ammo clipses that we have in inventory are empty; in this case TakeNextAmmoClip will fail, cause it doesn't make sense to attach empty ammo clip
		//so let's make our player fill it up(if possible - if has right bullets for it), and then attach it to weapon
		if (CAmmoClipComponent *pEmptyAmmoClip = pInventory->TakeNextEmptyAmmoClip(this))
		{
			//fill up ammo clip, like before remove from player and add to ammo clip
			const int bulletsRemoved = pInventory->ServerAddRemoveFreeBulletAmount(pEmptyAmmoClip->GetBulletName(), (-pEmptyAmmoClip->GetMaxAmmo()));
			const int bulletsAdded = pEmptyAmmoClip->ServerAddRemoveAmmo(bulletsRemoved);
			//if filling is succeeded, then new ammo clip becomes current one
			if (bulletsAdded > 0)
				SetCurrentAmmoClip(pEmptyAmmoClip->GetEntityId());

			ServerUpdateStaticData();
		}
	}
}

int CWeaponComponent::GetMaxAmmo()
{
	if (GetCurrentAmmoClip())
	{
		return GetCurrentAmmoClip()->GetMaxAmmo();
	}
	return 0;
}

int CWeaponComponent::GetCurrentAmmo()
{
	if (GetCurrentAmmoClip())
	{
		return GetCurrentAmmoClip()->GetCurrentAmmo();
	}
	return 0;
}

bool CWeaponComponent::IsWeaponReadyToShoot()
{
	return pFireModes->CanShot();
}

bool CWeaponComponent::IsSingle()
{
	return pFireModes->IsSingle();
}

void CWeaponComponent::ReleaseAmmoClipAttachment()
{
	if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			if (IAttachment *pAttachment = pMan->GetInterfaceByName("mag_att"))
			{
				pAttachment->ClearBinding();
			}
		}
	}
}

void CWeaponComponent::AttachAmmoClip()
{
	if (!GetCurrentAmmoClip())
		return;

	//before attaching new ammo clip, release attachment to get rid of whatever was there before
	ReleaseAmmoClipAttachment();

	IEntity *pAmmoClipEntity = gEnv->pEntitySystem->GetEntity(sWeaponProperties.currentAmmoClip);
	//we will use high-efficient cgf attachment for visual purposes
	CCGFAttachment *pCgfAtt = new CCGFAttachment();
	IStatObj *magCgf = pAmmoClipEntity->GetStatObj(0);
	pCgfAtt->pObj = magCgf;
	if (pCgfAtt)
	{
		if (ICharacterInstance *pChar = m_pEntity->GetCharacter(Character_slot))
		{
			if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
			{
				if (IAttachment *pAttachment = pMan->GetInterfaceByName("mag_att"))
				{
					pAttachment->AddBinding(pCgfAtt);
					//play ammo clip attach sound here
					pAudio->Play(SOUND_Reload);
				}
			}
		}
	}
}

CAmmoClipComponent * CWeaponComponent::GetCurrentAmmoClip()
{
	if (sWeaponProperties.currentAmmoClip <= 0)
		return nullptr;
	
	return gEnv->pEntitySystem->GetEntity(sWeaponProperties.currentAmmoClip)->GetComponent<CAmmoClipComponent>();
}

bool CWeaponComponent::HasAmmo()
{
	bool bHasAmmo;
	(GetCurrentAmmoClip() && !GetCurrentAmmoClip()->IsEmpty()) ? bHasAmmo = true : bHasAmmo = false;
	return bHasAmmo;
}
void CWeaponComponent::NextFireMode()
{
	pFireModes->NextMode();
}
Quat CWeaponComponent::GetBulletOutputOrientation()
{
	if (ICharacterInstance *pChar = GetEntity()->GetCharacter(Character_slot))
	{
		if (auto *pBarrelOutAttachment = pChar->GetIAttachmentManager()->GetInterfaceByName("barrel_out"))
		{
			QuatTS bulletOrigin = pBarrelOutAttachment->GetAttWorldAbsolute();
			return bulletOrigin.q;
		}
	}
	return Quat();
}

void CWeaponComponent::ServerResetClass()
{
	sWeaponProperties = sWeaponStartupProperties;
}
