/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Base pure virtual component. Weapons, ammo clipses and any other items derive from it

--------------------------------------------------------------------------------- */

#pragma once
#include <CryEntitySystem/IEntityComponent.h>
#include "ItemProperties.h"

class CPlayerComponent;
class CWeaponComponent;
class CAmmoClipComponent;
class CAmmoPackComponent;
struct SVehicleComponent;
struct NoParams;

struct SItemComponent : public IEntityComponent
{
	enum class EInputFlagType
	{
		Hold = 0,
		Toggle
	};
protected:
	const EEntityAspects kItemInput = eEA_GameClientD;
	const EEntityAspects kItemPositionAspect = eEA_GameServerDynamic;
	const EEntityAspects kItemStaticData = eEA_GameServerStatic;
public:
	//ComponentSystem
	SItemComponent() = default;
	virtual ~SItemComponent() {}
	virtual void Initialize();
	virtual void  ProcessEvent(SEntityEvent& event);
	virtual uint64 GetEventMask() const override;
	static void ReflectType(Schematyc::CTypeDesc<SItemComponent>& desc);
	//Input Component
	Cry::DefaultComponents::CInputComponent *GetInput() { return pInput; }
	virtual void RegisterActions();
	virtual void ClientActionRegister() {}
	virtual void HandleInputFlagChange(ItemInputFlags flags, int activationMode, EInputFlagType type = EInputFlagType::Hold);
	virtual void ServerActionRegister() {}
	void SERVER_REGISTER_ACTION(uint16 flag, std::function<void(int)> f);
	virtual void Client_Action_RenderPlacement(int activationMode);
	//~Input Component
	//NETWORKING
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual bool ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) { return true; }
	virtual NetworkAspectType GetClassAspectMask() const { return 0; }
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kItemPositionAspect | kItemStaticData | kItemInput | GetClassAspectMask(); };
	void SvUpdatePosition() { if(gEnv->bServer) NetMarkAspectsDirty(kItemPositionAspect); }
	virtual void ServerUpdateStaticData() { NetMarkAspectsDirty(kItemStaticData); }
	void DelegateAuthority(int channelId = 0);
	//RMI
	bool ServerDrop(NoParams&& p, INetChannel *);
	//~NETWORKING
	//Specific under-class component
	virtual void InitClass() = 0;
	virtual void ClassProcessEvent(SEntityEvent& event) = 0;
	virtual string GetItemName() { return sItemName; }
	virtual void CreateItemName();
	//Possession
	virtual void SetOwner(EntityId newOwner) { sItemProperties.ownerId = newOwner; sItemProperties.ownerIdComp = newOwner; }
	virtual CPlayerComponent *GetOwner();
	//Picking/droping
	virtual void ServerPickup(CPlayerComponent *pNewOwner);
	virtual bool IsPickable() { return sItemProperties.bIsPickable && !gEnv->IsEditor() && sItemProperties.ownerId <= 0; }
	virtual void IsPickable(bool pickable) { sItemProperties.bIsPickable = pickable; }
	virtual string GetPickupLabel(CPlayerComponent *pPicker) { return sItemProperties.sPickupLabel; }
	virtual void ClientDrop();
	virtual void ServerUse() {}
	virtual void ServerStopUse() {}
	virtual void ServerCarry(CPlayerComponent *pCarrier);
	virtual void ServerPlace();
	virtual void SetCarrier(CPlayerComponent *pCarrier) { sItemProperties.pPlayerCarrier = pCarrier; }
	virtual void ServerReset();
	virtual void ServerResetClass() {}
	//Usage
	virtual bool IsSelected() { return sItemProperties.bIsSelected; }
	virtual void IsSelected(bool is) { sItemProperties.bIsSelected = is; }
	//Rendering settings
	virtual void LoadCharacter();
	virtual void LoadGeometry();
	virtual void ReLoadMesh();
	virtual void ReLoadPhysics();
	//Existance
	virtual void Spawn();
	//Classes
	virtual SItemComponent *GetItem() { return m_pEntity->GetComponent<SItemComponent>(); }
	virtual CWeaponComponent *IsWeapon();
	virtual CAmmoClipComponent *IsAmmoClip();
	virtual CAmmoPackComponent *IsAmmoPack();
	virtual SVehicleComponent *IsVehicle();
	//Properties getting
	virtual SItemProperties *GetProperties() { return &sItemProperties; }
	virtual int GetInvUIType() { return type_invUI; }
	virtual void IsAttached(bool is) { sItemProperties.bIsAttached = is; }
	virtual bool IsAttached() { return sItemProperties.bIsAttached; }
	//VARIABLES
protected:
	string sItemName;
	bool bIsInitializedLocally = false;
	//input component
	Cry::DefaultComponents::CInputComponent *pInput = nullptr;
	//PROPERTIES
	SItemProperties sItemProperties;
	SItemProperties sPrevItemProperties;
	//special variables used to reset this component in the game world
	SItemProperties sItemStartupProperties;
	Vec3 vStartPos = ZERO;
	Quat qStartOrientation = IDENTITY;
	int type_invUI;

};