#include "CaptureArea.h"
#include "Components/PlayerComponent.h"
#include "CrySchematyc/Env/Elements/EnvComponent.h"
#include "CryNetwork/Rmi.h"
#include "GlobalResources.h"
#include "AmmoPackComponent.h"

static void RegisterCaptureArea(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CCaptureAreaComponent));
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterCaptureArea)

void CCaptureAreaComponent::Initialize()
{
	__super::Initialize();
	m_iCaptureTicker = 0;
	//all components should be bound to network on both sides, and preferably in initialize method
	GetEntity()->GetNetEntity()->BindToNetwork();

	auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
	pEntityIterator->MoveFirst();

	while (!pEntityIterator->IsEnd())
	{
		IEntity *pEntity = pEntityIterator->Next();
		if (CAmmoPackComponent *pAmmoPack = pEntity->GetComponent<CAmmoPackComponent>())
		{
			switch (GetAreaType())
			{

			case AreaShape::AREA_TYPE_BOX:
				if (GetBox()->IsContainPoint(pEntity->GetPos())) {
					pAmmoPack->SetCaptureArea(this);
				}
			case AreaShape::AREA_TYPE_CYLINDER:
				if (GetCylinder()->IsContainPoint(pEntity->GetPos())) {
					pAmmoPack->SetCaptureArea(this);
				}
			}
		}
	}
}

void CCaptureAreaComponent::ProcessEvent(SEntityEvent& event)
{
	//updating of capture area, from my understanding, should be done ONLY on server side!
	//it's because all entities (except for player) have server authority, and all the info comes from there
	//it'll also prevent hacking this area of the game

	//some things in superclass are client only (render), so we must allow it to trigger
	__super::ProcessEvent(event);

		switch (event.event)
		{
		case ENTITY_EVENT_UPDATE:
		{	
			//update cylinder position
			if (m_pAreaCylinder)
				m_pAreaCylinder->position = m_pEntity->GetWorldPos();
			if (gEnv->bServer && !gEnv->IsEditor())
			{
			//////////////////////////////////////////////////START_SERVER_UPDATE//////////////////////////////////////////////////

			m_iGermanCount = 0;
			m_iRussianCount = 0;


			//decide capturing team
			if (m_iCaptureTicker <= 0) {

				//make sure if it drops below 0 it gets reset
				m_iCaptureTicker = 0;

				//first player in area
				if (m_iPlayersInArea == 1 && m_pPlayersInArea[0])
					m_CapturingTeam = m_pPlayersInArea[0]->GetTeamIndex();
				//if the area has multiple players and it reached 0 then the point must have changed hands
				else if (m_iPlayersInArea > 1) {
					if (m_CapturingTeam == Team_Lobby_01) {
						m_CapturingTeam = Team_Lobby_02;
					}
					else if (m_CapturingTeam == Team_Lobby_02) {
						m_CapturingTeam = Team_Lobby_01;
					}
				}

			}

			//find out how many of each faction are in the area
			for (int i = 0; i < m_iPlayersInArea; i++) {
				if (m_pPlayersInArea[i])
				{
					if (m_pPlayersInArea[i]->GetTeamIndex() == Team_Lobby_02) {
						m_iGermanCount++;
					}
					else if (m_pPlayersInArea[i]->GetTeamIndex() == Team_Lobby_01) {
						m_iRussianCount++;
					}
				}
			}
			if (m_iGermanCount == 0 && m_iRussianCount == 0) {
				m_iCaptureTicker--;
			}

			//tally up capturing team or bring down if no one is fighting for it
			if (m_CapturingTeam == Team_Lobby_02) {
				m_iCaptureTicker += m_iGermanCount;
				m_iCaptureTicker -= m_iRussianCount;
			}
			else if (m_CapturingTeam == Team_Lobby_01) {
				m_iCaptureTicker += m_iRussianCount;
				m_iCaptureTicker -= m_iGermanCount;
			}

			//you may not capture a point until you have no enemies on it but you can 
			//over cap it to prolong the time it takes to be neutralized
			m_bContested = false;
			if (m_iGermanCount != 0 && m_iRussianCount != 0) {
				m_bContested = true;
			}

			if (!m_bContested) {
				if (m_iCaptureTicker >= 1000) {
					m_iCaptureTicker = 1000;
					m_bCaptured = true;
				}
#if DEBUG
				else if (m_iCaptureTicker > 0) {
						CryLog(ToString(m_iCaptureTicker));
				}

				if (m_bCaptured) {
						CryLog("captured");
				}

#endif
				if (m_bCaptured && m_iCaptureTicker <= 0) {
					m_iCaptureTicker = 0;
					m_bCaptured = false;
				}
			}
			//here we will send RMI to all clients about the result of the capture area properties : m_CapturingTeam, m_bCaptured and m_bContested
			//we must make sure, that RMI is called only when any of the values changed. Otherwise it will get called every frame, and it will be a huge problem and will cause massive lags from server side
			//in cryengine network functions can be done only on players, that's why we will have to grab any of the capturing team player to send info
			//send RMI only when any of the value changes from the last saved
			if (m_CapturingTeam != m_CapturingTeam_last || m_bCaptured != m_bCaptured_last || m_bContested != m_bContested_last)
			{
				//assing the new values
				m_CapturingTeam_last = m_CapturingTeam;
				m_bCaptured_last = m_bCaptured;
				m_bContested_last = m_bContested;
			}
		}
		//////////////////////////////////////////END_SERVER_UPDATE/////////////////////////////////////////////////////////
			else if (!gEnv->bServer || gEnv->IsEditor()){
		//////////////////////////////////////////////START_CLIENT_UPDATE//////////////////////////////////////////////////////////
				//determine the color of the capture area
				m_cColor = ColorB(Col_Green);
				if (m_bCaptured) {

					if (m_CapturingTeam == Team_Lobby_01) {
						m_cColor = ColorB(Col_Red);
					}
					else {
						m_cColor = ColorB(Col_Azure);
					}
				}
				else {


					if (m_bContested) {
						m_cColor = ColorB(Col_Yellow);
					}
				}
				Render(m_cColor);
			}
		}
	}
	//needs to be moved to child class of PlayerAreaTrigger so that the color can be modified.
#if DEBUG
	//render is client only, because server does not (ant CAN'T) render anything; if we try - server will immidiately crash
	//Render(m_cColor);
#endif
	///////////////////////////////////////////////END_CLIENT_UPDATE/////////////////////////////////////////////////////////////////////
}

void CCaptureAreaComponent::ReflectType(Schematyc::CTypeDesc<CCaptureAreaComponent>& desc)
{
	desc.SetGUID("{4135431D-51A1-49E5-A7E3-90490CACF2B7}"_cry_guid);
	desc.AddBase<CPlayerAreaTriggerComponent>();
	desc.SetEditorCategory("Capture areas");
	desc.SetLabel("Capture area component");
	desc.SetDescription("");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CCaptureAreaComponent::m_CaptureAreaProperties, 'art', "AreaShape", "Area shape", "Chose type of area", SCaptureAreaProperties());
}