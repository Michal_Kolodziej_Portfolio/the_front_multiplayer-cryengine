/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : It handles attachment system for player.

--------------------------------------------------------------------------------- */

#pragma once
#include <CryEntitySystem/IEntityComponent.h>

struct SItemComponent;
class CAmmoPackComponent;
struct IAttachment;

class CAttachmentComponent final : public IEntityComponent
{
public:
	CAttachmentComponent() = default;
	virtual ~CAttachmentComponent() {}
	//ComponentSystem
	virtual void Initialize() override {}
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent &event) override {}
	static void ReflectType(Schematyc::CTypeDesc<CAttachmentComponent>& desc);
	void ResetAttachments();
	//RMI
	//AttachmentSystem
	void AttachToBack(EntityId itemToAttach, int slotId);
	void AttachToBelt(EntityId itemToAttach, int slotId);
	void AttachToHand(EntityId itemToAttach);
	void SwapBackAttachments(int slot_first, int slot_second);
	Vec3 GetWeaponAttachmentLocalPos();
	Vec3 GetWeaponAttachmentWorldPos();
	Vec3 GetAttachmentPositionForItem(SItemComponent *pItem);
	string GetAttachmentNameForItem(SItemComponent *pItem);
	IAttachment *GetAttachmentForItem(SItemComponent *pItem);

	bool bIsLocal = false;
};