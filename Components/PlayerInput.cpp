#include "StdAfx.h"
#include "PlayerComponent.h"
#include "PlayerUIComponent.h"
#include "FlashUI/FlashUI.h"
#include "Cry3DEngine/ITimeOfDay.h"
#include "UISystem/HudComponent.h"
#include "VehicleComponent.h"
#include "GameTeam.h"
#include "ServerComponent.h"
#include "AccountComponent.h"

void CPlayerComponent::InitInputActions()
{
	pInput->RegisterAction("player", "moveleft", [this](int activationMode, float value) { if (!bFreezePlayer) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveLeft, activationMode); }});
	pInput->BindAction("player", "moveleft", eAID_KeyboardMouse, EKeyId::eKI_A);

	pInput->RegisterAction("player", "moveright", [this](int activationMode, float value) { if (!bFreezePlayer) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveRight, activationMode); }});
	pInput->BindAction("player", "moveright", eAID_KeyboardMouse, EKeyId::eKI_D);

	pInput->RegisterAction("player", "moveforward", [this](int activationMode, float value) { if (!bFreezePlayer) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveForward, activationMode); }});
	pInput->BindAction("player", "moveforward", eAID_KeyboardMouse, EKeyId::eKI_W);

	pInput->RegisterAction("player", "moveback", [this](int activationMode, float value) { if (!bFreezePlayer) { HandleInputFlagChange((TInputFlags)EInputFlag::MoveBack, activationMode); }});
	pInput->BindAction("player", "moveback", eAID_KeyboardMouse, EKeyId::eKI_S);

	pInput->RegisterAction("player", "mouse_rotateyaw", [this](int activationMode, float value) { if (!bFreezePlayer) { m_mouseDeltaRotation.x -= value; if (!gEnv->bServer) NetMarkAspectsDirty(kMovementAspect); }});
	pInput->BindAction("player", "mouse_rotateyaw", eAID_KeyboardMouse, EKeyId::eKI_MouseX);

	pInput->RegisterAction("player", "mouse_rotatepitch", [this](int activationMode, float value) { if (!bFreezePlayer) { m_mouseDeltaRotation.y -= value; if (!gEnv->bServer) NetMarkAspectsDirty(kMovementAspect); }});
	pInput->BindAction("player", "mouse_rotatepitch", eAID_KeyboardMouse, EKeyId::eKI_MouseY);

	pInput->RegisterAction("player", "use", [this](int activationMode, float value) { Action_Use(activationMode); });
	pInput->BindAction("player", "use", eAID_KeyboardMouse, EKeyId::eKI_F);

	pInput->RegisterAction("player", "carry", [this](int activationMode, float value) { Action_Carry(activationMode); });
	pInput->BindAction("player", "carry", eAID_KeyboardMouse, EKeyId::eKI_P);

	pInput->RegisterAction("player", "select_slot0", [this](int activationMode, float value) { Action_SelectSlot(activationMode, value, 0); });
	pInput->RegisterAction("player", "select_slot1", [this](int activationMode, float value) { Action_SelectSlot(activationMode, value, 1); });
	pInput->RegisterAction("player", "select_slot2", [this](int activationMode, float value) { Action_SelectSlot(activationMode, value, 2); });
	pInput->BindAction("player", "select_slot0", eAID_KeyboardMouse, EKeyId::eKI_1);
	pInput->BindAction("player", "select_slot1", eAID_KeyboardMouse, EKeyId::eKI_2);
	pInput->BindAction("player", "select_slot2", eAID_KeyboardMouse, EKeyId::eKI_3);

	pInput->RegisterAction("player", "shoot", [this](int activationMode, float value) { Action_Shoot(activationMode); });
	pInput->BindAction("player", "shoot", eAID_KeyboardMouse, EKeyId::eKI_Mouse1);

	pInput->RegisterAction("player", "sprint", [this](int activationMode, float value) { if (!bFreezePlayer) { HandleInputFlagChange((TInputFlags)EInputFlag::Sprint, activationMode); }});
	pInput->BindAction("player", "sprint", eAID_KeyboardMouse, EKeyId::eKI_LShift);

	pInput->RegisterAction("player", "aim", [this](int activationMode, float value) { Action_Aim(activationMode); });
	pInput->BindAction("player", "aim", eAID_KeyboardMouse, EKeyId::eKI_Mouse2);

	pInput->RegisterAction("player", "drop", [this](int activationMode, float value) { Action_Drop(activationMode); });
	pInput->BindAction("player", "drop", eAID_KeyboardMouse, EKeyId::eKI_J);

	pInput->RegisterAction("player", "results", [this](int activationMode, float value) { Action_ResultsWindow(activationMode); });
	pInput->BindAction("player", "results", eAID_KeyboardMouse, EKeyId::eKI_Tab);

	pInput->RegisterAction("player", "dialog_exit", [this](int activationMode, float value) { Action_ExitDialog(activationMode); });
	pInput->BindAction("player", "dialog_exit", eAID_KeyboardMouse, EKeyId::eKI_Escape);

	pInput->RegisterAction("player", "jump", [this](int activationMode, float value) { if (!bFreezePlayer) { HandleInputFlagChange((TInputFlags)EInputFlag::Jump, activationMode); }});
	pInput->BindAction("player", "jump", eAID_KeyboardMouse, EKeyId::eKI_Space);

	pInput->RegisterAction("player", "inv_toggle", [this](int activationMode, float value) { Action_Inventory(activationMode); });
	pInput->BindAction("player", "inv_toggle", eAID_KeyboardMouse, EKeyId::eKI_I);
}
//Server

void CPlayerComponent::Action_Use(int activationMode)
{
	if (!bFreezePlayer)
	{
		if (activationMode == eIS_Pressed)
		{
			//player sends single request to server, that he wants to pick something up; target item is sent through aspect when it changes
			SRmi < RMI_WRAP(&CPlayerComponent::ServerUse)>::InvokeOnServer(this, NoParams{});
		}
	}
}

void CPlayerComponent::Action_Carry(int activationMode)
{
	if (!bFreezePlayer)
	{
		if (activationMode == eIS_Pressed)
		{
			//carrying ammo pack or other things
			SRmi < RMI_WRAP(&CPlayerComponent::ServerCarry)>::InvokeOnServer(this, NoParams{});
		}
	}
}

void CPlayerComponent::Action_SelectSlot(int activationMode, float value, int slotId)
{
	if (!bFreezePlayer)
	{
		if (activationMode == eIS_Pressed)
		{
			//inventory handles everything that uses inventory
			GetInventory()->ClientSelectSlot(slotId);
		}
	}
}

void CPlayerComponent::Action_Shoot(int activationMode)
{
	if (activationMode == eIS_Pressed)
	{
		//Death cam switch works only when player is ragdolized; no need to involve network for this
		if (!IsPlayerAlive())
		{
			for (int i = sPlayerProperties.iDeathCamIndex; i < 32; i++)
			{
				if (i != GetIndexInTeam())
				{
					if (GetTeam()->GetAccountAtIndex(i))
					{
						sPlayerProperties.iDeathCamIndex = i;
						return;
					}
				} 
			}
			return;
		}
	}
}

void CPlayerComponent::Action_Aim(int activationMode)
{
	if (!bFreezePlayer)
	{
		//aiming is quite obvious; it's a camera component related calculation of offset etc.; camera exists only on local clients and aim will always be calculated here; 
		//if in the future any special animations for aiming are given, then player obviously we will create some flag and send this info to server, but just for the animation sake
		if (activationMode == eIS_Down)
		{
			if (pInventory->GetSelectedItem())
			{
				if (GetInventory()->HasWeaponSelected() > 0)
				{
					if (CWeaponComponent *pWeapon = GetInventory()->HasWeaponSelected())
						pWeapon->StartAim();
				}
			}
		}
		if (activationMode == eIS_Released)
		{
			if (pInventory->GetSelectedItem())
			{
				if (GetInventory()->HasWeaponSelected() > 0)
				{
					if (CWeaponComponent *pWeapon = GetInventory()->HasWeaponSelected())
						pWeapon->StopAim();
				}
			}
		}
	}
}

void CPlayerComponent::Action_Drop(int activationMode)
{
	if (!bFreezePlayer)
	{
		if (activationMode == eIS_Pressed)
		{
			//player will call ClientDrop on the item he wants to drop, item will handle networking
			if (SItemComponent *item = GetInventory()->GetSelectedItem())
				item->ClientDrop();
		}
	}
}

void CPlayerComponent::Action_ResultsWindow(int activationMode)
{
	if (!bFreezePlayer)
	{
		//results are client only, as each player is updated for all clients immidiately after any changes in kills/deaths are done, so we use the local values, don't need delays from server in receiving this informations
		if (activationMode == eIS_Pressed)
		{
			//here we show the match summary menu
			GetUISystem()->ShowSummary();

			//we will iterate through both teams and take values
			for (int i = Team_Lobby_01; i < 3; i++)
			{
				for (int x = 0; x < MAX_TEAM_PLAYERS; x++)
				{
					if (CAccountComponent *pMemberAccount = GetServer()->GetTeam(i)->GetAccountAtIndex(x))
					{
						if (CPlayerComponent *player = pMemberAccount->GetAssociatedPlayer())
						{
							const string name = pMemberAccount->GetLogin();
							const int tmIndex = pMemberAccount->GetTeamIndex() - 1;
							const int indexInTm = pMemberAccount->GetIndexInTeam();
							const int kills = player->GetKills();
							const int deaths = player->GetDeaths();

							SUIArguments args;
							args.AddArgument<string>(name);
							args.AddArgument<int>(tmIndex);
							args.AddArgument<int>(indexInTm);
							args.AddArgument<string>(ToString(kills));
							args.AddArgument<string>(ToString(deaths));
							GetUISystem()->SummarySetPlayer(args);
						}
					}
				}
			}
		}
		else if (activationMode == eIS_Released)
		{
			GetUISystem()->ShowSummary(false);
			GetUISystem()->SummaryClear();
		}
	}
}

void CPlayerComponent::Action_ExitDialog(int activationMode)
{
	//obviously this is just internal method for local clients showing the window asking to close or get back to game
	if (activationMode == eIS_Pressed)
	{
		if (GetUISystem()->bIsAmmoPackDialogOpened)
		{
			SRmi<RMI_WRAP(&CPlayerComponent::ServerStopVisiting)>::InvokeOnServer(this, NoParams{});
		}
		else
		{
			if (!bExitDialogOpened)
			{
				//on escape pressed exit game dialog will show up
				bExitDialogOpened = true;
				bFreezePlayer = true;
				GetUISystem()->ShowExitDialog();
			}
			else
			{
				//if on exit dialog player chooses no, or if presses escape once again this function will be called and it will hide exit dialog
				bExitDialogOpened = false;
				bFreezePlayer = false;
				GetUISystem()->ShowExitDialog(false);
			}
		}
	}
}

void CPlayerComponent::Action_Inventory(int activationMode)
{
	if (activationMode == eIS_Pressed)
	{
		//this will toggle inventory open/close
		GetUISystem()->ShowInventory();
	}
}
