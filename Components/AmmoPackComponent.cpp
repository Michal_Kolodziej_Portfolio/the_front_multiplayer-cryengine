#include "StdAfx.h"
#include "AmmoPackComponent.h"
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include "CrySchematyc/Env/Elements/EnvComponent.h"
#include "CaptureArea.h"
#include "Components/ServerComponent.h"
#include "AccountComponent.h"
#include "FlashUI/FlashUI.h"
#include "GlobalResources.h"
#include "PlayerUIComponent.h"

static void RegisterAmmoPack(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAmmoPackComponent));
		{

		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterAmmoPack);

CAmmoPackComponent::CAmmoPackComponent()
{					
}

void CAmmoPackComponent::ReflectType(Schematyc::CTypeDesc<CAmmoPackComponent>& desc)
{
	desc.SetGUID("{C578B0C4-A833-4D2C-A4A3-CE2646408BED}"_cry_guid);
	desc.AddBase<SItemComponent>();
	desc.SetEditorCategory("Items");
	desc.SetLabel("Ammo pack");
	desc.SetDescription("Weapon ammo pack");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CAmmoPackComponent::sItemProperties, 'ipr', "ItemProperties", "Item properties", "ItemProperties", SItemProperties());
	desc.AddMember(&CAmmoPackComponent::sAmmoPackProperties, 'apr', "AmmoPackProperties", "Ammo pack properties", "Ammo pack propereties", SAmmoPackProperties());
}

void CAmmoPackComponent::InitClass()
{
	sItemProperties.sPickupLabel = "Press F to open or P to carry";
	//resetting
	sAmmoPackStartupProperties = sAmmoPackProperties;

	auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
	pEntityIterator->MoveFirst();

	while (!pEntityIterator->IsEnd())
	{
		IEntity *pEntity = pEntityIterator->Next();
		if (CCaptureAreaComponent *pCapArea = pEntity->GetComponent<CCaptureAreaComponent>())
		{
			switch (pCapArea->GetAreaType())
			{

			case AreaShape::AREA_TYPE_BOX:
				if (pCapArea->GetBox()->IsContainPoint(m_pEntity->GetPos())) {
					SetCaptureArea(pCapArea);
				}
			case AreaShape::AREA_TYPE_CYLINDER:
				if (pCapArea->GetCylinder()->IsContainPoint(m_pEntity->GetPos())) {
					SetCaptureArea(pCapArea);
				}
			}
		}
	}
}

void CAmmoPackComponent::ClassProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED:
	{
		if (sAmmoPackProperties != sAmmoPackPrevProperties)
		{
			sAmmoPackPrevProperties = sAmmoPackProperties;
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		//now if capturea area of this ammo pack get its state changed, we will handle it here
		if (pCaptureArea)
		{
			int iCapTeam = pCaptureArea->GetCapturingTeam();
			if (pCaptureArea->GetIsCaptured())
			{
				sAmmoPackProperties.iTeam = iCapTeam;
			}
			else
				sAmmoPackProperties.iTeam = -1;
		}
	}
	break;
	}
}

bool CAmmoPackComponent::ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kItemStaticData)
	{
		ser.BeginGroup("StaticData");

		ser.Value("currentAmmo", sAmmoPackProperties.iCurrentAmmo);

		if (!gEnv->bServer)
		{
			if (CPlayerComponent *localPlayer = GetServer()->GetLocalPlayer())
			{
				if (localPlayer->GetVisitedAmmoPack() == this)
				{
					//update visited ammo pack values
					SUIArguments args;
					args.AddArgument<int>(GetMaxAmmo());
					args.AddArgument<int>(GetCurrentAmmo());
					args.AddArgument<string>(GetBulletShortName());
					localPlayer->GetUISystem()->SetAmmoPackDialog(args);
				}
			}
		}

		ser.EndGroup();
	}
	return true;
}

void CAmmoPackComponent::ServerPickup(CPlayerComponent * pNewOwner)
{
	//ammo pack is specific type of item and while it is considered pickable, it won't be attached to player at all, therefore physics is not disabled and extra logic is needed here
	if (!pNewOwner || !IsPickable())
		return;

	//first of all we will have to check if this ammo pack is the same team as requestor player, or no team
	if (GetTeam() == pNewOwner->GetTeamIndex() || GetTeam() == Team_NO_TEAM)
	{
		//we should update visited ammo pack so client can get info about it
		pNewOwner->ServerSetVisitedAmmoPack(GetEntityId());
	}
}

int CAmmoPackComponent::ServerAddRemoveAmmo(int ammo)
{
	if (ammo == 0)
		return 0;

	int ammoReturn = 0;

	//ADDING
	if (ammo > 0)
	{
		//firstly we have to add current ammo to the ammo we want to add
		const int newAmmo = sAmmoPackProperties.iCurrentAmmo + ammo;
		//then remove it from max ammo; this will handle possibility that if we try to add more than its capacity, we will reduce it and grab max that we could possibly add
		int ammoAboveMax = sAmmoPackProperties.iMaxAmmo - newAmmo;
		//if ammo above the maximum is not negative value, then make it 0, so it doesn't increase amount of bullets added
		if (ammoAboveMax > 0)
			ammoAboveMax = 0;
		//now we add ammo that we want to add, to ammoAboveMax value to get value of possible amount to add; ammoAboveMax is negative if we try to add too much
		const int possibleAmmoAdd = ammo + ammoAboveMax;
		//now we got the ammo amount that ammo pack can handle at max
		//we will apply this ammo amount
		sAmmoPackProperties.iCurrentAmmo += possibleAmmoAdd;
		ammoReturn = possibleAmmoAdd;
	}
	//REMOVING
	else
	{
		//firstly we will add current ammo to the (negative) ammo amount that we want to remove
		const int newAmmo = sAmmoPackProperties.iCurrentAmmo + ammo;
		//then we have to check, if value is positive or zero, then newAmmo represents new ammo amount
		if (newAmmo >= 0)
		{
			sAmmoPackProperties.iCurrentAmmo = newAmmo;
			ammoReturn = (-ammo);
		}
		//but if it's negative, it means we are trying to remove more ammo than this ammo pack currently has, so in this case we will make current ammo 0, and return current ammo state(before removing)
		else
		{
			const int removedAmmo = sAmmoPackProperties.iCurrentAmmo;
			sAmmoPackProperties.iCurrentAmmo = 0;
			ammoReturn = removedAmmo;
		}
	}
	//serialize values for ammo pack
	ServerUpdateStaticData();
	return ammoReturn;
}

string CAmmoPackComponent::GetPickupLabel(CPlayerComponent * pPicker)
{
	if (!pPicker)
		return sItemProperties.sPickupLabel;

	if (GetTeam() == -1 || pPicker->GetTeamIndex() == GetTeam())
		return sItemProperties.sPickupLabel;

	return "This ammo pack is captured by other team!";
}

void CAmmoPackComponent::ServerResetClass()
{
	sAmmoPackProperties = sAmmoPackStartupProperties;
}

