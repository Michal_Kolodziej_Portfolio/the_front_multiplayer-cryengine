#include "StdAfx.h"
#include "RagdollComponent.h"
#include "CryEntitySystem/IEntity.h"

static void RegisterRagdoll(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CRagdollComponent));
		// Functions
		{
		}
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterRagdoll)

void CRagdollComponent::Initialize()
{
	m_pEntity->LoadCharacter(Character_slot, "Objects/Characters/SampleCharacter/thirdperson.cdf");
	SEntityPhysicalizeParams physParams;
	physParams.type = PE_ARTICULATED;
	physParams.mass = 80.f;
	physParams.nSlot = 0;
	physParams.bCopyJointVelocities = true;
	m_pEntity->Physicalize(physParams);
	m_pEntity->GetNetEntity()->BindToNetwork();
}

void CRagdollComponent::ReflectType(Schematyc::CTypeDesc<CRagdollComponent>& desc)
{
	desc.SetGUID("{A72217A7-8413-4DD6-AB5B-3596DEF3970F}"_cry_guid);
}
