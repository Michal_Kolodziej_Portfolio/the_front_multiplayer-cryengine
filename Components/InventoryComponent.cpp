#include "StdAfx.h"
#include "InventoryComponent.h"
#include "WeaponComponent.h"
#include "AmmoClipComponent.h"
#include "GlobalResources.h"

static void RegisterInventoryComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CInventoryComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterInventoryComponent)

void CInventoryComponent::Initialize()
{
	SRmi<RMI_WRAP(&CInventoryComponent::ServerSelectSlot)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	SRmi<RMI_WRAP(&CInventoryComponent::ServerSwapSlots)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
}

uint64 CInventoryComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE);
}

void CInventoryComponent::ReflectType(Schematyc::CTypeDesc<CInventoryComponent>& desc)
{
	desc.SetGUID("{CA270097-0B0E-4B10-8322-6F0A037E3784}"_cry_guid);
}

bool CInventoryComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	//inventory is sent entirely from the server whenever something changes
	if (aspect == kInventoryStaticData)
	{
		ser.BeginGroup("StaticData");

		//iterate through all objects in inventory
		SItemComponent *itemWeapon = nullptr;
		SItemComponent *itemWeaponPrev = nullptr;
		SItemComponent *itemAmmoClip = nullptr;
		SItemComponent *itemAmmoClipPrev = nullptr;
		for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
		{
			ser.Value("bulletFirst", bullet[i].first);
			ser.Value("bulletSecond", bullet[i].second);

			//making sure that before we send variables, we also fill up compressed variables(when I figure out what's 'eid' for we can remove one of them)
			//WRITING COMPRESSED IDS
			if (ser.IsWriting() && gEnv->bServer)
			{
				ammoClipComp[i] = ammoClip[i];
				if (i < WEAPON_CAPACITY)
					weaponIdComp[i] = weaponId[i];
			}
			//~WRITING COMPRESSED IDS

			//before it's serialized, we will save what item was in this slot to eventually remove it later
			if (ser.IsReading() && !gEnv->bServer)
			{
				//make sure, that we pick up valid id for this entity(also remove with compressed/un values)
				IEntity *pEntity = gEnv->pEntitySystem->GetEntity(ammoClip[i]);
				if (pEntity)
					itemAmmoClipPrev = pEntity->GetComponent<SItemComponent>();
			}
			//serializing ammo clip inventory
			ser.Value("ammoclip", ammoClip[i]);
			ser.Value("ammoclipComp", ammoClipComp[i], 'eid');
			if (ser.IsReading() && !gEnv->bServer)
			{
				//make sure, that we pick up valid id for this entity(also remove with compressed/un values)
				IEntity *pEntity = Server_GetEntityById(ammoClip[i], ammoClipComp[i]);
				if (pEntity)
				{
					ammoClip[i] = pEntity->GetId();
					itemAmmoClip = pEntity->GetComponent<SItemComponent>();
				}

			}
			if (i < WEAPON_CAPACITY)
			{
				//before it's serialized, we will save what item was in this slot to eventually remove it later
				if (ser.IsReading() && !gEnv->bServer)
				{
					//make sure, that we pick up valid id for this entity(also remove with compressed/un values)
					IEntity *pEntity = gEnv->pEntitySystem->GetEntity(weaponId[i]);
					if (pEntity)
						itemWeaponPrev = pEntity->GetComponent<SItemComponent>();
				}
				//serializing weapon inventory
				ser.Value("weapons", weaponId[i]);
				ser.Value("weaponsComp", weaponIdComp[i], 'eid');
				if (ser.IsReading() && !gEnv->bServer)
				{
					//make sure, that we pick up valid id for weapon entity
					IEntity *pEntity = Server_GetEntityById(weaponId[i], weaponIdComp[i]);
					if (pEntity)
					{
						weaponId[i] = pEntity->GetId();
						itemWeapon = pEntity->GetComponent<SItemComponent>();
					}
				}
			}
			if (bIsLocal)
			{
				CPlayerComponent *player = m_pEntity->GetComponent<CPlayerComponent>();
				if (itemWeapon)
				{
					//if this player is local, then we should also add item to his inventory(if item is valid)
					int bullets = 0;
					player->GetUISystem()->AddItem(itemWeapon->GetItemName(), i, itemWeapon->GetInvUIType(), bullets);
					itemWeapon = nullptr;
					itemWeaponPrev = nullptr;
				}
				else if(itemWeaponPrev)
				{
					//if item is not valid though, we should clear UI slot
					player->GetUISystem()->RemoveItem(i, itemWeaponPrev->GetInvUIType());
					itemWeaponPrev = nullptr;
					itemWeapon = nullptr;
				}
				//AMMO CLIP
				if (itemAmmoClip)
				{
					//if this player is local, then we should also add item to his inventory(if item is valid)
					int bullets = 0;
					if (itemAmmoClip->IsAmmoClip())
						bullets = itemAmmoClip->IsAmmoClip()->GetCurrentAmmo();

					player->GetUISystem()->AddItem(itemAmmoClip->GetItemName(), i, itemAmmoClip->GetInvUIType(), bullets);
					itemAmmoClip = nullptr;
					itemAmmoClipPrev = nullptr;
				}
				else if (itemAmmoClipPrev)
				{
					//if item is not valid though, we should clear UI slot
					player->GetUISystem()->RemoveItem(i, itemAmmoClipPrev->GetInvUIType());
					itemAmmoClipPrev = nullptr;
					itemWeapon = nullptr;
				}
			}
		}
		SItemComponent *foreignItemPrev = nullptr;
		if (ser.IsReading() && !gEnv->bServer)
		{
			if (IEntity *pForeignEntity = Server_GetEntityById(foreignItem, foreignItemComp))
				foreignItemPrev = pForeignEntity->GetComponent<SItemComponent>();
		}

		//serialize selected item slot
		ser.Value("activeSlot", activeSlot);
		ser.Value("foreignItem", foreignItem);
		ser.Value("foreignItemComp", foreignItemComp, 'eid');

		if (ser.IsReading() && !gEnv->bServer)
		{
			if (IEntity *pForeignEntity = Server_GetEntityById(foreignItem, foreignItemComp))
			{
				SItemComponent *item = pForeignEntity->GetComponent<SItemComponent>();
				item->SetCarrier(m_pEntity->GetComponent<CPlayerComponent>());
				if (foreignItemPrev && foreignItemPrev != item)
				{
					foreignItemPrev->SetCarrier(nullptr);
				}
			}
			else if (foreignItemPrev)
				foreignItemPrev->SetCarrier(nullptr);

			if (CAttachmentComponent *pAttachments = m_pEntity->GetComponent<CAttachmentComponent>())
			{
				//reset attachments on client will attach stuff based on client's inventory state
				pAttachments->ResetAttachments();
			}
		}


		ser.EndGroup();
	}
	return true;
}

bool CInventoryComponent::ServerSelectSlot(SlotPass && p, INetChannel *)
{
	if (p.slot < 0 || p.slot > WEAPON_CAPACITY || weaponId[p.slot] <= 0)
		return true;

	SelectSlot(p.slot);
	return true;
}

bool CInventoryComponent::ServerSwapSlots(SwapSlotsParams && p, INetChannel *)
{
	if (p.slot_first < 0 || p.slot_second < 0)
		return true;

	EntityId firstItem = weaponId[p.slot_first];
	EntityId secondItem = weaponId[p.slot_second];

	weaponId[p.slot_first] = secondItem;
	weaponId[p.slot_second] = firstItem;

	return true;
}

bool CInventoryComponent::ServerAddItem(SItemComponent * pNewItem)
{
	//this method is for managing types of items
	if (!pNewItem)
		return false;

	bool bOk = false;
	if (CWeaponComponent *pNewWeapon = pNewItem->IsWeapon())
		bOk = ServerAddWeapon(pNewWeapon);

	else if (CAmmoClipComponent *pNewAmmoClip = pNewItem->IsAmmoClip())
		bOk = ServerAddAmmoClip(pNewAmmoClip);

	else if (CAmmoPackComponent *pNewAmmoPack = pNewItem->IsAmmoPack())
		bOk = (ServerAddRemoveFreeBulletAmount(pNewAmmoPack, 1) > 0) ? true : false;

	ServerUpdateStaticData();

	return bOk;
}

bool CInventoryComponent::ServerAddWeapon(CWeaponComponent * pNewWeapon)
{
	if (!pNewWeapon)
		return false;

	for (int i = 0; i < WEAPON_CAPACITY; i++)
	{
		if (weaponId[i] <= 0)
		{
			weaponId[i] = pNewWeapon->GetEntityId();
			return true;
		}
	}

	return false;
}

bool CInventoryComponent::ServerAddAmmoClip(CAmmoClipComponent * pNewAmmoClip)
{
	if (!pNewAmmoClip)
		return false;

	for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
	{
		if (ammoClip[i] <= 0)
		{
			ammoClip[i] = pNewAmmoClip->GetEntityId();
			return true;
		}
	}
	return false;
}

void CInventoryComponent::ServerRemoveItem(SItemComponent * pItemToRemove)
{	
	//this method is for managing types of items
	if (!pItemToRemove)
		return;

	if (CWeaponComponent *pNewWeapon = pItemToRemove->IsWeapon())
		ServerRemoveWeapon(pNewWeapon);

	else if (CAmmoClipComponent *pNewAmmoClip = pItemToRemove->IsAmmoClip())
		ServerRemoveAmmoClip(pNewAmmoClip);

	else if (CAmmoPackComponent *pNewAmmoPack = pItemToRemove->IsAmmoPack())
		ServerAddRemoveFreeBulletAmount(pNewAmmoPack, -1);

	ServerUpdateStaticData();
}

void CInventoryComponent::ServerRemoveWeapon(CWeaponComponent * pItemToRemove)
{
	if (!pItemToRemove)
		return;

	for (int i = 0; i < WEAPON_CAPACITY; i++)
	{
		if (weaponId[i] == pItemToRemove->GetEntityId())
		{
			//handle possibility of removing weapon that is also the selected weapon
			if (i == activeSlot)
				activeSlot = -1;

			weaponId[i] = 0;
			return;
		}
	}
}

void CInventoryComponent::ServerRemoveAmmoClip(CAmmoClipComponent * pItemToRemove)
{
	if (!pItemToRemove)
		return;

	for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
	{
		if (ammoClip[i] == pItemToRemove->GetEntityId())
		{
			ammoClip[i] = 0;
			return;
		}
	}
}

int CInventoryComponent::ServerAddRemoveFreeBulletAmount(CAmmoPackComponent * pAmmoPack, int ammount)
{
	if (!pAmmoPack || ammount == 0)
		return 0;

	string bulletName = pAmmoPack->GetBulletName();
	return ServerAddRemoveFreeBulletAmount(bulletName, ammount);
}

int CInventoryComponent::ServerAddRemoveFreeBulletAmount(string bulletName, int ammount)
{
	if (bulletName.empty())
		return 0;

	int bulletsReturn = 0;
	//Get or create slot for bullet
	const int bulletGroupIndex = ServerGetOrCreateBulletGroup(bulletName);

	//if group index is less than 0, it can't be created
	if (bulletGroupIndex < 0)
		return 0;

	//REMOVING
	if (ammount < 0)
	{
		//getting current ammount of bullets in this group
		const int currentAmmount = bullet[bulletGroupIndex].second;
		//add number of bullets we want to remove
		const int ammountAfter = currentAmmount + ammount;
		//now if difference is negative or 0, it means we tried to remove more bullets of this type, than we currently have, so we make it 0 and return amount of bullets we had before removing
		if (ammountAfter <= 0)
		{
			//zero out entire pair and return saved current ammount
			bullet[bulletGroupIndex].second = 0;
			bullet[bulletGroupIndex].first.Empty();
			bulletsReturn = currentAmmount;
		}
		//if difference is larger than 0, it means we removed less bullets than we currently have, so we simply reduce it by (negative) ammount (ammountAfter stores that value)
		else
		{
			bullet[bulletGroupIndex].second = ammountAfter;
			//return value; because ammount in removing process is negative, we will flip it back to positive
			bulletsReturn = (-ammount);
		}
	}
	//ADDING
	else
	{
		//getting current ammount of bullets in all groups
		const int currentAmmount = GetBulletsCount();
		//add number of bullets we want to add
		const int ammountAfter = currentAmmount + ammount;
		//now we will check the difference between ammountAfter adding and max ammount
		const int difference = BULLET_CAPACITY - ammountAfter;
		//if difference is negative, it means we tried to add more bullets than we can and we have to reduce ammount of bullets added by this negative value
		if (difference < 0)
		{
			bulletsReturn = ammount + difference;
			bullet[bulletGroupIndex].second += bulletsReturn;
		}
		//if difference is positive, it means we still have some space left, so we will simply return ammount and apply ammount 
		else
		{
			bulletsReturn = ammount;
			bullet[bulletGroupIndex].second += ammount;
		}
	}
	//because this function is public, and it is not involved in RemoveItem method, that serializes stuff, we have to make serialize here
	ServerUpdateStaticData();

	return bulletsReturn;
}

int CInventoryComponent::ServerGetOrCreateBulletGroup(string bulletName)
{
	if (!bulletName.empty())
	{
		//Find bullet index for this bullet name
		int bulletTypeIndex = -1;
		for (int i = 0; i < BULLET_TYPE_CAPACITY; i++)
		{
			//if bullet already has its group in inventory, find it
			if (bullet[i].first == bulletName)
			{
				bulletTypeIndex = i;
				return i;
			}
			//if it doesn't have its inventory group, then find first empty position where we could place it
			else if (bulletTypeIndex < 0)
			{
				if (bullet[i].first.empty())
					bulletTypeIndex = i;
			}
		}

		//create bullet group if bullet was assigned to empy slot
		if (bulletTypeIndex > -1)
		{
			bullet[bulletTypeIndex].first = bulletName;
			return bulletTypeIndex;
		}
	}
	return -1;
}

int CInventoryComponent::GetBulletsCount()
{
	int bulletsToReturn = 0;
	for (int i = 0; i < BULLET_TYPE_CAPACITY; i++)
	{
		bulletsToReturn += bullet[i].second;
	}
	return bulletsToReturn;
}

void CInventoryComponent::ClientSelectSlot(int slotId)
{
	SRmi < RMI_WRAP(&CInventoryComponent::ServerSelectSlot)>::InvokeOnServer(this, SlotPass{ slotId });
}

int CInventoryComponent::GetItemSlot(SItemComponent * pGetItem)
{
	if (!pGetItem)
		return -1;

	if (pGetItem->IsWeapon())
	{
		for (int i = 0; i < WEAPON_CAPACITY; i++)
		{
			if (weaponId[i] == pGetItem->GetEntityId())
			{
				return i;
			}
		}
	}
	if (CAmmoClipComponent *pMag = pGetItem->IsAmmoClip())
	{
		for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
		{
			if (ammoClip[i] == pMag->GetEntityId())
			{
				return i;
			}
		}
	}
	return -1;
}

int CInventoryComponent::GetItemSlot(EntityId itemId)
{
	if (itemId <= 0)
		return -1;

	SItemComponent *pItemToGet = gEnv->pEntitySystem->GetEntity(itemId)->GetComponent<SItemComponent>();
	if (pItemToGet->IsWeapon())
	{
		for (int i = 0; i < WEAPON_CAPACITY; i++)
		{
			if (weaponId[i] == itemId)
			{
				return i;
			}
		}
	}
	else if (pItemToGet->IsAmmoClip())
	{
		for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
		{
			if (ammoClip[i] == itemId)
			{
				return i;
			}
		}
	}
	return -1;
}

void CInventoryComponent::SetCurrentItem(EntityId id)
{
	if (id <= 0)
	{
		pCurrentItem = nullptr;
		return;
	}

	pCurrentItem = gEnv->pEntitySystem->GetEntity(id)->GetComponent<SItemComponent>();
}

EntityId CInventoryComponent::GetItemId(int slotId)
{
	return weaponId[slotId];
}

SItemComponent * CInventoryComponent::GetWeaponItem(int slotId)
{
	if (weaponId[slotId] > 0)
		return gEnv->pEntitySystem->GetEntity(weaponId[slotId])->GetComponent<SItemComponent>();
	return nullptr;
}

SItemComponent * CInventoryComponent::GetAmmoClipItem(int slotId)
{
	if (ammoClip[slotId] > 0)
		return gEnv->pEntitySystem->GetEntity(ammoClip[slotId])->GetComponent<SItemComponent>();
	return nullptr;
}

EntityId CInventoryComponent::GetAmmoClipItemId(int slotId)
{
	return ammoClip[slotId];
}

SItemComponent * CInventoryComponent::GetSelectedItem()
{
	if (pCurrentItem)
		return pCurrentItem;

	if (activeSlot < 0)
		return nullptr;

	if (IEntity *pEnt = gEnv->pEntitySystem->GetEntity(weaponId[activeSlot]))
		return pEnt->GetComponent<SItemComponent>();

	return nullptr;
}

CWeaponComponent *CInventoryComponent::HasWeaponSelected()
{
	if (activeSlot < 0)
		return nullptr;

	if (IEntity *pEntity = gEnv->pEntitySystem->GetEntity(weaponId[activeSlot]))
	{
		if (CWeaponComponent *pWeapon = pEntity->GetComponent<CWeaponComponent>())
			return pWeapon;
	}

	return nullptr;
}

CAmmoPackComponent *CInventoryComponent::GetCarriedAmmoPack()
{
	if (activeSlot < 0)
		return nullptr;

	if (IEntity *pEntity = gEnv->pEntitySystem->GetEntity(weaponId[activeSlot]))
	{
		if (CAmmoPackComponent *pAmmoPack = pEntity->GetComponent<CAmmoPackComponent>())
			return pAmmoPack;
	}

	return nullptr;
}

int CInventoryComponent::GetAmmoClipNumForCurrentWeapon()
{
	if (CWeaponComponent *pWeapon = HasWeaponSelected())
	{
		int count = 0;
		for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
		{
			if (ammoClip[i] > 0)
			{
				IEntity *pAmmoClipEntity = gEnv->pEntitySystem->GetEntity(ammoClip[i]);
				pWeapon->IsAmmoClipCorrect(pAmmoClipEntity->GetClass()->GetName());
				count++;
			}
		}
		return count;
	}
	return -1;
}

CAmmoClipComponent *CInventoryComponent::TakeNextAmmoClip(CWeaponComponent * pWeaponRequestor)
{
	if (!pWeaponRequestor)
		return nullptr;

	CAmmoClipComponent *pReturnAmmoClip = nullptr;

	for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
	{
		if (ammoClip[i] > 0)
		{
			if (CAmmoClipComponent *pNextAmmoClip = gEnv->pEntitySystem->GetEntity(ammoClip[i])->GetComponent<CAmmoClipComponent>())
			{
				if (pWeaponRequestor->IsAmmoClipCorrect(pNextAmmoClip->GetEntity()->GetClass()->GetName()) && !pNextAmmoClip->IsEmpty())
				{
					ammoClip[i] = 0;
					pReturnAmmoClip = pNextAmmoClip;
					break;
				}
			}
		}
	}
	ServerUpdateStaticData();
	return pReturnAmmoClip;
}

CAmmoClipComponent *CInventoryComponent::TakeNextEmptyAmmoClip(CWeaponComponent * pWeaponRequestor)
{
	if (!pWeaponRequestor)
		return nullptr;

	CAmmoClipComponent *pReturnAmmoClip = nullptr;

	for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
	{
		if (ammoClip[i] > 0)
		{
			if (CAmmoClipComponent *pNextAmmoClip = gEnv->pEntitySystem->GetEntity(ammoClip[i])->GetComponent<CAmmoClipComponent>())
			{
				if (pWeaponRequestor->IsAmmoClipCorrect(pNextAmmoClip->GetEntity()->GetClass()->GetName()) && pNextAmmoClip->IsEmpty())
				{
					ammoClip[i] = 0;
					pReturnAmmoClip = pNextAmmoClip;
					break;
				}
			}
		}
	}
	ServerUpdateStaticData();
	return pReturnAmmoClip;
}

int CInventoryComponent::FillAmmoClip(CAmmoClipComponent * pCurrentAmmoClip)
{
	if (!pCurrentAmmoClip)
		return -1;

	int bulletCount = 0;
	//for (int i = 0; i < sFreeBullet.size(); i++)
	//{
	//	if (!sFreeBullet[i].empty() && pCurrentAmmoClip->IsBulletCorrect(sFreeBullet[i]))
	//	{
	//		if (pCurrentAmmoClip->GetCurrentAmmo() < pCurrentAmmoClip->sAmmoClipProperties.iMaxAmmo)
	//		{
	//			pCurrentAmmoClip->FillUp();
	//			sFreeBullet[i].Empty();
	//			bulletCount += 1;
	//		}
	//	}
	//}
	return bulletCount;
}

string CInventoryComponent::GetCurrentItemName()
{
	if (SItemComponent *pSelectedItem = GetSelectedItem())
	{
		return pSelectedItem->GetItemName();
	}
	return "no_weapon";
}

CWeaponComponent *CInventoryComponent::GetSelectedWeapon()
{
	return HasWeaponSelected();
}

void CInventoryComponent::ClearInventory()
{
	//for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
	//{
	//	if (i < WEAPON_CAPACITY)
	//		weaponId[i] = 0;
	//	ammoClip[i] = 0;
	//}
	//sFreeBullet.clear();
	//activeSlot = -1;
	////we need to clear also ui inventory, but we need to make sure it is done only on client, cause server does not know of any UI system
	//if (!gEnv->bServer)
	//{
	//	CPlayerComponent *player = m_pEntity->GetComponent<CPlayerComponent>();
	//	player->GetUISystem()->ClearInventory();
	//}
}

string CInventoryComponent::SchematycEntityToShortName(string schematycName)
{
	string sShortName;
	string sLongName = schematycName;
	sLongName.erase(0, sLongName.find_last_of(':') + 1);
	string sFirst;
	sFirst.SetAt(0, sLongName[0]);
	sFirst.MakeUpper();
	sLongName.erase(0, 1);
	sShortName = sFirst + sLongName;
	return sShortName;
}

void CInventoryComponent::SelectSlot(int slotId)
{
	int deselectedSlot = -1;
	if (activeSlot > -1)
	{
		deselectedSlot = activeSlot;
		activeSlot = -1;

		IEntity *pDeselectedEntity = gEnv->pEntitySystem->GetEntity(weaponId[deselectedSlot]);
		SItemComponent *pDeselectedItem = pDeselectedEntity->GetComponent<SItemComponent>();
		pDeselectedItem->DelegateAuthority(0);
	}
	if (deselectedSlot != slotId)
	{
		activeSlot = slotId;

		IEntity *pSelectedEntity = gEnv->pEntitySystem->GetEntity(weaponId[activeSlot]);
		SItemComponent *pSelectedItem = pSelectedEntity->GetComponent<SItemComponent>();
		pSelectedItem->DelegateAuthority(m_pEntity->GetNetEntity()->GetChannelId());
	}

	ServerUpdateStaticData();
}

void CInventoryComponent::SelectForeignItem(EntityId id)
{
	//deselect currently selected item
	SelectSlot(activeSlot);
	//set foreign item 
	foreignItem = id;
	foreignItemComp = id;
	ServerUpdateStaticData();
}

void CInventoryComponent::ServerDropEverything()
{
	activeSlot = -1;
	for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
	{
		bullet[i].first.Empty();
		bullet[i].second = 0;

		if (IEntity *pEnt = gEnv->pEntitySystem->GetEntity(ammoClip[i]))
		{
			SItemComponent *item = pEnt->GetComponent<SItemComponent>();
			item->ServerDrop(NoParams(), nullptr);
			ammoClip[i] = 0;
			ammoClipComp[i] = 0;
		}
		if (i < WEAPON_CAPACITY)
		{
			if (IEntity *pEnt = gEnv->pEntitySystem->GetEntity(weaponId[i]))
			{
				SItemComponent *item = pEnt->GetComponent<SItemComponent>();
				item->ServerDrop(NoParams(), nullptr);
				weaponId[i] = 0;
				weaponIdComp[i] = 0;
			}
		}
	}
	pCurrentItem = nullptr;
	if (IEntity *pEnt = gEnv->pEntitySystem->GetEntity(foreignItem))
	{
		SItemComponent *item = pEnt->GetComponent<SItemComponent>();
		item->ServerDrop(NoParams(), nullptr);
		foreignItem = 0;
		foreignItemComp = 0;
	}

	ServerUpdateStaticData();
}

void CInventoryComponent::ServerReset()
{
	for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
	{
		if (i < WEAPON_CAPACITY)
		{
			weaponId[i] = 0;
			weaponIdComp[i] = 0;
		}
		ammoClip[i] = 0;
		ammoClipComp[i] = 0;

		bullet[i].first.Empty();
		bullet[i].second = 0;
	}
	pCurrentItem = nullptr;
	foreignItem = 0;
	foreignItemComp = 0;

	ServerUpdateStaticData();
}
