/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Modified camera component. Allows access to its properties from c++

--------------------------------------------------------------------------------- */

#pragma once

#include <CrySchematyc/MathTypes.h>
#include <CrySchematyc/Reflection/TypeDesc.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>

#include <CrySystem/VR/IHMDDevice.h>
#include <CrySystem/VR/IHMDManager.h>
#include "DefaultComponents/Audio/ListenerComponent.h"

struct SCameraComponent : public IEntityComponent, public IHmdDevice::IAsyncCameraCallback
{
protected:
	virtual void   Initialize() final;
	virtual void   ProcessEvent(SEntityEvent& event) final;
	virtual uint64 GetEventMask() const final;
	virtual bool OnAsyncCameraCallback(const HmdTrackingState& state, IHmdDevice::AsyncCameraContext& context) final;

public:
	virtual ~SCameraComponent() override;

	static void ReflectType(Schematyc::CTypeDesc<SCameraComponent>& desc)
	{
		desc.SetGUID("{FCFCFE3B-84A1-445F-A57F-429F9BDFAFDD}"_cry_guid);
	}

	virtual void Activate()
	{
		if (s_pActiveCamera != nullptr)
		{
			SCameraComponent* pPreviousCamera = s_pActiveCamera;
			s_pActiveCamera = nullptr;
			m_pEntity->UpdateComponentEventMask(pPreviousCamera);

			pPreviousCamera->m_pAudioListener->SetActive(false);
		}

		s_pActiveCamera = this;

		if (s_pActiveCamera->m_pAudioListener != nullptr)
		{
			s_pActiveCamera->m_pAudioListener->SetActive(true);
		}

		m_pEntity->UpdateComponentEventMask(this);

		if (IHmdDevice* pDevice = gEnv->pSystem->GetHmdManager()->GetHmdDevice())
		{
			pDevice->SetAsynCameraCallback(this);
		}
	}

	bool                 IsActive() const { return s_pActiveCamera == this; }

	virtual void         EnableAutomaticActivation(bool bActivate) { m_bActivateOnCreate = bActivate; }
	bool                 IsAutomaticallyActivated() const { return m_bActivateOnCreate; }

	virtual void         SetNearPlane(float nearPlane) { m_nearPlane = nearPlane; }
	float                GetNearPlane() const { return m_nearPlane; }

	virtual void         SetFieldOfView(CryTransform::CAngle angle) { m_fieldOfView = angle; }
	CryTransform::CAngle GetFieldOfView() const { return m_fieldOfView; }

	virtual CCamera& GetCamera() { return m_camera; }
	const CCamera&   GetCamera() const { return m_camera; }

protected:
	bool                                               m_bActivateOnCreate = true;
	Schematyc::Range<0, 32768>                         m_nearPlane = 0.25f;
	CryTransform::CClampedAngle<20, 360>               m_fieldOfView = 75.0_degrees;

	CCamera                                            m_camera;
	Cry::Audio::DefaultComponents::CListenerComponent* m_pAudioListener = nullptr;

	static SCameraComponent*                           s_pActiveCamera;
};
