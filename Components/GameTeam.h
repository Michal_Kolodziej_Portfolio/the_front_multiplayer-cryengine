/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Game team class is component attached to server component that manages teams

--------------------------------------------------------------------------------- */

#pragma once

#include "CryEntitySystem/IEntityComponent.h"

#define MAX_TEAM_PLAYERS 32

class CAccountComponent;

class CGameTeam : public IEntityComponent
{
	const EEntityAspects kTeamInitialData = eEA_GameServerA;
	const EEntityAspects kTeamStaticData = eEA_GameServerStatic;
public:
	CGameTeam() = default;
	CGameTeam::CGameTeam(string _team_name, int _index)
		: sTeamName(_team_name)
		, iIndex(_index)
	{
		CryLogAlways("Team %s initialized with index %d", sTeamName, iIndex);
	}
	CGameTeam::~CGameTeam() {}

	// IEntityComponent
	virtual void Initialize() override {}
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	static void ReflectType(Schematyc::CTypeDesc<CGameTeam>& desc);
	// ~IEntityComponent

	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kTeamInitialData | kTeamStaticData; };

	void AddAccount(CAccountComponent *pAccountToAdd);
	void RemoveAccount(CAccountComponent *pAccountToRemove);

	int GetIndex() { return iIndex; }
	int GetCount() { return iListLength; }

	void Set(string sNewName, int iNewIndex);

	string GetTeamName() { return sTeamName; }
	void SetTeamName(string name) { sTeamName = name; NetMarkAspectsDirty(kTeamInitialData); }

	CAccountComponent *GetAccountAtIndex(int index);

	EntityId GetMemberId(int index) { return list_serializer[index]; }

	int GetScore() { return iScore; }
	void AddScore() { iScore++; NetMarkAspectsDirty(kTeamInitialData); }

	void ServerReset() { iScore = 0; NetMarkAspectsDirty(kTeamInitialData); }
private:
	int iScore = 0;
	string sTeamName;
	int iListLength = 0;
	int iIndex = -1;
	std::vector<CAccountComponent *> pList, pList_last;
	EntityId list_serializer[MAX_TEAM_PLAYERS] = { 0 };
	EntityId list_serializerComp[MAX_TEAM_PLAYERS] = { 0 };
};
