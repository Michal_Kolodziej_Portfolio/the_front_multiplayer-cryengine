#include "StdAfx.h"
#include "WeaponComponent.h"


void CWeaponComponent::ClientActionRegister()
{
	pInput->RegisterAction("item", "weapon_reload", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_Reload, activationMode); });
	pInput->BindAction("item", "weapon_reload", eAID_KeyboardMouse, EKeyId::eKI_R);

	pInput->RegisterAction("item", "weapon_shoot", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_Shoot, activationMode); });
	pInput->BindAction("item", "weapon_shoot", eAID_KeyboardMouse, EKeyId::eKI_Mouse1);

	pInput->RegisterAction("item", "switch_firemode", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_SwitchFiremode, activationMode); });
	pInput->BindAction("item", "switch_firemode", eAID_KeyboardMouse, EKeyId::eKI_X);
}
//ACTIONS HAPPEN ONLY ON SERVER SIDE
void CWeaponComponent::ServerActionRegister()
{
	using std::placeholders::_1;

	SERVER_REGISTER_ACTION((ItemInputFlags)IEInputFlag::Action_Reload, std::bind(&CWeaponComponent::Action_Reload, this, _1));
	SERVER_REGISTER_ACTION((ItemInputFlags)IEInputFlag::Action_Shoot, std::bind(&CWeaponComponent::Action_Shoot, this, _1));
	SERVER_REGISTER_ACTION((ItemInputFlags)IEInputFlag::Action_SwitchFiremode, std::bind(&CWeaponComponent::Action_SwitchFiremode, this, _1));
}

void CWeaponComponent::Action_Reload(int activationMode)
{
	if (activationMode == eIS_Pressed)
		ServerReload();
}

void CWeaponComponent::Action_Shoot(int activationMode)
{
	if (activationMode == eIS_Pressed)
		ServerFire();
	else if (activationMode == eIS_Released)
		ServerStopFire();
}

void CWeaponComponent::Action_SwitchFiremode(int activationMode)
{
	if (activationMode == eIS_Pressed)
		NextFireMode();
}
