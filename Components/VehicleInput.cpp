#include "StdAfx.h"
#include "VehicleComponent.h"


void SVehicleComponent::ClientActionRegister()
{
	pInput->RegisterAction("vehicle", "engine_start", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_EngineToggle, activationMode); });
	pInput->BindAction("vehicle", "engine_start", eAID_KeyboardMouse, EKeyId::eKI_E);

	pInput->RegisterAction("vehicle", "rotateleft", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_MoveLeft, activationMode); });
	pInput->BindAction("vehicle", "rotateleft", eAID_KeyboardMouse, EKeyId::eKI_A);

	pInput->RegisterAction("vehicle", "rotateright", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_MoveRight, activationMode); });
	pInput->BindAction("vehicle", "rotateright", eAID_KeyboardMouse, EKeyId::eKI_D);

	pInput->RegisterAction("vehicle", "moveforward", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_MoveForward, activationMode); });
	pInput->BindAction("vehicle", "moveforward", eAID_KeyboardMouse, EKeyId::eKI_W);

	pInput->RegisterAction("vehicle", "moveback", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_MoveBack, activationMode); });
	pInput->BindAction("vehicle", "moveback", eAID_KeyboardMouse, EKeyId::eKI_S);

	pInput->RegisterAction("vehicle", "gear_next", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_GearUp, activationMode); });
	pInput->BindAction("vehicle", "gear_next", eAID_KeyboardMouse, EKeyId::eKI_Period);

	pInput->RegisterAction("vehicle", "gear_prev", [this](int activationMode, float value) { HandleInputFlagChange((ItemInputFlags)IEInputFlag::Action_GearDown, activationMode); });
	pInput->BindAction("vehicle", "gear_prev", eAID_KeyboardMouse, EKeyId::eKI_Comma);
}
//ACTIONS HAPPEN ONLY ON SERVER SIDE
void SVehicleComponent::ServerActionRegister()
{
	using std::placeholders::_1;

	SERVER_REGISTER_ACTION((ItemInputFlags)IEInputFlag::Action_EngineToggle, std::bind(&SVehicleComponent::Action_EngineToggle, this, _1));
	SERVER_REGISTER_ACTION((ItemInputFlags)IEInputFlag::Action_GearUp, std::bind(&SVehicleComponent::Action_GearUp, this, _1));
	SERVER_REGISTER_ACTION((ItemInputFlags)IEInputFlag::Action_GearDown, std::bind(&SVehicleComponent::Action_GearDown, this, _1));
}

void SVehicleComponent::Action_EngineToggle(int activationMode)
{
	if (activationMode == eIS_Pressed)
	{
		//if engine is already working, then shut it down
		if (IsEngineWorking())
			ServerShutdownEngine();
		//if not, then attempt start engine
		else
			ServerStartupEngine();
	}
	else if (activationMode == eIS_Released)
	{
		//if button is released, player stopped trying to startup engine, then abort startup
		if (!IsEngineWorking())
			ServerStartupEngineAbort();
	}
}

void SVehicleComponent::Action_GearUp(int activationMode)
{
	if (activationMode == eIS_Pressed)
	{
		NextGear();
	}
}

void SVehicleComponent::Action_GearDown(int activationMode)
{
	if (activationMode == eIS_Pressed)
	{
		PreviousGear();
	}
}
