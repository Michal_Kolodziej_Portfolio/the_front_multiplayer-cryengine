#include "StdAfx.h"
#include "ItemComponent.h"
#include "UISystem/HudComponent.h"
#include "WeaponComponent.h"
#include "AmmoClipComponent.h"
#include "VehicleComponent.h"
#include "PlayerUIComponent.h"
#include "GlobalResources.h"
#include "CryNetwork/Rmi.h"

void SItemComponent::Initialize()
{
	CreateItemName();
	InitClass();
	Spawn();
	if (gEnv->bServer)
		gEnv->pNetContext->BindObject(GetEntityId(), 0, GetNetSerializeAspectMask(), true);
	m_pEntity->GetNetEntity()->BindToNetwork();
	if (!gEnv->bServer)
	{
		pInput = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CInputComponent>();
		RegisterActions();
		ClientActionRegister();
	}
	else
	{
		//get server and add this item to the items server list
		GetServer()->ServerAddItem(this);
		//set reset properties
		sItemStartupProperties = sItemProperties;
		vStartPos = m_pEntity->GetWorldPos();
		qStartOrientation = m_pEntity->GetWorldRotation();
	}
	SRmi<RMI_WRAP(&SItemComponent::ServerDrop)>::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
}

void SItemComponent::ProcessEvent(SEntityEvent & event)
{
	if (event.event == ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED)
	{
		if (sItemProperties != sPrevItemProperties)
		{
			sPrevItemProperties = sItemProperties;
			ReLoadMesh();
			ReLoadPhysics();
		}
	}
	if (event.event == ENTITY_EVENT_UPDATE)
	{
		//here we will update item's position to all clients. Items have server authority that's why we will send the position only from server to clients
		//eventually we will set start position from client to server, but only in the situations where we drop it etc, but info will be just as a request, and actual reposition will be sent from server anyway
		if (gEnv->bServer)
		{
			//Seting position if item is carried by someone; we can't use attachments, because position is updated before item is detached; might fix it at some point
			if (sItemProperties.pPlayerCarrier)
			{
				Vec3 attPos = sItemProperties.pPlayerCarrier->GetAttachmentComponent()->GetWeaponAttachmentWorldPos();
				m_pEntity->SetPos(attPos);
			}
			//Update position for serialization
			if (sItemProperties.currentPosition != m_pEntity->GetWorldPos() || sItemProperties.currentOrientation != m_pEntity->GetWorldRotation())
			{
				sItemProperties.currentPosition = m_pEntity->GetWorldPos();
				sItemProperties.currentOrientation = m_pEntity->GetWorldRotation();
				SvUpdatePosition();
			}
		}
		else
		{
			if (!sItemProperties.bIsAttached)
			{
				if (sItemProperties.currentPosition != m_pEntity->GetWorldPos() || sItemProperties.currentOrientation != m_pEntity->GetWorldRotation())
				{
					if (sItemProperties.currentPosition != ZERO)
					{
						m_pEntity->SetPos(sItemProperties.currentPosition);
						m_pEntity->SetRotation(sItemProperties.currentOrientation);
					}
				}
			}
		}
	}
	//we will call proccess event on all classes that inherit from this base structure
	ClassProcessEvent(event);
}

uint64 SItemComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE) | BIT64(ENTITY_EVENT_COMPONENT_PROPERTY_CHANGED) | BIT64(ENTITY_EVENT_TIMER);
}

void SItemComponent::ReflectType(Schematyc::CTypeDesc<SItemComponent>& desc)
{
	desc.SetGUID("{4E12912C-A701-4B90-BD29-2F4A2DDC52B5}"_cry_guid);
}

bool SItemComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kItemStaticData)
	{
		ser.BeginGroup("StaticData");

		ser.Value("physics", GetProperties()->bPhysics, 'bool');
		ser.Value("ownerId", sItemProperties.ownerId);
		ser.Value("ownerIdComp", sItemProperties.ownerIdComp, 'eid');

		m_pEntity->EnablePhysics(GetProperties()->bPhysics);

		ser.EndGroup();
	}

	ClassNetSerialize(ser, aspect, profile, flags);
	
	if (aspect == kItemPositionAspect)
	{
		ser.BeginGroup("PositionSynchronization");

		ser.Value("currentPosition", sItemProperties.currentPosition, 'wrld');
		ser.Value("currentOrientation", sItemProperties.currentOrientation, 'ori3');

		if (gEnv->bServer)
		{
			if (sItemProperties.currentPosition != ZERO)
			{
				m_pEntity->SetPos(sItemProperties.currentPosition);
				m_pEntity->SetRotation(sItemProperties.currentOrientation);
			}
		}

		ser.EndGroup();
	}
	if (aspect == kItemInput)
	{
		ser.BeginGroup("InputData");

		auto inputs = sItemProperties.m_inputFlags;
		auto prevState = sItemProperties.m_inputFlags;

		if (gEnv->bServer)
			sItemProperties.m_prevInputFlags = sItemProperties.m_inputFlags;

		ser.Value("inputFlags", sItemProperties.m_inputFlags, 'ui16');

		if (ser.IsReading())
		{
			auto changedKeys = inputs ^ sItemProperties.m_inputFlags;

			auto pressedKeys = changedKeys & inputs;
			if (pressedKeys != 0)
			{
				HandleInputFlagChange(pressedKeys, eIS_Pressed);
			}

			auto releasedKeys = changedKeys & prevState;
			if (releasedKeys != 0)
			{
				HandleInputFlagChange(pressedKeys, eIS_Released);
			}

			if (gEnv->bServer)
			{
				ServerActionRegister();
			}
		}

		ser.EndGroup();
	}
	return true;
}

void SItemComponent::CreateItemName()
{
	//this method is called only once to create item short name
	//reason for this is that schematyc entities use very long naming "folder::entity_name"
	//so in order to show pickup message etc we need to have simple name with upper cases only
	string sLongName = GetEntity()->GetClass()->GetName();
	sLongName.erase(0, sLongName.find_last_of(':') + 1);
	string sFirst;
	sFirst.SetAt(0, sLongName[0]);
	sFirst.MakeUpper();
	sLongName.erase(0, 1);
	sItemName = sFirst + sLongName;
}

CPlayerComponent * SItemComponent::GetOwner()
{
	//we check if we should bother doing anything with this id
	if (sItemProperties.ownerId <= 0)
		return nullptr;

	//return owner's pointer
	if (IEntity *pOwnerEntity = gEnv->pEntitySystem->GetEntity(sItemProperties.ownerId))
		return pOwnerEntity->GetComponent<CPlayerComponent>();

	return nullptr;
}

void SItemComponent::ServerPickup(CPlayerComponent * pNewOwner)
{	
	//we have to check if new owner is valid player; it's also good moment to check whether the item is pickable; then if item is available for pickup, we should check if picker has available inventory slot
	if (!pNewOwner || !IsPickable() || !pNewOwner->GetInventory()->ServerAddItem(this))
		return;

	//set owner first
	SetOwner(pNewOwner->GetEntityId());
	//disable collision of this item; we have special variable for this to be serialized to clients
	GetProperties()->bPhysics = false;
	//update aspect to sync clients with owner and physics
	ServerUpdateStaticData();
}

void SItemComponent::DelegateAuthority(int channelId)
{
	//channel that should control item
	INetChannel *net = gEnv->pGameFramework->GetNetChannel(channelId);
	//delegate authority to this channel (0 for server)
	gEnv->pNetContext->DelegateAuthority(GetEntityId(), net);
	m_pEntity->GetNetEntity()->SetChannelId(channelId);
}

bool SItemComponent::ServerDrop(NoParams && p, INetChannel *)
{
	//get valid owner
	if (IEntity *pOwnerEnt = gEnv->pEntitySystem->GetEntity(sItemProperties.ownerId))
	{
		if (CPlayerComponent *pPlayerOwner = pOwnerEnt->GetComponent<CPlayerComponent>())
		{
			Vec3 attPos = pPlayerOwner->GetAttachmentComponent()->GetAttachmentPositionForItem(this);
			Quat ownerRot = pOwnerEnt->GetWorldRotation();
			//save position of attachment for this item before removing
			//remove this item from owner's inventory
			pPlayerOwner->GetInventory()->ServerRemoveItem(this);
			//nullify owner
			SetOwner(0);
			//reset physics
			GetProperties()->bPhysics = true;
			//send over to clients
			ServerUpdateStaticData();
			//physics will get enabled later with serialization process, but on server side we have to do it now in order to apply impulse
			m_pEntity->EnablePhysics(true);
			//set item position; set rotation taken from owner to make item fly in the right direction
			m_pEntity->SetPos(attPos);
			m_pEntity->SetRotation(ownerRot);
			//delegate authority back to server
			DelegateAuthority(0);
			//apply impulse on drop
			Vec3 impulseDir = pOwnerEnt->GetForwardDir();
			pe_action_impulse imp;
			imp.impulse = impulseDir * 2.5f;
			m_pEntity->GetPhysicalEntity()->Action(&imp);
		}
	}
	return true;
}


void SItemComponent::ClientDrop()
{
	SRmi<RMI_WRAP(&SItemComponent::ServerDrop)>::InvokeOnServer(this, NoParams{});
}

void SItemComponent::ServerCarry(CPlayerComponent * pCarrier)
{
	//at the moment 40kg is max weight that player can carry TODO: make it dependable on player's strength
	if (!pCarrier || GetProperties()->sPhysicsProperties.fMass > 40.f)
		return;

	sItemProperties.pPlayerCarrier = pCarrier;
	DelegateAuthority(pCarrier->GetEntity()->GetNetEntity()->GetChannelId());
	GetProperties()->bPhysics = false;
	pCarrier->GetInventory()->SelectForeignItem(GetEntityId());

	ServerUpdateStaticData();
}

void SItemComponent::ServerPlace()
{
	if (!sItemProperties.pPlayerCarrier)
		return;

	DelegateAuthority(0);
	GetProperties()->bPhysics = true;
	m_pEntity->EnablePhysics(true);
	sItemProperties.pPlayerCarrier->GetInventory()->SelectForeignItem(0);
	m_pEntity->SetPos(sItemProperties.pPlayerCarrier->GetTargetPosition());
	sItemProperties.pPlayerCarrier = nullptr;

	ServerUpdateStaticData();
}

void SItemComponent::LoadCharacter()
{
	//loading character with skeleton to character slot
	string charPath = sItemProperties.sRenderProperties.sCharPath.value.c_str();
	if (charPath.empty())
		return;

	GetEntity()->LoadCharacter(Character_slot, charPath);

	string matPath = sItemProperties.sRenderProperties.sMaterial.value.c_str();
	if (matPath.empty())
		return;

	if (IMaterial *pMat = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(matPath))
	{
		GetEntity()->SetMaterial(pMat);
	}
}

void SItemComponent::LoadGeometry()
{
	//loading geometry to geometry slot
	string geomPath = sItemProperties.sRenderProperties.sGeomPath.value.c_str();
	if (geomPath.empty())
		return;

	GetEntity()->LoadGeometry(Geometry_slot, geomPath);

	string matPath = sItemProperties.sRenderProperties.sMaterial.value.c_str();
	if (matPath.empty())
		return;

	if (IMaterial *pMat = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(matPath))
	{
		GetEntity()->SetMaterial(pMat);
	}
}

void SItemComponent::ReLoadMesh()
{
	//reloading both meshes
	//we need to have character mesh for weapons in order to be able to have aim posision, bullet output, ammo clip attachment etc.
	LoadGeometry();
	LoadCharacter();
}

void SItemComponent::ReLoadPhysics()
{
	//just physicalizing
	SEntityPhysicalizeParams params;
	params.type = PE_RIGID;
	params.mass = sItemProperties.sPhysicsProperties.fMass;
	GetEntity()->Physicalize(params);
}

void SItemComponent::Spawn()
{
	//it's basically resetting this item
	ReLoadMesh();
	ReLoadPhysics();
}
//very useful for various types of items so we don't have to cast it manually every time
CWeaponComponent * SItemComponent::IsWeapon()
{
	return m_pEntity->GetComponent<CWeaponComponent>();
}

CAmmoClipComponent * SItemComponent::IsAmmoClip()
{
	return m_pEntity->GetComponent<CAmmoClipComponent>();
}

CAmmoPackComponent * SItemComponent::IsAmmoPack()
{
	return m_pEntity->GetComponent<CAmmoPackComponent>();
}

SVehicleComponent * SItemComponent::IsVehicle()
{
	return m_pEntity->GetComponent<SVehicleComponent>();
}

void SItemComponent::ServerReset()
{
	m_pEntity->SetPos(vStartPos);
	m_pEntity->SetRotation(qStartOrientation);
	sItemProperties = sItemStartupProperties;
	ServerResetClass();

	ServerUpdateStaticData();
	SvUpdatePosition();
}