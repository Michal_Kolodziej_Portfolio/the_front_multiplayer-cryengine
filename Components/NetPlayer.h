/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Defines all parameters for RMIs

--------------------------------------------------------------------------------- */

#pragma once
#include <CryEntitySystem/IEntityComponent.h>

enum TeamLobby
{
	Team_NO_TEAM = -1,
	Team_Lobby_Waiting = 0,
	Team_Lobby_01,
	Team_Lobby_02
};

struct NoParams
{
	void SerializeWith(TSerialize ser)
	{
	}
};
struct MessageParams
{
	string message;
	string str1;
	string str2;
	int int1;
	int int2;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("message", message, 'stab');
		ser.Value("str1", str1, 'stab');
		ser.Value("str2", str2, 'stab');
		ser.Value("int1", int1);
		ser.Value("int2", int2);
	}
};
struct LoginParams
{
	string login;
	string password;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("login", login, 'stab');
		ser.Value("password", password, 'stab');
	}
};
struct LoginParamsWithId
{
	string login;
	string password;
	bool access;
	EntityId entityId;
	EntityId entityIdComp;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("login", login, 'stab');
		ser.Value("password", password, 'stab');
		ser.Value("access", access, 'bool');
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
	}
};
struct BooleanPass
{
	bool boolean;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("boolean", boolean, 'bool');
	}
};
struct EntityIdPass
{
	EntityId entityId;
	EntityId entityIdComp;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
	}
};
struct EntityIdWithString
{
	EntityId entityId;
	EntityId entityIdComp;
	string str;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("str", str, 'stab');
	}
};
struct VehicleMovementParams
{
	EntityId entityId;
	EntityId entityIdComp;
	int movement;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("movement", movement);
	}
};
struct GearPass
{
	EntityId entityId;
	EntityId entityIdComp;
	int iGear;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("iGear", iGear);
	}
};
struct WeaponPositionPass
{
	EntityId entityId;
	EntityId entityIdComp;
	Vec3 weaponPos;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("weaponPos", weaponPos, 'wrld');
	}
};
struct AmmoPackDropParams
{
	EntityId ammoPackId;
	EntityId ammoPackIdComp;
	Vec3 pos;
	Quat orient;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("ammoPackId", ammoPackId);
		ser.Value("ammoPackIdComp", ammoPackIdComp, 'eid');
		ser.Value("pos", pos, 'wrld');
		ser.Value("orient", orient, 'ori3');
	}
};
struct AttachToPlayerParams
{
	EntityId entityId;
	EntityId entityIdComp;
	int slot;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("slot", slot );
	}
};
struct SwapSlotsParams
{
	int slot_first;
	int slot_second;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("slot_first", slot_first);
		ser.Value("slot_second", slot_second);
	}
};
struct AmmoPackManagementParams
{
	int bulletCount;
	bool bDeposit;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("bulletCount", bulletCount);
		ser.Value("bDeposit", bDeposit, 'bool');
	}
};
struct CaptureAreaSynchronizeParams
{
	EntityId entityId;
	EntityId entityIdComp;
	int iCapTeam;
	bool bCaptured;
	bool bContested;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("iCapTeam", iCapTeam);
		ser.Value("bCaptured", bCaptured, 'bool');
		ser.Value("bContested", bContested, 'bool');
	}
};
struct FillAmmoClipParams
{
	EntityId ammoClipId;
	EntityId ammoClipIdComp;
	int bulletCount;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("ammoClipId", ammoClipId);
		ser.Value("ammoClipIdComp", ammoClipIdComp, 'eid');
		ser.Value("bulletCount", bulletCount);
	}
};
struct SlotPass
{
	int slot;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("slot", slot);
	}
};
struct PickupDropParams
{
	EntityId entityId;
	EntityId entityIdComp;
	int slotId;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("slotId", slotId);
	}
};
struct SelectParams
{
	int slotId;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("slotId", slotId);
	}
};
struct FireParams
{
	EntityId weaponId;
	EntityId weaponIdComp;
	Vec3 bulletDirection;
	int ammo;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("weaponId", weaponId);
		ser.Value("weaponIdComp", weaponIdComp, 'eid');
		ser.Value("bulletDirection", bulletDirection, 'dir3');
		ser.Value("ammo", ammo);
	}
};
struct FullSerializeItemParams
{
	EntityId itemId;
	EntityId itemIdComp;
	Vec3 itemPos;
	bool bPickable;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("itemId", itemId);
		ser.Value("itemIdComp", itemIdComp, 'eid');
		ser.Value("itemPos", itemPos, 'wrld');
		ser.Value("bPickable", bPickable, 'bool');
	}
};
struct FullSerializeWeaponParams
{
	EntityId itemId;
	EntityId itemIdComp;
	EntityId currentAmmoClip;
	EntityId currentAmmoClipComp;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("itemId", itemId);
		ser.Value("itemIdComp", itemIdComp, 'eid');
		ser.Value("currentAmmoClip", currentAmmoClip);
		ser.Value("currentAmmoClipComp", currentAmmoClipComp, 'eid');
	}
};
struct AttachAmmoClipToWeaponParams
{
	EntityId weaponId;
	EntityId weaponIdComp;
	EntityId ammoClip;
	EntityId ammoClipComp;
	int slotId;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("weaponId", weaponId);
		ser.Value("weaponIdComp", weaponIdComp, 'eid');
		ser.Value("ammoClip", ammoClip);
		ser.Value("ammoClipComp", ammoClipComp, 'eid');
		ser.Value("slotId", slotId);
	}
};
struct SprintParams
{
	int activationMode;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("activationMode", activationMode);
	}
};
struct HeartbeatParams
{
	float fCurrentStamina;
	float fCurrentHealth;
	float fDayTime;
	int iTeam01_Score;
	int iTeam02_Score;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("fCurrentStamina", fCurrentStamina);
		ser.Value("fCurrentHealth", fCurrentHealth);
		ser.Value("fDayTime", fDayTime);
		ser.Value("iTeam01_Score", iTeam01_Score);
		ser.Value("iTeam02_Score", iTeam02_Score);
	}
};
struct HitParams
{
	EntityId attackerId;
	EntityId attackerIdComp;
	Vec3 attackerPosOnShot;
	float damage;
	Vec3 pushDir;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("attackerId", attackerId);
		ser.Value("attackerIdComp", attackerIdComp);
		ser.Value("attackerPosOnShot", attackerPosOnShot, 'wrld');
		ser.Value("damage", damage);
		ser.Value("pushDir", pushDir, 'dir3');
	}
};
struct LobbyParams
{
	string login;
	EntityId entityId;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("login", login, 'stab');
		ser.Value("entityId", entityId);
	}
};
struct NewClientInLobbyParams
{
	string sClient;
	int index; //Only from server to client
	void SerializeWith(TSerialize ser)
	{
		ser.Value("sClient", sClient, 'stab');
		ser.Value("index", index);
	}
};
struct AddToLobbyParams
{
	string sClient;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("sClient", sClient, 'stab');
	}
};
struct WaitingListParams
{
	string c01;
};
struct SetClientInLobbyParams
{
	string sClient;
	int team;
	int index;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("sClient", sClient, 'stab');
		ser.Value("team", team);
		ser.Value("index", index);
	}
};
struct SetClientTeamInLobby
{
	string sClient;
	int team;
	int index;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("sClient", sClient, 'stab');
		ser.Value("team", team);
		ser.Value("index", index);
	}
}; 
struct TeamParams
{
	EntityId entityId;
	EntityId entityIdComp;
	int team;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("entityId", entityId);
		ser.Value("entityIdComp", entityIdComp, 'eid');
		ser.Value("team", team);
	}
};
struct SetTeamParams
{
	string name;
	int team;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("name", name, 'stab');
		ser.Value("team", team);
	}
};
struct StringPass
{
	string str;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("str", str, 'stab');
	}
};
struct CVarString
{
	string str;
	string str1;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("str", str, 'stab');
		ser.Value("str1", str1, 'stab');
	}
};
struct CVarInt
{
	string str;
	int integer;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("str", str, 'stab');
		ser.Value("integer", integer);
	}
};
struct KillParams
{
	EntityId killerId;
	EntityId victimId;
	Vec3 pushDir;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("killerId", killerId);
		ser.Value("victimId", victimId);
		ser.Value("pushDir", pushDir, 'dir3');
	}
};
struct UpdateResultsParams
{
	string sClient;
	int team;
	int index;
	int kills;
	int deaths;
	void SerializeWith(TSerialize ser)
	{
		ser.Value("sClient", sClient, 'stab');
		ser.Value("team", team);
		ser.Value("index", index);
		ser.Value("kills", kills);
		ser.Value("deaths", deaths);
	}
};
struct PlayerFullSerializeParams
{
	EntityId playerId;
	//Inventory serialize
	EntityId pItem0;
	EntityId pItem1;
	EntityId pItem2;
	EntityId pAmmoClip0;
	EntityId pAmmoClip1;
	EntityId pAmmoClip2;
	EntityId pAmmoClip3;
	EntityId pAmmoClip4;
	EntityId pAmmoClip5;
	EntityId pAmmoClip6;
	EntityId pAmmoClip7;
	EntityId pAmmoClip8;
	EntityId pAmmoClip9;
	EntityId activeItem;
	EntityId curMag;

	void SerializeWith(TSerialize ser)
	{
		ser.Value("playerId", playerId);
		ser.Value("pItem0", pItem0);
		ser.Value("pItem0", pItem1);
		ser.Value("pItem0", pItem2);
		ser.Value("pAmmoClip0", pAmmoClip0);
		ser.Value("pAmmoClip0", pAmmoClip1);
		ser.Value("pAmmoClip0", pAmmoClip2);
		ser.Value("pAmmoClip0", pAmmoClip3);
		ser.Value("pAmmoClip0", pAmmoClip4);
		ser.Value("pAmmoClip0", pAmmoClip5);
		ser.Value("pAmmoClip0", pAmmoClip6);
		ser.Value("pAmmoClip0", pAmmoClip7);
		ser.Value("pAmmoClip0", pAmmoClip8);
		ser.Value("pAmmoClip0", pAmmoClip9);
		ser.Value("activeItem", activeItem);
		ser.Value("curMag", curMag);
	}
};