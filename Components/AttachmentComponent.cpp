#include "AttachmentComponent.h"
#include "CrySchematyc/Env/Elements/EnvComponent.h"
#include "PlayerComponent.h"
#include "ItemComponent.h"
#include "CryNetwork/Rmi.h"
#include "AmmoPackComponent.h"
#include "PlayerUIComponent.h"
#include "GlobalResources.h"

static void RegisterAttachmentComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CAttachmentComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterAttachmentComponent)


#define FIND_EMPTY_SLOT -9

uint64 CAttachmentComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_START_GAME) | BIT64(ENTITY_EVENT_UPDATE);
}

void CAttachmentComponent::ReflectType(Schematyc::CTypeDesc<CAttachmentComponent>& desc)
{
	desc.SetGUID("{17974475-F28B-4FDA-9EB7-8C36ECE20944}"_cry_guid);
}

Vec3 CAttachmentComponent::GetWeaponAttachmentLocalPos()
{
	//used temporarily to calculate aim offset until we have aim poses
	CPlayerComponent *pOwner = m_pEntity->GetComponent<CPlayerComponent>();
	if (ICharacterInstance *pChar = pOwner->GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			if (IAttachment *pAttachment = pMan->GetInterfaceByName("weapon"))
			{
				Vec3 attLocalPos = pAttachment->GetAttModelRelative().t;
				return attLocalPos;
			}
		}
	}
	return Vec3();
}

Vec3 CAttachmentComponent::GetWeaponAttachmentWorldPos()
{
	CPlayerComponent *pOwner = m_pEntity->GetComponent<CPlayerComponent>();
	if (ICharacterInstance *pChar = pOwner->GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			if (IAttachment *pAttachment = pMan->GetInterfaceByName("weapon"))
			{
				Vec3 attLocalPos = pAttachment->GetAttWorldAbsolute().t + Vec3(0.f, 0.f, pAttachment->GetAttAbsoluteDefault().t.z);
				return attLocalPos;
			}
		}
	}
	return Vec3();
}

Vec3 CAttachmentComponent::GetAttachmentPositionForItem(SItemComponent *pItem)
{
	if (!pItem)
		return Vec3();

	CPlayerComponent *pPlayerOwner = m_pEntity->GetComponent<CPlayerComponent>();
	string attName = GetAttachmentNameForItem(pItem);

	if (attName.empty())
		return Vec3();

	if (ICharacterInstance *pChar = pPlayerOwner->GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attName))
			{
				return pAttachment->GetAttWorldAbsolute().t + Vec3(0.f, 0.f, pAttachment->GetAttAbsoluteDefault().t.z);
			}
		}
	}
	return Vec3();
}

string CAttachmentComponent::GetAttachmentNameForItem(SItemComponent * pItem)
{
	if (!pItem)
		return string();

	EntityId itemId = pItem->GetEntityId();
	CPlayerComponent *pPlayerOwner = m_pEntity->GetComponent<CPlayerComponent>();
	CInventoryComponent *pInventory = pPlayerOwner->GetInventory();
	int slot = pInventory->GetItemSlot(itemId);

	if (slot < 0)
		return string();
	
	if (pItem->IsWeapon())
	{
		if (slot == pInventory->GetActiveSlot())
			return "weapon";

		return "back_0" + ToString(slot);
	}

	if (pItem->IsAmmoClip())
		return "ammoclip_0" + ToString(slot);
	
	return string();
}

IAttachment * CAttachmentComponent::GetAttachmentForItem(SItemComponent * pItem)
{
	if (!pItem)
		return nullptr;

	string attName = GetAttachmentNameForItem(pItem);
	CPlayerComponent *pPlayerOwner = m_pEntity->GetComponent<CPlayerComponent>();

	if (attName.empty())
		return nullptr;

	if (ICharacterInstance *pChar = pPlayerOwner->GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attName))
			{
				return pAttachment;
			}
		}
	}
	return nullptr;
}

void CAttachmentComponent::AttachToBack(EntityId itemToAttach, int slotId)
{
	if (slotId < 0)
		return;
	
	const bool bAttach = itemToAttach > 0;
	//first of all we should check if currently active item is the one we're trying to attach to back; and if so, then detach it from the hand first
	if (bAttach)
	{
		if (SItemComponent *pSelectedItem = m_pEntity->GetComponent<CInventoryComponent>()->GetSelectedItem())
		{
			if (itemToAttach == pSelectedItem->GetEntityId())
				AttachToHand(0);
		}
	}

	CPlayerComponent *pPlayerOwner = m_pEntity->GetComponent<CPlayerComponent>();
	CEntityAttachment *pSelectedItem = new CEntityAttachment();

	if (bAttach)
		pSelectedItem->SetEntityId(itemToAttach);

	if (ICharacterInstance *pChar = pPlayerOwner->GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			string attachmentName = "back_0" + ToString(slotId);
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attachmentName))
			{
				if (bAttach)
					pAttachment->AddBinding(pSelectedItem);
				else
					pAttachment->ClearBinding();
			}
		}
	}
}

void CAttachmentComponent::AttachToBelt(EntityId itemToAttach, int slotId)
{
	if (slotId < 0)
		return;

	const bool bAttach = itemToAttach > 0;

	CPlayerComponent *pPlayerOwner = m_pEntity->GetComponent<CPlayerComponent>();
	CEntityAttachment *pSelectedItem = new CEntityAttachment();

	if (bAttach)
		pSelectedItem->SetEntityId(itemToAttach);

	if (ICharacterInstance *pChar = pPlayerOwner->GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			string attachmentName = "ammoclip_0" + ToString(slotId);
			if (IAttachment *pAttachment = pMan->GetInterfaceByName(attachmentName))
			{
				if (bAttach)
					pAttachment->AddBinding(pSelectedItem);
				else
					pAttachment->ClearBinding();
			}
		}
	}
}

void CAttachmentComponent::AttachToHand(EntityId itemToAttach)
{
	const bool bAttach = itemToAttach > 0;

	//first of all we should grab the item we are trying to attach to hand and detach it from its current back attachment
	if (bAttach)
	{
		if (CInventoryComponent *pInventory = m_pEntity->GetComponent<CInventoryComponent>())
		{
			int slot = pInventory->GetItemSlot(itemToAttach);
			if (slot > -1)
				AttachToBack(0, slot);
		}
	}

	CPlayerComponent *pPlayerOwner = m_pEntity->GetComponent<CPlayerComponent>();
	CEntityAttachment *pSelectedItem = new CEntityAttachment();

	if (bAttach)
		pSelectedItem->SetEntityId(itemToAttach);

	if (ICharacterInstance *pChar = pPlayerOwner->GetAnimations()->GetCharacter())
	{
		if (IAttachmentManager *pMan = pChar->GetIAttachmentManager())
		{
			if (IAttachment *pAttachment = pMan->GetInterfaceByName("weapon"))
			{
				if (bAttach)
				{
					pAttachment->AddBinding(pSelectedItem);
					if (bIsLocal)
						pPlayerOwner->GetUISystem()->Activate(pPlayerOwner->GetInventory()->GetItemSlot(itemToAttach));
				}
				else
				{
					pAttachment->ClearBinding();
					if (bIsLocal)
						pPlayerOwner->GetUISystem()->Deactivate();

				}
				pPlayerOwner->GetInventory()->SetCurrentItem(itemToAttach);
			}
		}
	}
	//update player hud weapon info
	pPlayerOwner->PlayerOnEvent(PLAYER_EVENT_HUD_UPDATE_WEAPON_INFO);
}

void CAttachmentComponent::ResetAttachments()
{
	if (CInventoryComponent *pInventory = m_pEntity->GetComponent<CInventoryComponent>())
	{
		for (int i = 0; i < AMMOCLIP_CAPACITY; i++)
		{
			if (i < WEAPON_CAPACITY)
			{
				if (pInventory->GetActiveSlot() != i)
					AttachToBack(pInventory->GetItemId(i), i);
				else
					AttachToHand(pInventory->GetItemId(i));
			}
			AttachToBelt(pInventory->GetAmmoClipItemId(i), i);
		}
		if (pInventory->GetActiveSlot() < 0)
		{
			AttachToHand(0);
			//AttachToHand(pInventory->GetForeignItemId());
		}
	}
}

void CAttachmentComponent::SwapBackAttachments(int slot_first, int slot_second)
{
	//EntityId first = att_back[slot_first];
	//EntityId second = att_back[slot_second];

	//AttachToBack(0, slot_first);
	//AttachToBack(0, slot_second);

	//if (first > 0)
	//{
	//	AttachToBack(first, slot_second);
	//}
	//if (second > 0)
	//{
	//	AttachToBack(second, slot_first);
	//}
}
