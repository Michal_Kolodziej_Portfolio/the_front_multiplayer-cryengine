#include "StdAfx.h"
#include "EngineComponent.h"
#include "CryNetwork/Rmi.h"

static void RegisterEngine(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CEngineComponent));
		// Functions
		{
		}
	}
}
CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterEngine)

void CEngineComponent::Initialize()
{
	pVehicle = m_pEntity->GetComponent<SVehicleComponent>();
	//Initialize Gears ratios
	sEngineProperties.GEAR_RATIO[0] = GetProperties()->fBackwardGear9;
	sEngineProperties.GEAR_RATIO[1] = GetProperties()->fBackwardGear8;
	sEngineProperties.GEAR_RATIO[2] = GetProperties()->fBackwardGear7;
	sEngineProperties.GEAR_RATIO[3] = GetProperties()->fBackwardGear6;
	sEngineProperties.GEAR_RATIO[4] = GetProperties()->fBackwardGear5;
	sEngineProperties.GEAR_RATIO[5] = GetProperties()->fBackwardGear4;
	sEngineProperties.GEAR_RATIO[6] = GetProperties()->fBackwardGear3;
	sEngineProperties.GEAR_RATIO[7] = GetProperties()->fBackwardGear2;
	sEngineProperties.GEAR_RATIO[8] = GetProperties()->fBackwardGear1;
	sEngineProperties.GEAR_RATIO[9] = GetProperties()->fBackwardGear0;
	sEngineProperties.GEAR_RATIO[10] = GetProperties()->fForwardGear0;
	sEngineProperties.GEAR_RATIO[11] = GetProperties()->fForwardGear1;
	sEngineProperties.GEAR_RATIO[12] = GetProperties()->fForwardGear2;
	sEngineProperties.GEAR_RATIO[13] = GetProperties()->fForwardGear3;
	sEngineProperties.GEAR_RATIO[14] = GetProperties()->fForwardGear4;
	sEngineProperties.GEAR_RATIO[15] = GetProperties()->fForwardGear5;
	sEngineProperties.GEAR_RATIO[16] = GetProperties()->fForwardGear6;
	sEngineProperties.GEAR_RATIO[17] = GetProperties()->fForwardGear7;
	sEngineProperties.GEAR_RATIO[18] = GetProperties()->fForwardGear8;
	sEngineProperties.GEAR_RATIO[19] = GetProperties()->fForwardGear9;
	sEngineProperties.rpmThou[0] = GetProperties()->fThousandRpm1;
	sEngineProperties.rpmThou[1] = GetProperties()->fThousandRpm2;
	sEngineProperties.rpmThou[2] = GetProperties()->fThousandRpm3;
	sEngineProperties.rpmThou[3] = GetProperties()->fThousandRpm4;
	sEngineProperties.rpmThou[4] = GetProperties()->fThousandRpm5;
	sEngineProperties.rpmThou[5] = GetProperties()->fThousandRpm6;
	sEngineProperties.rpmThou[6] = GetProperties()->fThousandRpm7;
	sEngineProperties.rpmThou[7] = GetProperties()->fThousandRpm8;
	sEngineProperties.rpmThou[8] = GetProperties()->fThousandRpm9;
	sEngineProperties.rpmThou[9] = GetProperties()->fThousandRpm10;
	for (int i = 0; i < 20; i++)
	{
		//gear ratio calculations from proportions
		sEngineProperties.GEAR_RATIO[i] /= 2.f;
		sEngineProperties.GEAR_RATIO[i] /= GetProperties()->iMaxRpm;
	}
	for (int i = 0; i < 10; i++)
	{
		sEngineProperties.rpmPace[i] = 1000 * (i + 1);
	}

	sEngineStartupProperties = sEngineProperties;
}

uint64 CEngineComponent::GetEventMask() const
{
	return BIT64(ENTITY_EVENT_TIMER) | BIT64(ENTITY_EVENT_UPDATE);
}

void CEngineComponent::ProcessEvent(SEntityEvent & event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_TIMER:
	{
		if (event.nParam[0] == 33)
		{
			if (sEngineProperties.bIsStarting)
				Startup();
		}
	}
	break;
	case ENTITY_EVENT_UPDATE:
	{
		if (gEnv->bServer)
		{
			if (IsWorking())
			{
				//horsepower calculation for rpm rise usage
				float horsePower = roundf(((GetCurrentHorsePower() * 0.001f) * 12.f));
				int rpmRiseRatio = int(horsePower);
				GetProperties()->iRpmRiseTempo = rpmRiseRatio;

				if (sEngineProperties.bReleaseClutch)
				{
					if (sEngineProperties.iClutchSeparation > 0.01f)
						sEngineProperties.iClutchSeparation -= 0.01f;
				}
				if (sEngineProperties.bFreeRpm)
				{
					if (GetProperties()->iCurrentRpm < GetProperties()->iIdleRpm)
					{
						GetProperties()->iCurrentRpm = GetProperties()->iIdleRpm;
					}
					else if (GetProperties()->iCurrentRpm > GetProperties()->iIdleRpm)
					{
						GetProperties()->iCurrentRpm -= GetProperties()->iRpmRiseTempo + 10;
					}
				}
			}
			else
			{
				if (GetProperties()->iCurrentRpm > 0)
					GetProperties()->iCurrentRpm -= GetProperties()->iRpmRiseTempo + 10;
				else if (GetProperties()->iCurrentRpm < 0)
					GetProperties()->iCurrentRpm = 0;
			}
			NetMarkAspectsDirty(kEngineAspect);
		}
		if (sEngineProperties.bIsStarting)
		{
			//starting engine sounds and effects
		}
		if (sEngineProperties.iClutchSeparation < 0.01f)
			sEngineProperties.iClutchSeparation = 0.01f;

		if (sEngineProperties.iClutchSeparation == 0.01f)
			sEngineProperties.bReleaseClutch = false;
	}
	break;
	}
}

void CEngineComponent::ReflectType(Schematyc::CTypeDesc<CEngineComponent>& desc)
{
	desc.SetGUID("{38B1A20A-3620-49F7-9BE9-EAC8C0B95910}"_cry_guid);
	desc.SetEditorCategory("Vehicles");
	desc.SetLabel("Engine Component");
	desc.SetDescription("Engine component");
	desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach });
	desc.AddMember(&CEngineComponent::sEngineProperties, 'engp', "EngineProperties", "Engine settings", "Settings of this engine", SEngineProperties());
}

bool CEngineComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == kEngineAspect)
	{
		ser.BeginGroup("EngineSynchronization");

		ser.Value("currentRpm", sEngineProperties.iCurrentRpm);
		ser.Value("currentGear", sEngineProperties.iActiveGear);
		ser.Value("isWorking", sEngineProperties.bIsWorking, 'bool');

		ser.EndGroup();
	}
	return true;
}

void CEngineComponent::Start()
{
	if (!sEngineProperties.bIsWorking)
	{
		sEngineProperties.bIsStarting = true;
		m_pEntity->SetTimer(33, GetProperties()->iStartTime*1000);
	}
}

void CEngineComponent::AbortStarting()
{
	sEngineProperties.bIsStarting = false;
	m_pEntity->SetTimer(33, 0);
}

void CEngineComponent::Stop()
{
	sEngineProperties.bIsWorking = false;
	sEngineProperties.bIsStarting = false;
	NetMarkAspectsDirty(kEngineAspect);
}

void CEngineComponent::StartNow()
{
	sEngineProperties.bIsStarting = false;
	sEngineProperties.bIsWorking = true;
	GetProperties()->iCurrentRpm = GetProperties()->iIdleRpm;
}

void CEngineComponent::OpenThrottle()
{
	if (IsWorking())
	{
		sEngineProperties.bFreeRpm = false;
		//locking max rpms
		if (GetProperties()->iCurrentRpm >= GetProperties()->iMaxRpm)
		{
			GetProperties()->iCurrentRpm = GetProperties()->iMaxRpm;
			GetProperties()->iCurrentRpm -= GetProperties()->iRpmRiseTempo + 2;
		}
		//GEARS
		if (GetProperties()->iActiveGear == NEUTRAL)
			GetProperties()->iCurrentRpm += GetProperties()->iRpmRiseTempo;
		else
		{
			if (pVehicle)
			{
				if (sEngineProperties.iClutchSeparation != 0.01f)
					sEngineProperties.bReleaseClutch = true;

				const int iActiveGear = (GetCurrentGear() < 0) ? GetCurrentGear() + 10 : GetCurrentGear() + 9;
				const float fVehicleCurrentSpeed = pVehicle->GetPhysicsSpeed() * 3.f;
				const float fActiveGearRatio = sEngineProperties.GEAR_RATIO[iActiveGear];

				const float fRpmBySpeed = roundf(fVehicleCurrentSpeed / fActiveGearRatio);
				const int iRpmBySpeedInt = int(fRpmBySpeed);
				
				if (!sEngineProperties.bReleaseClutch || iRpmBySpeedInt >= GetProperties()->iCurrentRpm)
				{
					GetProperties()->iCurrentRpm = iRpmBySpeedInt + 1;
					if (iRpmBySpeedInt >= GetProperties()->iCurrentRpm)
					{
						sEngineProperties.bReleaseClutch = true;
						sEngineProperties.iClutchSeparation = 1.f;
					}
				}
				else if(sEngineProperties.bReleaseClutch)
				{
					float fIdleRpm = float(GetProperties()->iIdleRpm);
					float fCurRpm = fIdleRpm * sEngineProperties.iClutchSeparation;
					GetProperties()->iCurrentRpm = int(fCurRpm);
				}

				if (GetProperties()->iCurrentRpm < GetProperties()->iMaxRpm)
				{
					pVehicle->Accelerate();
				}
			}
		}
	}
}

void CEngineComponent::CloseThrottle()
{
	if (IsWorking())
	{
		sEngineProperties.bFreeRpm = true;
		sEngineProperties.bReleaseClutch = false;
		sEngineProperties.iClutchSeparation = 1.f;
	}
}

void CEngineComponent::SetGear(int gear)
{
	GetProperties()->iActiveGear = gear;
}

void CEngineComponent::NextGear()
{
	if (GetCurrentGear() == GetLastGear())
		return;

	SetGear(GetCurrentGear() + 1);
}

void CEngineComponent::PreviousGear()
{
	if (GetCurrentGear() == GetFirstGear())
		return;

	SetGear(GetCurrentGear() - 1);
}

float CEngineComponent::GetCurrentHorsePower()
{
	int top = -1;
	int bottom = -1;
	for (int i = 0; i < 10; i++)
	{
		if (top == -1)
		{
			if (GetProperties()->iCurrentRpm < sEngineProperties.rpmPace[i])
			{
				top = i;
			}
		}
		if(bottom == -1)
		{
			if (GetProperties()->iCurrentRpm > sEngineProperties.rpmPace[i])
			{
				bottom = i;
			}
		}
		if (GetProperties()->iCurrentRpm == sEngineProperties.rpmPace[i])
		{
			return sEngineProperties.rpmThou[i];
		}
	}

	if (top != -1 && bottom != -1)
	{
		int rpmDiff = sEngineProperties.rpmPace[top] - sEngineProperties.rpmPace[bottom];
		float hpTopRatio = sEngineProperties.rpmPace[top] / sEngineProperties.rpmThou[top];
		float hpBottomRatio = sEngineProperties.rpmPace[bottom] / sEngineProperties.rpmThou[bottom];
		float hpRatioDiff = hpTopRatio - hpBottomRatio;
		(hpRatioDiff < 0.f) ? hpRatioDiff *= -1.f : hpRatioDiff;
		int bottomRpmDiff = GetProperties()->iCurrentRpm - sEngineProperties.rpmPace[bottom];
		float searchedRatio = (bottomRpmDiff * hpRatioDiff) / rpmDiff;
		float neededRatio = hpBottomRatio + searchedRatio;
		float searchedHP = GetProperties()->iCurrentRpm / neededRatio;
		return searchedHP;
	}
	else if (top != -1 && bottom == -1)
	{
		float hpTopRatio = sEngineProperties.rpmPace[top] / sEngineProperties.rpmThou[top];
		float searchedHP = GetProperties()->iCurrentRpm / hpTopRatio;
		return searchedHP;
	}
	else if (top == -1 && bottom != -1)
	{
		float hpBottomRatio = sEngineProperties.rpmPace[bottom] / sEngineProperties.rpmThou[bottom];
		float searchedHP = GetProperties()->iCurrentRpm / hpBottomRatio;
		return searchedHP;
	}
	return -1.f;
}

float CEngineComponent::GetHorsePowerAtRpm(int rpm)
{
	int top = -1;
	int bottom = -1;
	for (int i = 0; i < 10; i++)
	{
		if (top == -1)
		{
			if (rpm < sEngineProperties.rpmPace[i])
			{
				top = i;
			}
		}
		if (bottom == -1)
		{
			if (rpm > sEngineProperties.rpmPace[i])
			{
				bottom = i;
			}
		}
		if (rpm == sEngineProperties.rpmPace[i])
		{
			return sEngineProperties.rpmThou[i];
		}
	}

	if (top != -1 && bottom != -1)
	{
		int rpmDiff = sEngineProperties.rpmPace[top] - sEngineProperties.rpmPace[bottom];
		float hpTopRatio = sEngineProperties.rpmPace[top] / sEngineProperties.rpmThou[top];
		float hpBottomRatio = sEngineProperties.rpmPace[bottom] / sEngineProperties.rpmThou[bottom];
		float hpRatioDiff = hpTopRatio - hpBottomRatio;
		(hpRatioDiff < 0.f) ? hpRatioDiff *= -1.f : hpRatioDiff;
		int bottomRpmDiff = rpm - sEngineProperties.rpmPace[bottom];
		float searchedRatio = (bottomRpmDiff * hpRatioDiff) / rpmDiff;
		float neededRatio = hpBottomRatio + searchedRatio;
		float searchedHP = rpm / neededRatio;
		return searchedHP;
	}
	else if (top != -1 && bottom == -1)
	{
		float hpTopRatio = sEngineProperties.rpmPace[top] / sEngineProperties.rpmThou[top];
		float searchedHP = rpm / hpTopRatio;
		return searchedHP;
	}
	else if (top == -1 && bottom != -1)
	{
		float hpBottomRatio = sEngineProperties.rpmPace[bottom] / sEngineProperties.rpmThou[bottom];
		float searchedHP = rpm / hpBottomRatio;
		return searchedHP;
	}
	return -1.f;
}

void CEngineComponent::ServerReset()
{
	sEngineProperties = sEngineStartupProperties;

	NetMarkAspectsDirty(kEngineAspect);
}

void CEngineComponent::Startup()
{
	sEngineProperties.bIsStarting = false;
	sEngineProperties.bIsWorking = true;
	NetMarkAspectsDirty(kEngineAspect);
}
