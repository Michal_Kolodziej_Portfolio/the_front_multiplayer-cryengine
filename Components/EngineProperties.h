/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : This will hold engine properties

--------------------------------------------------------------------------------- */

#pragma once
#include <CrySchematyc/ResourceTypes.h>
#include <CrySchematyc/Reflection/TypeDesc.h>
#include "CrySchematyc/MathTypes.h"
#include "FuryResources.h"

enum Gearbox_geartype
{
	GEAR_BACK_10 = -10,
	GEAR_BACK_9,
	GEAR_BACK_8,
	GEAR_BACK_7,
	GEAR_BACK_6,
	GEAR_BACK_5,
	GEAR_BACK_4,
	GEAR_BACK_3,
	GEAR_BACK_2,
	GEAR_BACK_1,
	NEUTRAL,
	GEAR_NEUTRAL = NEUTRAL,
	GEAR_FOR_1,
	GEAR_FOR_2,
	GEAR_FOR_3,
	GEAR_FOR_4,
	GEAR_FOR_5,
	GEAR_FOR_6,
	GEAR_FOR_7,
	GEAR_FOR_8,
	GEAR_FOR_9,
	GEAR_FOR_10,
};

//PROPERTIES
struct SEngineProperties
{
	inline bool operator==(const SEngineProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SEngineProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	//Non-editor properties
	int iCurrentRpm = 0;
	int iRpmRiseTempo = 0;
	int iActiveGear = 0;
	//Editor properties
	int iStartTime = 0;
	int iIdleRpm = 0;
	int iMaxRpm = 0;
	Schematyc::Range<0, 10, 0, 100, int> iForwardGears = 0;
	Schematyc::Range<0, 10, 0, 100, int> iBackwardGears = 0;
	//forward gears ratios
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear0 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear1 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear2 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear3 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear4 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear5 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear6 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear7 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear8 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fForwardGear9 = 0.f;
	//backward gears ratios
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear0 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear1 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear2 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear3 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear4 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear5 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear6 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear7 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear8 = 0.f;
	Schematyc::Range<0, 700, 0, 700, float> fBackwardGear9 = 0.f;
	//power every thousand rpms
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm1 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm2 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm3 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm4 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm5 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm6 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm7 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm8 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm9 = 0.f;
	Schematyc::Range<0, 10000, 0, 10000, float> fThousandRpm10 = 0.f;
	//non editor
	float iClutchSeparation = 1.f;
	bool bReleaseClutch = false;
	int rpmPace[10];
	float rpmThou[10];
	bool bFreeRpm = false;
	float GEAR_RATIO[20];
	bool bIsWorking = false;
	bool bIsStarting = false;
};
static void ReflectType(Schematyc::CTypeDesc<SEngineProperties>& desc)
{
	desc.SetGUID("{50C75F4C-C236-47EA-8515-58F9AABC014B}"_cry_guid);
	desc.SetLabel("Engine general properties");
	desc.SetDescription("General engine properties");
	desc.AddMember(&SEngineProperties::iStartTime, 'stat', "StartTime", "Engine start time in seconds", "How many seconds engine needs to startup", 0);
	desc.AddMember(&SEngineProperties::iIdleRpm, 'idlr', "IdleRpm", "RPM while idling", "Idle rpm speed", 0);
	desc.AddMember(&SEngineProperties::iMaxRpm, 'maxr', "MaxRpm", "Max RPM", "Max rpms of this engine", 0);
	desc.AddMember(&SEngineProperties::iForwardGears, 'forg', "ForwardGears", "Number of forward gears", "Number of forward gears", 0);
	desc.AddMember(&SEngineProperties::iBackwardGears, 'bacg', "BackwardGears", "Number of backward gears", "Number of backward gears", 0);
	//froward gears ratios
	desc.AddMember(&SEngineProperties::fForwardGear0, 'for0', "ForwardGear0", "Forward gear 1 ratio", "Forward gear 1 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear1, 'for1', "ForwardGear1", "Forward gear 2 ratio", "Forward gear 2 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear2, 'for2', "ForwardGear2", "Forward gear 3 ratio", "Forward gear 3 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear3, 'for3', "ForwardGear3", "Forward gear 4 ratio", "Forward gear 4 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear4, 'for4', "ForwardGear4", "Forward gear 5 ratio", "Forward gear 5 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear5, 'for5', "ForwardGear5", "Forward gear 6 ratio", "Forward gear 6 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear6, 'for6', "ForwardGear6", "Forward gear 7 ratio", "Forward gear 7 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear7, 'for7', "ForwardGear7", "Forward gear 8 ratio", "Forward gear 8 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear8, 'for8', "ForwardGear8", "Forward gear 9 ratio", "Forward gear 9 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fForwardGear9, 'for9', "ForwardGear9", "Forward gear 10 ratio", "Forward gear 10 ratio", 0.f);
	//backward gears ratios
	desc.AddMember(&SEngineProperties::fBackwardGear0, 'bac0', "BackwardGear0", "Backward gear 1 ratio", "Backward gear 1 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear1, 'bac1', "BackwardGear1", "Backward gear 2 ratio", "Backward gear 2 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear2, 'bac2', "BackwardGear2", "Backward gear 3 ratio", "Backward gear 3 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear3, 'bac3', "BackwardGear3", "Backward gear 4 ratio", "Backward gear 4 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear4, 'bac4', "BackwardGear4", "Backward gear 5 ratio", "Backward gear 5 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear5, 'bac5', "BackwardGear5", "Backward gear 6 ratio", "Backward gear 6 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear6, 'bac6', "BackwardGear6", "Backward gear 7 ratio", "Backward gear 7 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear7, 'bac7', "BackwardGear7", "Backward gear 8 ratio", "Backward gear 8 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear8, 'bac8', "BackwardGear8", "Backward gear 9 ratio", "Backward gear 9 ratio", 0.f);
	desc.AddMember(&SEngineProperties::fBackwardGear9, 'bac9', "BackwardGear9", "Backward gear 10 ratio", "Backward gear 10 ratio", 0.f);
	//power every thousand rpms
	desc.AddMember(&SEngineProperties::fThousandRpm1, 'tho1', "Thousand1", "1000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm2, 'tho2', "Thousand2", "2000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm3, 'tho3', "Thousand3", "3000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm4, 'tho4', "Thousand4", "4000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm5, 'tho5', "Thousand5", "5000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm6, 'tho6', "Thousand6", "6000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm7, 'tho7', "Thousand7", "7000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm8, 'tho8', "Thousand8", "8000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm9, 'tho9', "Thousand9", "9000 RPM power", "power on every 1000 rpms", 0.f);
	desc.AddMember(&SEngineProperties::fThousandRpm10, 'th10', "Thousand10", "10000 RPM power", "power on every 1000 rpms", 0.f);
}