/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : This will store all vehicles basic properties for editor and non-editor

--------------------------------------------------------------------------------- */

#pragma once
#include <CrySchematyc/ResourceTypes.h>
#include <CrySchematyc/Reflection/TypeDesc.h>
#include "CrySchematyc/MathTypes.h"
#include "FuryResources.h"
#include "Components/AdvancedAnimationComponent.h"

//Animation properties
struct SVehicleAnimationProperties
{
	inline bool operator==(const SVehicleAnimationProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SVehicleAnimationProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	Cry::DefaultComponents::EMeshType rType;
	Cry::DefaultComponents::SRenderParameters pRenderParams;
	Schematyc::MannequinAnimationDatabasePath sDatabaseFile;
	SAdvancedAnimationComponent::SDefaultScopeSettings sDefaultScopeSettings;
	Cry::DefaultComponents::SPhysicsParameters pPhysicsParams;
};
static void ReflectType(Schematyc::CTypeDesc<SVehicleAnimationProperties>& desc)
{
	desc.SetGUID("{6ABA672C-70CC-4F36-83EA-402C0859A6D8}"_cry_guid);
	desc.AddMember(&SVehicleAnimationProperties::rType, 'type', "Type", "Type", "Determines the behavior of the static mesh", Cry::DefaultComponents::EMeshType::RenderAndCollider);
	desc.AddMember(&SVehicleAnimationProperties::sDatabaseFile, 'dbpa', "DatabasePath", "Animation Database", "Path to the Mannequin .adb file", "");
	desc.AddMember(&SVehicleAnimationProperties::sDefaultScopeSettings, 'defs', "DefaultScope", "Default Scope Context Name", "Default Mannequin scope settings", SAdvancedAnimationComponent::SDefaultScopeSettings());
}
//PROPERTIES
struct SVehicleProperties
{
	inline bool operator==(const SVehicleProperties& rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
	inline bool operator!=(const SVehicleProperties& rhs) const { return 0 != memcmp(this, &rhs, sizeof(rhs)); }

	EntityId cabineSlot[10] = { 0 };
	EntityId cabineSlotComp[10] = { 0 };
	Schematyc::Range<1, 10, 0, 100, int> iCabineSlotsCount = 1;
	float fMaxSpeed = 0.f;
	float fCurSpeed = 0.f;
	//non editor
	int iCurrentRpm = 0;
	float physicsSpeed = 0.f;


	SVehicleAnimationProperties sAnimationProperties;
};
static void ReflectType(Schematyc::CTypeDesc<SVehicleProperties>& desc)
{
	desc.SetGUID("{1D4918EF-9161-4D74-9958-8F20A158A26E}"_cry_guid);
	desc.SetLabel("Vehicle general properties");
	desc.SetDescription("General vehicle properties");
	desc.AddMember(&SVehicleProperties::sAnimationProperties, 'sani', "AnimationProperties", "Animation settings", "Setting of vehicle animation system", SVehicleAnimationProperties());
	desc.AddMember(&SVehicleProperties::iCabineSlotsCount, 'cab', "CabineSlots", "Max space in vehicle", "Space in vehicle", 0);
	desc.AddMember(&SVehicleProperties::fMaxSpeed, 'fmax', "MaxSpeed", "Maximum speed", "Max speed of this vehicle", 0.f);
}