/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Ammo clip is attached to weapon when player reloads it and dropped when it's empty

--------------------------------------------------------------------------------- */

#pragma once
#include "AmmoClipProperties.h"

class CWeaponComponent;

class CAmmoClipComponent final : public SItemComponent
{
public:
	CAmmoClipComponent() = default;
	virtual ~CAmmoClipComponent() {}
	static void ReflectType(Schematyc::CTypeDesc<CAmmoClipComponent>& desc);
	virtual void InitClass();
	virtual void ClassProcessEvent(SEntityEvent& event) override;
	//NETWORKING
	virtual bool ClassNetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	int ServerAddRemoveAmmo(int ammo);
	//~NETWORKING
	//MAGAZINE USAGE
	virtual int GetCurrentAmmo() { return sAmmoClipProperties.iCurrentAmmo; }
	virtual void SetCurrentAmmo(int newAmmo) { sAmmoClipProperties.iCurrentAmmo = newAmmo; }
	virtual int GetMaxAmmo() { return sAmmoClipProperties.iMaxAmmo; }
	virtual int DecreaseAmmo() { sAmmoClipProperties.iCurrentAmmo -= 1; ServerUpdateStaticData(); return sAmmoClipProperties.iCurrentAmmo; }
	virtual bool IsEmpty() { return sAmmoClipProperties.iCurrentAmmo < 1; }
	virtual bool IsFull() { return sAmmoClipProperties.iCurrentAmmo == sAmmoClipProperties.iMaxAmmo; }
	virtual string GetBulletName() { return sAmmoClipProperties.sBulletName.value.c_str(); }
	virtual bool IsBulletCorrect(string bulletName) { return GetBulletName() == bulletName; }
	virtual string GetWeaponTypeName()
	{
		//every ammo clip has its own type of weapon that can be attached to
		//this method gets weapon name that this ammo clip can be attached to
		//this method shows short name of the weapon and makes it upper case, this method is used only for visual purposes (then you target this ammo clip it will show which weapon it can be attached to)
		string sLongName = sAmmoClipProperties.sWeaponName.value.c_str();
		sLongName.erase(0, sLongName.find_last_of(':') + 1);
		string sFirst;
		sFirst.SetAt(0, sLongName[0]);
		sFirst.MakeUpper();
		sLongName.erase(0, 1);
		string sWeaponName = sFirst + sLongName;
		return sWeaponName;
	}
	virtual void DropFromWeapon(CWeaponComponent *pWeaponParent);
	virtual void FillUp();
	virtual void ServerResetClass() override;
	//VARIABLES
	SAmmoClipProperties sAmmoClipProperties;
	SAmmoClipProperties sAmmoClipPrevProperties;
	SAmmoClipProperties sAmmoClipStartupProperties;
};