#include "StdAfx.h"
#include "FlashUI/FlashUI.h"
#include "PlayerUIComponent.h"
#include "UISystem/ExitDialogEvents.h"
#include "UISystem/AmmoPackDialogEvents.h"
#include "UISystem/InventoryEvents.h"

CExitDialogEvents exitDialogEvents;
CAmmoPackDialogEvents ammoPackDialogEvents;
CInventoryEvents inventoryEvents;

void CPlayerUIComponent::Initialize()
{
	pMan = gEnv->pFlashUI->GetUIActionManager();
	//Initialize result menu
	pResultMenu = gEnv->pFlashUI->GetUIElement("ResultMenu");
	pResultShow = gEnv->pFlashUI->GetUIAction("resultmenushow");
	pResultHide = gEnv->pFlashUI->GetUIAction("resultmenuhide");
	pMan->StartAction(pResultHide, "ResultMenu");
	//Initialize exit dialog
	pExitDialog = gEnv->pFlashUI->GetUIElement("ExitDialog");
	pExitShow = gEnv->pFlashUI->GetUIAction("exitdialogshow");
	pExitHide = gEnv->pFlashUI->GetUIAction("exitdialoghide");
	pMan->StartAction(pExitHide, "ExitDialog");
	exitDialogEvents.player = m_pEntity->GetComponent<CPlayerComponent>();
	pExitDialog->AddEventListener(&exitDialogEvents, "ExitDialog");
	//Initialize killer messages element
	pNotifications = gEnv->pFlashUI->GetUIElement("Notifications");
	pNotificationsShow = gEnv->pFlashUI->GetUIAction("notificationsshow");
	pNotificationsHide = gEnv->pFlashUI->GetUIAction("notificationshide");
	pMan->StartAction(pNotificationsShow, "Notifications");
	//Initialize ammo pack dialog
	pAmmoPackDialog = gEnv->pFlashUI->GetUIElement("AmmoPackDialog");
	pAmmoPackDialogShow = gEnv->pFlashUI->GetUIAction("ammopackdialogshow");
	pAmmoPackDialogHide = gEnv->pFlashUI->GetUIAction("ammopackdialoghide");
	ammoPackDialogEvents.player = m_pEntity->GetComponent<CPlayerComponent>();
	pMan->StartAction(pAmmoPackDialogHide, "AmmoPackDialog");
	//Initialize round loading dialog
	pRoundLoadingDialog = gEnv->pFlashUI->GetUIElement("RoundLoadingDialog");
	pRoundLoadingDialogShow = gEnv->pFlashUI->GetUIAction("roundloadingdialogshow");
	pRoundLoadingDialogHide = gEnv->pFlashUI->GetUIAction("roundloadingdialoghide");
	pMan->StartAction(pRoundLoadingDialogHide, "RoundLoadingDialog");
	//Initialize inventory 
	pInventoryMenu = gEnv->pFlashUI->GetUIElement("Inventory");
	pInventoryShow = gEnv->pFlashUI->GetUIAction("inventoryshow");
	pInventoryHide = gEnv->pFlashUI->GetUIAction("inventoryhide");
	inventoryEvents.pPlayer = m_pEntity->GetComponent<CPlayerComponent>();
	pInventoryMenu->AddEventListener(&inventoryEvents, "Inventory");
	pMan->StartAction(pInventoryShow, "Inventory");
}

void CPlayerUIComponent::ShowSummary(bool show)
{
	if (show)
	{
		pMan->StartAction(pResultShow, "ResultMenu");
	}
	else
	{
		pMan->StartAction(pResultHide, "ResultMenu");
	}
}

void CPlayerUIComponent::ShowExitDialog(bool show)
{
	if (show)
	{
		pMan->StartAction(pExitShow, "ExitDialog");
		pExitDialog->CallFunction("Init");
		pExitDialog->AddEventListener(&exitDialogEvents, "ExitDialog");
	}
	else
	{
		pMan->StartAction(pExitHide, "ExitDialog");
		pExitDialog->RemoveEventListener(&exitDialogEvents);
	}
}

void CPlayerUIComponent::ShowNotifications(bool show)
{
	if (show)
	{
		pMan->StartAction(pNotificationsShow, "Notifications");
	}
	else
	{
		pMan->StartAction(pNotificationsHide, "Notifications");
	}
}

void CPlayerUIComponent::KillMessage(SUIArguments args)
{
	pNotifications->CallFunction("AddKillMessage", args);
}

void CPlayerUIComponent::JoinMessage(SUIArguments args)
{
	pNotifications->CallFunction("AddJoinMessage", args);
}

void CPlayerUIComponent::SummarySetPlayer(SUIArguments args)
{
	pResultMenu->CallFunction("SetPlayer", args);
}

void CPlayerUIComponent::SummaryClear()
{
	pResultMenu->CallFunction("Clear");
}

void CPlayerUIComponent::ShowAmmoPackDialog(bool show)
{
	if (show)
	{
		bIsAmmoPackDialogOpened = true;
		pMan->StartAction(pAmmoPackDialogShow, "AmmoPackDialog");
		pAmmoPackDialog->AddEventListener(&ammoPackDialogEvents, "AmmoPackDialog");
	}
	else
	{
		bIsAmmoPackDialogOpened = false;
		pMan->StartAction(pAmmoPackDialogHide, "AmmoPackDialog");
		pAmmoPackDialog->RemoveEventListener(&ammoPackDialogEvents);
	}
}

void CPlayerUIComponent::SetAmmoPackDialog(SUIArguments args)
{
	pAmmoPackDialog->CallFunction("Set", args);
}

void CPlayerUIComponent::ShowRoundLoadingDialog(bool show)
{
	if (show)
	{
		if (!bIsRoundLoading)
		{
			bIsRoundLoading = true;
			pMan->StartAction(pRoundLoadingDialogShow, "RoundLoadingDialog");
		}
	}
	else
	{
		pMan->StartAction(pRoundLoadingDialogHide, "RoundLoadingDialog");
		bIsRoundLoading = false;
	}
}

void CPlayerUIComponent::ShowInventory()
{
	//firstly we will show inventory
	SUIArguments args;
	args.AddArgument<bool>(!bIsInventoryOpened);
	pInventoryMenu->CallFunction("ShowInventory", args);
	bIsInventoryOpened = !bIsInventoryOpened;
	//player will be frozen if inventory is opened
	m_pEntity->GetComponent<CPlayerComponent>()->bFreezePlayer = bIsInventoryOpened;

	if (bIsInventoryOpened)
	{
		//and if inventory is opened, we will show what bullets we have in it
		if (CPlayerComponent *player = m_pEntity->GetComponent<CPlayerComponent>())
		{
			CInventoryComponent *pInventory = player->GetInventory();
			//when we open inventory, we want to deactivate and holster any unholstered item
			pInventory->ClientSelectSlot(player->GetInventory()->GetActiveSlot());

			int bulletListIndex = -1;
			for (int i = 0; i < BULLET_TYPE_CAPACITY; i++)
			{
				if (!pInventory->GetBulletGroupName(i).empty())
				{
					bulletListIndex++;
					SetBullet(pInventory->SchematycEntityToShortName(pInventory->GetBulletGroupName(i)), bulletListIndex, pInventory->GetBulletGroupAmmount(i));
				}
			}
		}
	}
}

void CPlayerUIComponent::AddItem(string name, int slot, int type, int bullets)
{
	SUIArguments args;
	args.AddArgument<string>(name);
	args.AddArgument<int>(slot);
	args.AddArgument<int>(type);
	args.AddArgument<int>(bullets);
	pInventoryMenu->CallFunction("AddItem", args);
}

void CPlayerUIComponent::RemoveActiveItem()
{
	pInventoryMenu->CallFunction("RemoveActiveItem");
}

void CPlayerUIComponent::RemoveItem(int slot, int type)
{
	SUIArguments args;
	args.AddArgument<int>(slot);
	args.AddArgument<int>(type);
	pInventoryMenu->CallFunction("RemoveItem", args);
}

void CPlayerUIComponent::Activate(int slot)
{
	SUIArguments args;
	args.AddArgument<int>(slot);
	pInventoryMenu->CallFunction("Activate", args);
}

void CPlayerUIComponent::Deactivate()
{
	pInventoryMenu->CallFunction("Deactivate");
}

void CPlayerUIComponent::SetBullet(string name, int line, int count)
{
	SUIArguments args;
	args.AddArgument<string>(name);
	args.AddArgument<int>(line);
	args.AddArgument<int>(count);
	pInventoryMenu->CallFunction("SetBullet", args);
}

void CPlayerUIComponent::ClearInventory()
{
	pInventoryMenu->CallFunction("clear");
}
