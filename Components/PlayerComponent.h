/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Main player declaration. Definitions are in separated files for server and client

--------------------------------------------------------------------------------- */

#pragma once

#include <array>
#include <numeric>

#include "NetPlayer.h"
#include <CryMath/Cry_Camera.h>

#include <ICryMannequin.h>
#include <DefaultComponents/Input/InputComponent.h>
#include "PlayerProperties.h"
#include "InventoryComponent.h"
#include "AttachmentComponent.h"

struct SItemComponent;
class CBulletComponent;
class CGamePlugin;
class CHudComponent;
class CPlayerUIComponent;
class CAmmoPackComponent;
class CAccountComponent;
struct SVehicleComponent;
class CGameTeam;

enum EPlayerFlag
{
	PLAYER_EVENT_HUD_UPDATE_WEAPON_INFO = 0
};

class CPlayerComponent final : public IEntityComponent
{
	enum Timers
	{
		Timer_First = 0,
		Timer_Respawn,
		Timer_Last
	};
	enum class EInputFlagType
	{
		Hold = 0,
		Toggle
	};

	typedef uint8 TInputFlags;

	enum class EInputFlag
		: TInputFlags
	{
		MoveLeft = 1 << 0,
		MoveRight = 1 << 1,
		MoveForward = 1 << 2,
		MoveBack = 1 << 3,
		Sprint = 1 << 4,
		Jump = 1 << 5,
	};

	template<typename T, size_t SAMPLES_COUNT>
	class MovingAverage
	{
		static_assert(SAMPLES_COUNT > 0, "SAMPLES_COUNT shall be larger than zero!");

	public:

		MovingAverage()
			: m_values()
			, m_cursor(SAMPLES_COUNT)
			, m_accumulator()
		{
		}

		MovingAverage& Push(const T& value)
		{
			if (m_cursor == SAMPLES_COUNT)
			{
				m_values.fill(value);
				m_cursor = 0;
				m_accumulator = std::accumulate(m_values.begin(), m_values.end(), T(0));
			}
			else
			{
				m_accumulator -= m_values[m_cursor];
				m_values[m_cursor] = value;
				m_accumulator += m_values[m_cursor];
				m_cursor = (m_cursor + 1) % SAMPLES_COUNT;
			}

			return *this;
		}

		T Get() const
		{
			return m_accumulator / T(SAMPLES_COUNT);
		}

		void Reset()
		{
			m_cursor = SAMPLES_COUNT;
		}

	private:

		std::array<T, SAMPLES_COUNT> m_values;
		size_t m_cursor;

		T m_accumulator;
	};
	const EEntityAspects kMovementAspect = eEA_GameClientD;
	const EEntityAspects kPlayerData = eEA_GameServerB;
	const EEntityAspects kGameplayData = eEA_GameServerA;
	const EEntityAspects kVitalData = eEA_GameServerDynamic;
public:
	CPlayerComponent() = default;
	virtual ~CPlayerComponent();
	// IEntityComponent
	virtual void Initialize() override;
	virtual void InitServerMethods();
	virtual void InitClientMethods();
	virtual uint64 GetEventMask() const override;
	virtual void ProcessEvent(SEntityEvent& event) override;
	// ~IEntityComponent
	static void ReflectType(Schematyc::CTypeDesc<CPlayerComponent>& desc);
	//NETWORK
	//Defines who is the local player
	void LocalPlayerInitialize();
	//Aspect serialization
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual NetworkAspectType GetNetSerializeAspectMask() const { return kMovementAspect | kPlayerData | kGameplayData | kVitalData; };
	void ClientUpdateMovement() { NetMarkAspectsDirty(kMovementAspect); }
	void ServerUpdatePlayerData() { NetMarkAspectsDirty(kPlayerData); }
	void ServerUpdateGameplayData() { NetMarkAspectsDirty(kGameplayData); }
	void ServerUpdateVitalData() { NetMarkAspectsDirty(kVitalData); }
	//RMI
	//Server declarations
	bool ServerAmmoPackManagement(AmmoPackManagementParams&& p, INetChannel *);
	bool ServerStopVisiting(NoParams&& p, INetChannel *);
	bool ServerUse(NoParams&& p, INetChannel *);
	bool ServerCarry(NoParams&& p, INetChannel *);
	void ServerUpdate(float frameTime);
	void ServerAddKill() { sPlayerProperties.iKills += 1; ServerUpdateGameplayData(); }
	void ServerAddDeath() { sPlayerProperties.iDeaths += 1; ServerUpdateGameplayData(); }
	void ServerSetVisitedAmmoPack(EntityId packId);
	void ServerRegeneration();
	void ServerReset();
	//Client declarations
	void ClientUpdate(float frameTime);
	//Input actions - is good to have them separately so the whole code looks cleaner
	//we are going to put here only gameplay actions, movement stuff stays in playerclient.cpp
	void InitInputActions();
	void Action_Use(int activationMode);
	void Action_Carry(int activationMode);
	void Action_SelectSlot(int activationMode, float value, int slotId);
	void Action_Shoot(int activationMode);
	void Action_Aim(int activationMode);
	void Action_Drop(int activationMode);
	void Action_ResultsWindow(int activationMode);
	void Action_ExitDialog(int activationMode);
	void Action_Inventory(int activationMode);
	//Getting properties and components
	virtual CPlayerUIComponent *GetUISystem();
	SPlayerProperties *GetProperties();
	virtual CHudComponent *GetHudComponent() { return pHudComponent; }
	virtual CAttachmentComponent *GetAttachmentComponent() { return pAttachment; }
	virtual CInventoryComponent *GetInventory() { return pInventory; }
	virtual SCharacterControllerComponent *GetController() { return pController; }
	virtual SAdvancedAnimationComponent *GetAnimations() { return pAnimations; }
	virtual SCameraComponent *GetCamera() { return pCamera; }
	void SetAccount(CAccountComponent *pAssociatedAccount);
	CAccountComponent *GetAssociatedAccount() { return pAccount; }
	Vec3 GetAttachmentCurrentPosition(string attName);
	
	//Player
	void SetVehicle(EntityId newVehicle) { sPlayerProperties.vehicle = newVehicle; sPlayerProperties.vehicleComp = newVehicle; ServerUpdatePlayerData(); }
	void IsDriver(bool is) { sPlayerProperties.bIsDriver = is; ServerUpdatePlayerData(); }
	bool IsDriver() { return sPlayerProperties.bIsDriver; }
	void Reset();
	void Revive();
	void MoveToSpawnPoint();
	void ServerOnHit(CBulletComponent *pBullet, CPlayerComponent *pAttacker, Vec3 vPushDir, Vec3 vAttackerPosOnShoot);
	void ServerOnHit(float damage);
	void Ragdolize(Vec3 pushDir);
	CGameTeam *GetTeam();
	int GetTeamIndex();
	int GetIndexInTeam();
	void SetAnimationTagID(TagID tag, bool bSet);
	string GetLogin();
	void ShowPickUpMessage(string name);
	virtual bool CanSprint();
	void IsAdmin(bool is) { bIsAdmin = is; }
	bool IsAdmin() { return bIsAdmin; }
	CAmmoPackComponent *GetVisitedAmmoPack();
	static CPlayerComponent *GetLocalPlayer() { return pLocalPlayer; }
	void PlayerOnEvent(uint8 playerFlag);
	//CAMERA EFFECTS
	Vec3 GetCameraForwardDir();
	virtual void Aim(float x, float y, float z, float nearPlane = 0.25f);
	virtual Vec3 GetCameraLocalJointPos();
	virtual void ApplyRecoil(float strength);
	virtual void IsAiming(bool is) { bIsAiming = is; }
	virtual bool IsAiming() { return bIsAiming; }
	Quat GetLookOrientation() { return m_lookOrientation; }
	TInputFlags GetInputFlags() { return m_inputFlags; }

	string GetPlayerClassName() { return sClassShortName; }
	Vec3 GetTargetPosition() { return sPlayerProperties.vTargetPosition; }

	void IsPlayerAlive(bool alive) { sPlayerProperties.bIsAlive = alive; }
	bool IsPlayerAlive() { return sPlayerProperties.bIsAlive; }

	int GetKills() { return sPlayerProperties.iKills; }
	int GetDeaths() { return sPlayerProperties.iDeaths; }
protected:
	//Constantly updated methods
	void UpdateMovementRequest(float frameTime);
	void UpdateLookDirectionRequest(float frameTime);
	void UpdateAnimation(float frameTime);
	void UpdateCamera(float frameTime);
	//Flags
	void HandleInputFlagChange(TInputFlags flags, int activationMode, EInputFlagType type = EInputFlagType::Hold);
	void CreatePlayerShortName();
protected:
	//Components
	CPlayerUIComponent *pUI = nullptr;
	CHudComponent *pHudComponent = nullptr;
	CAttachmentComponent *pAttachment = nullptr;
	CInventoryComponent *pInventory = nullptr;
	SCameraComponent* pCamera = nullptr;
	SCharacterControllerComponent* pController = nullptr;
	SAdvancedAnimationComponent* pAnimations = nullptr;
	Cry::DefaultComponents::CInputComponent* pInput = nullptr;
	//Animations, movement and camera
	FragmentID m_idleFragmentId;
	FragmentID m_walkFragmentId;
public:
	//animation tag id is public for easier access
	TagID m_rotateTagId;
	TagID m_weaponTagId;
protected:
	TInputFlags m_inputFlags;
	Vec2 m_mouseDeltaRotation;
	MovingAverage<Vec2, 10> m_mouseDeltaSmoothingFilter;
	const float m_rotationSpeed = 0.002f;
	int m_cameraJointId = -1;
	FragmentID m_activeFragmentId;
	Quat m_lookOrientation; //!< Should translate to head orientation in the future
	float m_horizontalAngularVelocity;
	MovingAverage<float, 10> m_averagedHorizontalAngularVelocity;

	//~target item
	bool bIsAiming = false;
	Vec3 fAimOffset = ZERO;
public:
	bool bIsLocal = false;
	bool bExitDialogOpened = false;
	bool bFreezePlayer = false;
	//internal gameplay properties
private:
	static CPlayerComponent *pLocalPlayer;
	SPlayerProperties sPlayerProperties;
	SPlayerProperties sPrevPlayerProperties;
	//special variables for reseting player
	SPlayerProperties sPlayerStartupProperties;
	Quat qStartRotation = IDENTITY;
	CAccountComponent *pAccount = nullptr;
	string sClassShortName;
	bool bIsAdmin = false;
};