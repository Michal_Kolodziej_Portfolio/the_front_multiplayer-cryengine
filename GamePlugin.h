#pragma once

#include <CrySystem/ICryPlugin.h>
#include <CryGame/IGameFramework.h>
#include <CryEntitySystem/IEntityClass.h>
#include <CryNetwork/INetwork.h>
#include <string>

class CPlayerComponent;
struct SServerComponent;
struct SGameCVars;

// The entry-point of the application
// An instance of CGamePlugin is automatically created when the library is loaded
// We then construct the local player entity and CPlayerComponent instance when OnClientConnectionReceived is first called.
class CGamePlugin 
	: public ICryPlugin
	, public ISystemEventListener
	, public INetworkedClientListener
{
public:
	CRYINTERFACE_SIMPLE(ICryPlugin)
	CRYGENERATE_SINGLETONCLASS_GUID(CGamePlugin, "Game_Blank", "{FC9BD884-49DE-4494-9D64-191734BBB7E3}"_cry_guid)

	CGamePlugin();
	virtual ~CGamePlugin();
	
	// ICryPlugin
	virtual const char* GetName() const override { return "GamePlugin"; }
	virtual const char* GetCategory() const override { return "Game"; }
	virtual bool Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams) override;
	virtual void OnPluginUpdate(EPluginUpdateType updateType) override;
	// ~ICryPlugin

	// ISystemEventListener
	virtual void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override;
	// ~ISystemEventListener

	// INetworkedClientListener
	// Sent to the local client on disconnect
	virtual void OnLocalClientDisconnected(EDisconnectionCause cause, const char* description) override {}

	// Sent to the server when a new client has started connecting
	// Return false to disallow the connection
	virtual bool OnClientConnectionReceived(int channelId, bool bIsReset) override;
	// Sent to the server when a new client has finished connecting and is ready for gameplay
	// Return false to disallow the connection and kick the player
	virtual bool OnClientReadyForGameplay(int channelId, bool bIsReset) override;
	// Sent to the server when a client is disconnected
	virtual void OnClientDisconnected(int channelId, EDisconnectionCause cause, const char* description, bool bKeepClient) override;
	// Sent to the server when a client is timing out (no packets for X seconds)
	// Return true to allow disconnection, otherwise false to keep client.
	virtual bool OnClientTimingOut(int channelId, EDisconnectionCause cause, const char* description) override { return true; }
	// ~INetworkedClientListener
	bool RegisterFlowNodes();
	bool UnregisterFlowNodes();
	static CGamePlugin *Get() { return plugin; }
	//Players map
	int GetAccountsCount() { return m_accounts.size(); }
	int GetPlayersCount() { return m_players.size(); }
	EntityId GetAccountEntityId(int index) { return m_accounts[index]; }
	EntityId GetPlayerEntityId(int index) { return m_players[index]; }
	void AddPlayer(int channelId, EntityId id) { m_players.emplace(std::make_pair(channelId, id)); }
	void RemovePlayer(int channelId) { auto it = m_players.find(channelId); if (it != m_players.end()) m_players.erase(it); }
	float AddDayTime(float time) { day_time += time; return day_time; }

	SServerComponent *GetServer() { return pServer; }
	SGameCVars *GetGameCVars() { return pGameCvars; }
public:
	// Map containing player components, key is the channel id received in OnClientConnectionReceived
	std::unordered_map<int, EntityId> m_accounts;
	// Players in game
	std::unordered_map<int, EntityId> m_players;
private:
	//static plugin pointer on the server side
	static CGamePlugin *plugin;
	static float day_time;
	static int last_sec;

	SServerComponent *pServer = nullptr;

	SGameCVars *pGameCvars = nullptr;
};
