/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Lobby menu event listener

--------------------------------------------------------------------------------- */

#pragma once
#include "FlashUI/FlashUI.h"
#include "Components/AccountComponent.h"


class CInventoryEvents : public IUIElementEventListener
{
public:
	virtual void OnUIEvent(IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args) override
	{
		if (!gEnv->bServer)
		{
			string eventName = event.sDisplayName;

			if (eventName == "Drag_Item")
			{
				int index;
				args.GetArg<int>(0, index);
				slot_first = index;
			}
			if (slot_first > -1)
			{
				if (eventName == "Drop_Item")
				{
					int index;
					args.GetArg<int>(0, index);
					if (index == slot_first)
						return;

					//we will remove item from said slot on server and add it to the second slot specified
					SRmi < RMI_WRAP(&CInventoryComponent::ServerSwapSlots)>::InvokeOnServer(pPlayer->GetInventory(), SwapSlotsParams{ slot_first, index });
					slot_first = -1;
				}
			}
			if (eventName == "SwapSlots")
			{
				string swapCommand;
				args.GetArg<string>(0, swapCommand);

				string left = swapCommand;
				string right = swapCommand;

				left.erase(0, 2);
				right.erase(1, 2);

				int slot_from = atoi(left);
				int slot_to = atoi(right);

				SRmi < RMI_WRAP(&CInventoryComponent::ServerSwapSlots)>::InvokeOnServer(pPlayer->GetInventory(), SwapSlotsParams{ slot_from, slot_to });
			}
			if (eventName == "DropAmmoClip")
			{
				int index;
				args.GetArg<int>(0, index);

				if (SItemComponent *pAmmoClipItem = pPlayer->GetInventory()->GetAmmoClipItem(index))
					pAmmoClipItem->ClientDrop();
			}
			if (eventName == "DropItem")
			{
				int index;
				args.GetArg<int>(0, index);

				if (SItemComponent *pWeaponItem = pPlayer->GetInventory()->GetWeaponItem(index))
					pWeaponItem->ClientDrop();
			}
		}
	}
	CPlayerComponent *pPlayer = nullptr;
	int slot_first = -1;
};