#pragma once

#include "BaseMenu.h"

class CLoginMenu : public SBaseMenu
{
public:
	CLoginMenu::CLoginMenu(string sElementName, CAccountComponent *_localAccount);

	virtual void OnEvent(string sEventName, const SUIArguments& args) override;

	void AttemptLogin();
	void NoAccessShow();
	string GetAttemptedLogin() { return sLogin; }

private:
	string sLogin;
	string sPassword;
};