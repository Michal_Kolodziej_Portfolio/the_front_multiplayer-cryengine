#include "StdAfx.h"
#include "LobbyMenu.h"
#include "UISystem/BaseEventListener.h"


void CLobbyMenu::SetAccount(string login, int team, int index)
{
	SUIArguments args;
	args.AddArgument<string>(login);
	args.AddArgument<int>(team);
	args.AddArgument<int>(index);
	pElement->CallFunction("SetPlayer", args);
}

void CLobbyMenu::RemoveAccount(string login)
{
	SUIArguments args;
	args.AddArgument<string>(login);
	pElement->CallFunction("RemovePlayer", args);
}

void CLobbyMenu::Refresh()
{
	auto *pEntityIterator = gEnv->pEntitySystem->GetEntityIterator();
	pEntityIterator->MoveFirst();

	while (!pEntityIterator->IsEnd())
	{
		IEntity *pEntity = pEntityIterator->Next();

		if (CAccountComponent *pAccount = pEntity->GetComponent<CAccountComponent>())
		{
			if (pAccount->InLobby())
				SetAccount(pAccount->GetLogin(), pAccount->GetTeamIndex(), pAccount->GetIndexInTeam());
		}
	}
}

void CLobbyMenu::AllowJoin(bool allow)
{		
	SUIArguments allowJoin;
	allowJoin.AddArgument<bool>(allow);
	pElement->CallFunction("AllowJoin", allowJoin);
}

CLobbyMenu::CLobbyMenu(string sElementName, CAccountComponent * _localAccount)
{
	sName = sElementName;
	pLocalAccount = _localAccount;
	pManager = gEnv->pFlashUI->GetUIActionManager();
	pElement = gEnv->pFlashUI->GetUIElement(sName);
	pShow = gEnv->pFlashUI->GetUIAction(sName + "show");
	pHide = gEnv->pFlashUI->GetUIAction(sName + "hide");
	SUIArguments args;
	args.AddArgument<string>(sName);
	pManager->StartAction(pHide, args);
	pEventListener = new SBaseEventListener();
	pEventListener->pMenu = this;
	Initialize();
}

void CLobbyMenu::OnEvent(string sEventName, const SUIArguments & args)
{
	if (sEventName == "Exit")
	{
		gEnv->pConsole->ExecuteString("disconnect", false, true);
		gEnv->pConsole->ExecuteString("quit", false, true);
	}
	else if (sEventName == "Join")
	{
		Hide();
		GetServer()->ClientJoinGame();
	}
	else if (sEventName == "MoveToTeamRequest")
	{
		int team = -1;
		args.GetArg(0, team);

		GetServer()->ClientMoveToTeam(team);
	}
}