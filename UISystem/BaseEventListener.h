/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Base event listener is event listener for all ui elements

--------------------------------------------------------------------------------- */

#pragma once
#include "FlashUI/FlashUI.h"


struct SBaseEventListener : public IUIElementEventListener
{
	virtual void OnUIEvent(IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args) override
	{
		string eventName = event.sDisplayName;

		if (pMenu)
			pMenu->OnEvent(eventName, args);
		else
			CryLogAlways("No menu found for this listener");
	}
	SBaseMenu *pMenu = nullptr;
};