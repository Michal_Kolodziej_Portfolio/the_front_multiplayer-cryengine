#pragma once
#include "CryString/CryString.h"

struct IUIElement;
struct IUIAction;
struct IUIActionManager;
struct SBaseEventListener;
struct SUIArguments;
class CAccountComponent;

struct SBaseMenu
{
public:
	SBaseMenu::SBaseMenu() {}
	SBaseMenu::SBaseMenu(string sElementName, CAccountComponent *_localAccount);
	virtual SBaseMenu::~SBaseMenu() {}

	virtual void Initialize();

	virtual void Show();
	virtual void Hide();

	virtual void AddListener();
	virtual void RemoveListener();

	virtual void OnEvent(string sEventName, const SUIArguments& args) {}

	virtual bool IsOpened() { return bIsOpened; }

protected:

	IUIElement *pElement = nullptr;
	IUIAction *pShow = nullptr;
	IUIAction *pHide = nullptr;
	IUIActionManager *pManager = nullptr;

	string sName;

	SBaseMenu *pThisMenu = nullptr;

	SBaseEventListener *pEventListener = nullptr;

	CAccountComponent *pLocalAccount;

	bool bIsOpened = false;
};