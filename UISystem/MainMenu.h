#pragma once

#include "BaseMenu.h"

class CMainMenu : public SBaseMenu
{
public:
	CMainMenu::CMainMenu(string sElementName, CAccountComponent *_localAccount);

	virtual void Initialize() override;

	virtual void OnEvent(string sEventName, const SUIArguments& args) override;

	int GetClassIndex(string className);

private:
	string sClassList[10];
	string sClassListFull[10];
};