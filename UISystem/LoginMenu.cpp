#include "StdAfx.h"
#include "LoginMenu.h"
#include "BaseEventListener.h"
#include "GlobalResources.h"
#include "Components/ServerComponent.h"
#include "Components/AccountComponent.h"

void CLoginMenu::AttemptLogin()
{
	GetServer()->ClientAttemptLogin(sLogin, sPassword, pLocalAccount->GetEntityId());
}

void CLoginMenu::NoAccessShow()
{
	pElement->CallFunction("NoAccess");
}

CLoginMenu::CLoginMenu(string sElementName, CAccountComponent * _localAccount)
{
	sName = sElementName;
	pLocalAccount = _localAccount;
	pManager = gEnv->pFlashUI->GetUIActionManager();
	pElement = gEnv->pFlashUI->GetUIElement(sName);
	pShow = gEnv->pFlashUI->GetUIAction(sName + "show");
	pHide = gEnv->pFlashUI->GetUIAction(sName + "hide");
	SUIArguments args;
	args.AddArgument<string>(sName);
	pManager->StartAction(pHide, args);
	pEventListener = new SBaseEventListener();
	pEventListener->pMenu = this;
	Initialize();
}

void CLoginMenu::OnEvent(string sEventName, const SUIArguments& args)
{
	if (sEventName == "ReceiveLogin")
	{
		args.GetArg(0, sLogin);
	}
	else if (sEventName == "ReceivePasswordAndLogIn")
	{
		//this is called after pressing login button
		args.GetArg(0, sPassword);
		AttemptLogin();
	}
}
