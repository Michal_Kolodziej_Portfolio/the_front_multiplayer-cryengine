#pragma once

#include "UISystem/BaseMenu.h"

class CLobbyMenu : public SBaseMenu
{
public:
	CLobbyMenu::CLobbyMenu(string sElementName, CAccountComponent *_localAccount);
	
	virtual void OnEvent(string sEventName, const SUIArguments& args);

	void SetAccount(string login, int team, int index);
	void RemoveAccount(string login);

	void Refresh();

	void AllowJoin(bool allow);
};