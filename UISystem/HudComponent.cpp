#include "StdAfx.h"
#include "HudComponent.h"
#include "Components/PlayerComponent.h"
#include "FlashUI/FlashUI.h"
#include "Components/WeaponComponent.h"
#include "GlobalResources.h"
#include "Components/ServerComponent.h"
#include "Components/GameTeam.h"

CHudComponent::CHudComponent()
	: pHud(nullptr)
	, pShow(nullptr)
	, pHide(nullptr)
	, pMan(nullptr)
{}

void CHudComponent::ReflectType(Schematyc::CTypeDesc<CHudComponent>& desc)
{
	desc.SetGUID("{D78D8D94-AC48-4D08-98A7-3D4019266394}"_cry_guid);
}

void CHudComponent::Initialize()
{
	if (pPlayer = m_pEntity->GetComponent<CPlayerComponent>())
	{
		if (IFlashUI *pFlash = gEnv->pFlashUI)
		{
			pHud = pFlash->GetUIElement("Hud");
			pShow = pFlash->GetUIAction("hudshow");
			pHide = pFlash->GetUIAction("hudhide");
			pMan = pFlash->GetUIActionManager();
		}
		if (IsLoaded())
		{
			SUIArguments initArgs;
			initArgs.AddArgument(int(pPlayer->GetProperties()->GetHealth(false)));
			initArgs.AddArgument(int(pPlayer->GetProperties()->GetHealth(true)));
			initArgs.AddArgument(int(pPlayer->GetProperties()->GetStamina(false)));
			initArgs.AddArgument(int(pPlayer->GetProperties()->GetStamina(true)));
			initArgs.AddArgument(string(pPlayer->GetInventory()->GetCurrentItemName()));
			initArgs.AddArgument(string(GetServer()->GetTeam(Team_Lobby_01)->GetTeamName()));
			initArgs.AddArgument(string(GetServer()->GetTeam(Team_Lobby_02)->GetTeamName()));
			//GAMERULES team scores (team 1 and team 2)
			initArgs.AddArgument(int(0));
			initArgs.AddArgument(int(0));
			pHud->CallFunction("Init", initArgs);
			Show();
		}
	}
}

void CHudComponent::Show()
{
	if (IsLoaded())
	{
		pMan->StartAction(pShow, "Hud");
	}
}

void CHudComponent::Hide()
{
	if (IsLoaded())
	{
		pMan->StartAction(pHide, "Hud");
	}
}

void CHudComponent::UpdateCurrentProgressBars()
{
	SUIArguments args;
	args.AddArgument(int(pPlayer->GetProperties()->GetHealth(false)));
	args.AddArgument(int(pPlayer->GetProperties()->GetStamina(false)));
	pHud->CallFunction("UpdateCurHud", args);
}

void CHudComponent::UpdateCompleteProgressBars()
{
	SUIArguments args;
	args.AddArgument(int(pPlayer->GetProperties()->GetHealth(false)));
	args.AddArgument(int(pPlayer->GetProperties()->GetHealth(true)));
	args.AddArgument(int(pPlayer->GetProperties()->GetStamina(false)));
	args.AddArgument(int(pPlayer->GetProperties()->GetStamina(true)));
	pHud->CallFunction("UpdateCompletely", args);
}

void CHudComponent::UpdateWeaponInfo()
{
	if (CWeaponComponent *pWeapon = pPlayer->GetInventory()->HasWeaponSelected())
	{
		HideWeaponInfo(false);
		SUIArguments args;
		args.AddArgument(string(pPlayer->GetInventory()->GetCurrentItemName()));
		args.AddArgument(int(pWeapon->GetMaxAmmo()));
		args.AddArgument(int(pPlayer->GetInventory()->GetAmmoClipNumForCurrentWeapon()));
		args.AddArgument(int(pWeapon->GetCurrentAmmo()));
		pHud->CallFunction("UpdateWeaponInfo", args);
		return;
	}
	HideWeaponInfo(true);
}

void CHudComponent::UpdateAmmo()
{
	if (CWeaponComponent *pWeapon = pPlayer->GetInventory()->HasWeaponSelected())
	{
		SUIArguments args;
		args.AddArgument(int(pWeapon->GetMaxAmmo()));
		args.AddArgument(int(pPlayer->GetInventory()->GetAmmoClipNumForCurrentWeapon()));
		args.AddArgument(int(pWeapon->GetCurrentAmmo()));
		pHud->CallFunction("UpdateAmmo", args);
	}
}

void CHudComponent::HideWeaponInfo(bool hide)
{
	SUIArguments args;
	args.AddArgument(bool(hide));
	pHud->CallFunction("HideWeaponInfo", args);
}

void CHudComponent::SetScore(int tm1_score, int tm2_score)
{
	{
		SUIArguments args;
		args.AddArgument(int(Team_Lobby_01));
		args.AddArgument(int(tm1_score));
		pHud->CallFunction("SetScore", args);
	}
	{
		SUIArguments args;
		args.AddArgument(int(Team_Lobby_02));
		args.AddArgument(int(tm2_score));
		pHud->CallFunction("SetScore", args);
	}
}

void CHudComponent::VehicleInfoShow(bool show)
{
	SUIArguments args;
	args.AddArgument<bool>(show);
	pHud->CallFunction("VehicleInfoShow", args);
}

void CHudComponent::SetVehicle(string name, int rpm, int gear, int gear_max)
{
	SUIArguments args;
	args.AddArgument<string>(name);
	args.AddArgument<int>(rpm);
	args.AddArgument<int>(gear);
	args.AddArgument<int>(gear_max);
	pHud->CallFunction("VehicleInfoShow", args);
}

void CHudComponent::UpdateVehicleInfo(int rpm, int gear, int gear_max)
{
	SUIArguments args;
	args.AddArgument<int>(rpm);
	args.AddArgument<int>(gear);
	args.AddArgument<int>(gear_max);
	pHud->CallFunction("UpdateVehicleInfo", args);
}

void CHudComponent::UpdateTeamInfo()
{
	SUIArguments args;
	args.AddArgument<string>(GetServer()->GetTeam(Team_Lobby_01)->GetTeamName());
	args.AddArgument<string>(GetServer()->GetTeam(Team_Lobby_02)->GetTeamName());
	args.AddArgument<int>(GetServer()->GetTeam(Team_Lobby_01)->GetScore());
	args.AddArgument<int>(GetServer()->GetTeam(Team_Lobby_02)->GetScore());
	pHud->CallFunction("UpdateTeamInfo", args);
}

bool CHudComponent::IsLoaded()
{
	return pHud && pHide && pShow && pMan && pPlayer;
}
