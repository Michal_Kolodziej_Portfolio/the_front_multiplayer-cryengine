#include "StdAfx.h"
#include "BaseMenu.h"
#include "BaseEventListener.h"

SBaseMenu::SBaseMenu(string sElementName, CAccountComponent *_localAccount)
{
	sName = sElementName;
	pLocalAccount = _localAccount;
	pManager = gEnv->pFlashUI->GetUIActionManager();
	pElement = gEnv->pFlashUI->GetUIElement(sName);
	pShow = gEnv->pFlashUI->GetUIAction(sName + "show");
	pHide = gEnv->pFlashUI->GetUIAction(sName + "hide");
	SUIArguments args;
	args.AddArgument<string>(sName);
	pManager->StartAction(pHide, args);
	pEventListener = new SBaseEventListener();
	pEventListener->pMenu = this;
	Initialize();
}

void SBaseMenu::Initialize()
{
	pElement->CallFunction("Init");
	pThisMenu = this;
}

void SBaseMenu::Show()
{
	AddListener();
	SUIArguments args;
	args.AddArgument<string>(sName);
	pManager->StartAction(pShow, args);
	bIsOpened = true;
}

void SBaseMenu::Hide()
{
	SUIArguments args;
	args.AddArgument<string>(sName);
	pManager->StartAction(pHide, args);
	RemoveListener();
	bIsOpened = false;
}

void SBaseMenu::AddListener()
{
	pElement->AddEventListener(pEventListener, sName);
}

void SBaseMenu::RemoveListener()
{
	pElement->RemoveEventListener(pEventListener);
}
