/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : It is attached to each local player. It is visual representation of the server's player properties.

--------------------------------------------------------------------------------- */

#pragma once
#include "CryEntitySystem/IEntityComponent.h"

struct IUIElement;
struct IUIAction;
struct IUIActionManager;
class CPlayerComponent;

class CHudComponent final : public IEntityComponent
{
public:
	CHudComponent::CHudComponent();
	static void ReflectType(Schematyc::CTypeDesc<CHudComponent>& desc);
	void Initialize();
	void Show();
	void Hide();
	void UpdateCurrentProgressBars();
	void UpdateCompleteProgressBars();
	void UpdateWeaponInfo();
	void UpdateAmmo();
	void HideWeaponInfo(bool hide);
	void SetScore(int tm1_score, int tm2_score);
	void VehicleInfoShow(bool show = true);
	void SetVehicle(string name, int rpm, int gear, int gear_max);
	void UpdateVehicleInfo(int rpm, int gear, int gear_max);
	void UpdateTeamInfo();

private:
	bool IsLoaded();
private:
	IUIElement *pHud;
	IUIAction *pShow;
	IUIAction *pHide;
	IUIActionManager *pMan;
	//local player - owner of this component
	CPlayerComponent *pPlayer = nullptr;
};