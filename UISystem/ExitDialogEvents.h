/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Exit dialog event listener

--------------------------------------------------------------------------------- */

#pragma once
#include "FlashUI/FlashUI.h"


class CExitDialogEvents : public IUIElementEventListener
{
public:
	virtual void OnUIEvent(IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args) override
	{
		if (!gEnv->bServer)
		{
			string eventName = event.sDisplayName;

			if (eventName == "Exit")
			{
				//disconnecting from server happens first and is automatically caught by CGamePlugin and then sends messages to players of client disconnecting from server
				gEnv->pConsole->ExecuteString("disconnect", false, true);
				//when disconnection's done, quit game
				gEnv->pConsole->ExecuteString("quit", false, true);
			}
			if (eventName == "Back")
			{
				//if on exit dialog player chooses no, or if presses escape once again this function will be called and it will hide exit dialog
				player->bExitDialogOpened = false;
				player->bFreezePlayer = false;
				player->GetUISystem()->ShowExitDialog(false);
			}
		}
	}
	CPlayerComponent *player = nullptr;
};