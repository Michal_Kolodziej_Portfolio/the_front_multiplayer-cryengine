/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Ammo pack dialog event listener

--------------------------------------------------------------------------------- */

#pragma once
#include "FlashUI/FlashUI.h"


class CAmmoPackDialogEvents : public IUIElementEventListener
{
public:
	virtual void OnUIEvent(IUIElement* pSender, const SUIEventDesc& event, const SUIArguments& args) override
	{
		if (!gEnv->bServer)
		{
			string eventName = event.sDisplayName;

			if (eventName == "Deposit" || eventName == "Take")
			{
				int bulletCount = 0;
				args.GetArg(0, bulletCount);
				bool bDeposit = (eventName == "Deposit") ? true : false;
				SRmi<RMI_WRAP(&CPlayerComponent::ServerAmmoPackManagement)>::InvokeOnServer(player, AmmoPackManagementParams{ bulletCount, bDeposit });
			}
		}
	}
	CPlayerComponent *player = nullptr;
};