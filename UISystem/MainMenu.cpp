#include "StdAfx.h"
#include "MainMenu.h"
#include "BaseEventListener.h"
#include "CryEntitySystem/IEntitySystem.h"
#include "Components/AccountComponent.h"
#include "UISystem/LobbyMenu.h"
#include "UISystem/MainMenu.h"


CMainMenu::CMainMenu(string sElementName, CAccountComponent *_localAccount)
{
	sName = sElementName;
	pLocalAccount = _localAccount;
	pManager = gEnv->pFlashUI->GetUIActionManager();
	pElement = gEnv->pFlashUI->GetUIElement(sName);
	pShow = gEnv->pFlashUI->GetUIAction(sName + "show");
	pHide = gEnv->pFlashUI->GetUIAction(sName + "hide");
	SUIArguments args;
	args.AddArgument<string>(sName);
	pManager->StartAction(pHide, args);
	pEventListener = new SBaseEventListener();
	pEventListener->pMenu = this;
	Initialize();
}

void CMainMenu::Initialize()
{
	IEntityClassRegistry *pClassReg = gEnv->pEntitySystem->GetClassRegistry();
	IEntityClass *pClass = nullptr;
	SUIArguments classes;
	pClassReg->IteratorMoveFirst();
	for (int i = 0; i < pClassReg->GetClassCount(); i++)
	{
		if (pClass = pClassReg->IteratorNext())
		{
			string sClassName = pClass->GetName();
			sClassName.erase(0, sClassName.find_last_of(":") + 1);
			string sClassNameProper = sClassName;
			sClassNameProper.erase(0, 7);
			sClassName.erase(6, sClassName.length());
			if (sClassName == "player")
			{

				for (int x = 0; x < 10; x++)
				{
					if (sClassList[x].empty())
					{
						sClassList[x] = sClassNameProper;
						sClassListFull[x] = pClass->GetName();
						classes.AddArgument(sClassNameProper);
						break;
					}
				}
			}
		}
	}
	pElement->SetArray("classesArr", classes);
	SUIArguments args;
	args.AddArgument<int>(0);
	//call initialize function in flash
	pElement->CallFunction("Init", args);
}


int CMainMenu::GetClassIndex(string className)
{
	for (int i = 0; i < 10; i++)
	{
		if (sClassList[i] == className)
		{
			return i;
		}
	}
	return -1;
}


void CMainMenu::OnEvent(string sEventName, const SUIArguments & args)
{
	if (sEventName == "Play")
	{
		string sClass;
		args.GetArg(0, sClass);
		Hide();
		GetServer()->ClientJoinLobby(sClassListFull[GetClassIndex(sClass)]);
		//this is first appearance of this local account in lobby; so in this case we will actually set up whole lobby for local account with info of all accounts
		pLocalAccount->GetLobbyMenu()->Refresh();
		pLocalAccount->GetLobbyMenu()->Show();
	}
	else if (sEventName == "ExitGame")
	{
		gEnv->pConsole->ExecuteString("disconnect", false, true);
		gEnv->pConsole->ExecuteString("quit", false, true);
	}
}
