#include "StdAfx.h"
#include "GameRules.h"
#include "Components/BulletComponent.h"
#include "Components/PlayerComponent.h"

SGameRules::SGameRules()
{
	RegisterCvars();
}

SGameRules::~SGameRules()
{
}

void SGameRules::RegisterCvars()
{
	IConsole *console = gEnv->pConsole;

	REGISTER_INT("sv_rules_respawn_time", 5, VF_NULL, "Respawn time in seconds");
	pRespawnTime = console->GetCVar("sv_rules_respawn_time");
}

void SGameRules::Update()
{
}

void SGameRules::TimerEvent(CPlayerComponent * pPlayer, int timerId)
{
	if (!pPlayer)
		return;

	if (timerId == Timer_Respawn)
	{
		RespawnPlayer(pPlayer);
	}
}

void SGameRules::Hit(CBulletComponent * pBullet, CPlayerComponent * pAttacker, CPlayerComponent * pVictim)
{
	if (!pBullet || !pAttacker || !pVictim)
		return;

	const float bulletDamage = pBullet->GetDamage();
	//if player gets hit, apply damage to his health
	//check if current health is larger than damage we want to apply
	if (pVictim->GetProperties()->GetHealth(false) > bulletDamage)
	{
		//apply damage; health is automatically serialized when changes
		pVictim->GetProperties()->UseHealth(pBullet->GetDamage());
		pVictim->ServerUpdateVitalData();
	}
	//now if current health is less or equals damage we want to apply, we shouldn't apply damage, we kill the player instead
	else
	{
		//in this case firstly we will add death to victim and add kill to attacker
		//we don't cound frags when attacker kills teammate, instead of this you'll get counted death and victim(your teammate) does not suffer any penalty(he stays dead though)
		if (pAttacker->GetTeam() == pVictim->GetTeam())
			pAttacker->ServerAddDeath();
		else
		{
			pAttacker->ServerAddKill();
			pAttacker->GetTeam()->AddScore();
			pVictim->ServerAddDeath();
		}
		GetServer()->ServerKillDeath(pAttacker, pVictim);
		//in the end kill victim
		KillPlayer(pVictim);
		//set timer for eventual respawn
		pVictim->GetEntity()->SetTimer(Timer_Respawn, pRespawnTime->GetIVal()*1000);
	}
}

void SGameRules::Hit(float damage, CPlayerComponent * pVictim)
{
	if (!pVictim)
		return;

	if (pVictim->GetProperties()->GetHealth(false) > damage)
	{
		pVictim->GetProperties()->UseHealth(damage);
		pVictim->ServerUpdateVitalData();
	}
	else
	{
		pVictim->ServerAddDeath();
		GetServer()->ServerKillDeath(pVictim, pVictim);
		KillPlayer(pVictim);
		pVictim->GetEntity()->SetTimer(Timer_Respawn, pRespawnTime->GetIVal() * 1000);
	}
}

void SGameRules::KillPlayer(CPlayerComponent * pPlayer)
{	
	//zero out health
	pPlayer->GetProperties()->SetHealth(false, 0.f);
	//make dead
	pPlayer->IsPlayerAlive(false);
	//update inventory - drop everything he has
	pPlayer->GetInventory()->ServerDropEverything();
	//serialize info
	pPlayer->ServerUpdateGameplayData();
}

void SGameRules::RespawnPlayer(CPlayerComponent * pPlayer)
{
	pPlayer->GetProperties()->SetHealth(false, pPlayer->GetProperties()->GetHealth(true));
	pPlayer->IsPlayerAlive(true);
	pPlayer->MoveToSpawnPoint();
	pPlayer->ServerUpdateVitalData();
}
