#include "StdAfx.h"
#include "GamePlugin.h"

#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/EnvPackage.h>
#include <CrySchematyc/Utils/SharedString.h>

#include <IGameObjectSystem.h>
#include <IGameObject.h>

// Included only once per DLL module.
#include <CryCore/Platform/platform_impl.inl>
#include <CryNetwork/Rmi.h>
#include "Components/AccountComponent.h"
#include "Components/PlayerComponent.h"
#include "Components/ServerComponent.h"
#include "Components/GameTeam.h"
#include "GameConsoleVariables.h"

#include "Components/EditorPlayerComponent.h"

CGamePlugin *CGamePlugin::plugin = nullptr;
float CGamePlugin::day_time = 5.f;
int CGamePlugin::last_sec = 0;

CGamePlugin::CGamePlugin()
{
	RegisterFlowNodes();
}

CGamePlugin::~CGamePlugin()
{
	// Remove any registered listeners before 'this' becomes invalid
	gEnv->pGameFramework->RemoveNetworkedClientListener(*this);
	gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);

	if (gEnv->pSchematyc)
	{
		gEnv->pSchematyc->GetEnvRegistry().DeregisterPackage(CGamePlugin::GetCID());
	}
}

bool CGamePlugin::Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams)
{
	gEnv->pSystem->GetISystemEventDispatcher()->RegisterListener(this, "CGamePlugin");
	gEnv->pGameFramework->AddNetworkedClientListener(*this);
	time_t t = time(0);
	struct tm * now = localtime(&t);
	last_sec = now->tm_sec;
	SetUpdateFlags(EUpdateType_Update);
	plugin = this;

	pGameCvars = new SGameCVars();
	return true;
}

void CGamePlugin::OnPluginUpdate(EPluginUpdateType updateType)
{
	if (gEnv->bServer)
	{
		time_t t = time(0);
		struct tm * now = localtime(&t);
		//seconds counting
		if (now->tm_sec == last_sec + 1)
		{
			last_sec = now->tm_sec;
			if (day_time < 23)
			{
				day_time += 0.01f;
			}
			else
				day_time = 0.f;
		}
		else if (now->tm_sec == 0)
		{
			last_sec = now->tm_sec;
		}
	}
}

void CGamePlugin::OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam)
{
	switch (event)
	{
	case ESYSTEM_EVENT_REGISTER_SCHEMATYC_ENV:
	{
		auto staticAutoRegisterLambda = [](Schematyc::IEnvRegistrar& registrar)
		{
			Detail::CStaticAutoRegistrar<Schematyc::IEnvRegistrar&>::InvokeStaticCallbacks(registrar);
		};
		if (gEnv->pSchematyc)
		{
			gEnv->pSchematyc->GetEnvRegistry().RegisterPackage(
				stl::make_unique<Schematyc::CEnvPackage>(
					CGamePlugin::GetCID(),
					"EntityComponents",
					"Crytek GmbH",
					"Components",
					staticAutoRegisterLambda
					)
			);
		}
	}
	break;
	case ESYSTEM_EVENT_GAME_POST_INIT:
	{
		if (!gEnv->IsEditor())
		{
			if (!gEnv->IsDedicated())
				gEnv->pConsole->ExecuteString("connect", false, true);
			else
			{
				if (XmlNodeRef svMap = gEnv->pSystem->LoadXmlFromFile("ServerDatabase/ServerMap.xml"))
				{
					if (XmlNodeRef svAddress = gEnv->pSystem->LoadXmlFromFile("ServerDatabase/ServerAddress.xml"))
					{
						if (XmlNodeRef xmlMap = svMap->getChild(0))
						{
							if (XmlNodeRef address = svAddress->getChild(0))
							{
								string mapName = xmlMap->getAttr("name");
								string addrStr = address->getAttr("addr");
								gEnv->pConsole->ExecuteString("map " + mapName, false, true);
								gEnv->pConsole->ExecuteString("sv_bind " + addrStr, false, true);
								gEnv->pConsole->ExecuteString("map " + mapName, false, true);
							}
						}
					}
				}
			}
		}
	}
	break;
	case ESYSTEM_EVENT_LEVEL_LOAD_END:
	{
		if (!gEnv->IsEditor())
		{
			//CryLogAlways("LEVEL READY FOR GAMEPLAY");
			//SEntitySpawnParams spawnParams;
			//spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
			//spawnParams.sName = "SERVER";
			//IEntity *pServerEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams);
			//pServer = pServerEntity->GetOrCreateComponentClass<SServerComponent>();
		}
	}
	break;
	case ESYSTEM_EVENT_LEVEL_LOAD_START:
	{
		if (!gEnv->IsEditor())
		{
			CryLogAlways("LEVEL READY FOR GAMEPLAY");
			SEntitySpawnParams spawnParams;
			spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
			spawnParams.sName = "SERVER";
			IEntity *pServerEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams);
			pServer = pServerEntity->GetOrCreateComponentClass<SServerComponent>();
		}
	}
	break;
	}
}

bool CGamePlugin::OnClientConnectionReceived(int channelId, bool bIsReset)
{
	if (!gEnv->IsEditor())
	{
		SEntitySpawnParams spawnParams;
		spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
		string player_name = string().Format("Client%" PRISIZE_T, m_accounts.size());
		spawnParams.sName = player_name;
		spawnParams.nFlags |= ENTITY_FLAG_NEVER_NETWORK_STATIC;
		if (m_accounts.size() == 0 && !gEnv->IsDedicated())
		{
			spawnParams.id = LOCAL_PLAYER_ENTITY_ID;
			spawnParams.nFlags |= ENTITY_FLAG_LOCAL_PLAYER;
		}
		if (IEntity* pClientEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams))
		{
			CAccountComponent *pAccount = pClientEntity->GetOrCreateComponentClass<CAccountComponent>();
			CryLogAlways("%s connection received. Id %d", player_name, pClientEntity->GetId());
			pClientEntity->GetNetEntity()->SetChannelId(channelId);
			pClientEntity->GetNetEntity()->BindToNetwork();
			m_accounts.emplace(std::make_pair(channelId, pClientEntity->GetId()));
		}
	}
	else
	{
		SEntitySpawnParams spawnParams;
		spawnParams.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
		spawnParams.sName = "Player";
		spawnParams.nFlags |= ENTITY_FLAG_NEVER_NETWORK_STATIC;
		if (m_players.size() == 0 && !gEnv->IsDedicated())
		{
			spawnParams.id = LOCAL_PLAYER_ENTITY_ID;
			spawnParams.nFlags |= ENTITY_FLAG_LOCAL_PLAYER;
		}
		if (IEntity* pPlayerEntity = gEnv->pEntitySystem->SpawnEntity(spawnParams))
		{
			pPlayerEntity->GetNetEntity()->SetChannelId(channelId);
			pPlayerEntity->GetNetEntity()->BindToNetwork();
			CEditorPlayerComponent* pPlayer = pPlayerEntity->GetOrCreateComponentClass<CEditorPlayerComponent>();
		}
	}
	return true;
}

bool CGamePlugin::OnClientReadyForGameplay(int channelId, bool bIsReset)
{
	return true;
}

void CGamePlugin::OnClientDisconnected(int channelId, EDisconnectionCause cause, const char* description, bool bKeepClient)
{
	auto it = m_accounts.find(channelId);
	if (it != m_accounts.end())
	{
		if (CAccountComponent* account = gEnv->pEntitySystem->GetEntity(it->second)->GetComponent<CAccountComponent>())
		{
			if (!account->GetLogin().empty())
			{
				//update lobby hierarchy
				account->GetTeam()->RemoveAccount(account);
				CryLogAlways("ACCOUNT %s DISCONNECTED", account->GetLogin());
				//if player was already spawned
				if (CPlayerComponent *pAssociatedPlayer = account->GetAssociatedPlayer())
				{
					auto pl = m_players.find(channelId);
					m_players.erase(pl);
					gEnv->pEntitySystem->RemoveEntity(pAssociatedPlayer->GetEntityId());
				}
			}
			else
				CryLogAlways("UNKNOWN CLIENT LEFT THE SERVER");
		}

		gEnv->pEntitySystem->RemoveEntity(it->second);

		m_accounts.erase(it);
	}
}

bool CGamePlugin::RegisterFlowNodes()
{
	CryRegisterFlowNodes();
	return true;
}

bool CGamePlugin::UnregisterFlowNodes()
{
	CryUnregisterFlowNodes();
	return true;
}



CRYREGISTER_SINGLETON_CLASS(CGamePlugin)