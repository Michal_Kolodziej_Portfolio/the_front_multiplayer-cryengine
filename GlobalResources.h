/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Global methods and other resources that will make it easier to access some things

--------------------------------------------------------------------------------- */

#ifndef GLOBALRESOURCES_H
#define GLOBALRESOURCES_H
#pragma once

class CGamePlugin;
struct SServerComponent;

CGamePlugin *GetPlugin();
//plugin database access
//Players
int Server_GetPlayersCount();
EntityId Server_GetPlayerEntityId(int index);
int Server_GetAccountsCount();
EntityId Server_GetAccountEntityId(int index);
void Server_AddPlayer(int channelId, EntityId id);
void Server_RemovePlayer(int channelId);
//others
IEntity *Server_GetEntityById(EntityId id, EntityId compId);
SServerComponent *GetServer();
float AddDayTime(float time);
void ConsoleLog(Vec3 vector);
void ConsoleLog(string message);
void ConsoleLog(int integer);
void ConsoleLog(float floating);


#endif