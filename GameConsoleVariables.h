/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Game console variables structure

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

struct SGameCVars
{
public:
	SGameCVars::SGameCVars();
	SGameCVars::~SGameCVars() {}

	void InitServerCvars();
	void InitClientCvars();
	void InitBothSidedCvars();

	//variables
	ICVar *pTeamModify = nullptr;
	ICVar *pTeamName = nullptr;
	ICVar *pAdminPass = nullptr;
};
