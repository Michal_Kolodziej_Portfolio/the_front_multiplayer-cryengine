/* ---------------------------------------------------------------------------------

Author : Micha� Ko�odziej
Project : WW2 Shooter (The Front)
Purpose : Game rules for the server handling players

--------------------------------------------------------------------------------- */

#pragma once

#include <CryEntitySystem/IEntityComponent.h>

class CBulletComponent;
class CPlayerComponent;

struct SGameRules
{
	enum Timers
	{
		Timer_Respawn = 1
	};

	SGameRules::SGameRules();
	SGameRules::~SGameRules();

	void RegisterCvars();
	void Update();
	void TimerEvent(CPlayerComponent *pPlayer, int timerId);

	void Hit(CBulletComponent *pBullet, CPlayerComponent *pAttacker, CPlayerComponent *pVictim);
	void Hit(float damage, CPlayerComponent *pVictim);
	void KillPlayer(CPlayerComponent *pPlayer);
	void RespawnPlayer(CPlayerComponent *pPlayer);

private:

	//cvars
	ICVar *pRespawnTime = nullptr;
};
