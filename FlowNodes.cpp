#include "StdAfx.h"
#include "CryFlowGraph/IFlowBaseNode.h"

class CNetworkSelector : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_SERVER = 0,
		OUT_CLIENT
	};
public:
	CNetworkSelector(SActivationInfo *pActInfo)
	{
		pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Perform check for network authority")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("IsServer", _HELP("Output if is server")),
			OutputPortConfig<SFlowSystemVoid>("IsClient", _HELP("Output if is client")),
			{ 0 }
		};

		config.sDescription = "Performs check whether current instance is client or server";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				if (gEnv->IsClient())
					ActivateOutput(pActInfo, OUT_CLIENT, 0);
				else
					ActivateOutput(pActInfo, OUT_SERVER, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CNetworkSelector(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};

class CClientCheck : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_CLIENT
	};
public:
	CClientCheck(SActivationInfo *pActInfo)
	{
		pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Perform check for network authority")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("IsClient", _HELP("Output if is client")),
			{ 0 }
		};

		config.sDescription = "Performs check whether current instance is client";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				if (gEnv->IsClient())
					ActivateOutput(pActInfo, OUT_CLIENT, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CClientCheck(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};

class CServerCheck : public CFlowBaseNode<eNCT_Instanced>
{
	enum Inputs
	{
		IN_TRIGGER = 0
	};
	enum Outputs
	{
		OUT_SERVER
	};
public:
	CServerCheck(SActivationInfo *pActInfo)
	{
		pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
	}

	void GetConfiguration(SFlowNodeConfig& config) override
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<SFlowSystemVoid>("Check", _HELP("Perform check for network authority")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("IsServer", _HELP("Output if is server")),
			{ 0 }
		};

		config.sDescription = "Performs check whether current instance is server";
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo) override
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, IN_TRIGGER))
			{
				if (!gEnv->IsClient())
					ActivateOutput(pActInfo, OUT_SERVER, 0);
			}
		}
		break;
		};
	}
	virtual IFlowNodePtr Clone(SActivationInfo *pActInfo) override
	{
		return new CServerCheck(pActInfo);
	}
	virtual void GetMemoryUsage(ICrySizer* s) const override
	{
		s->Add(*this);
	}
};

REGISTER_FLOW_NODE("TheFront:NetworkSelector", CNetworkSelector);
REGISTER_FLOW_NODE("TheFront:CheckIsClient", CClientCheck);
REGISTER_FLOW_NODE("TheFront:CheckIsServer", CServerCheck);