#include "GlobalResources.h"
#include "GamePlugin.h"

CGamePlugin *GetPlugin(){ return CGamePlugin::Get(); }

int Server_GetPlayersCount() { return GetPlugin()->GetPlayersCount(); }
EntityId Server_GetPlayerEntityId(int index) { return GetPlugin()->GetPlayerEntityId(index); }
int Server_GetAccountsCount() { return GetPlugin()->GetAccountsCount(); }
EntityId Server_GetAccountEntityId(int index) { return GetPlugin()->GetAccountEntityId(index); }
void Server_AddPlayer(int channelId, EntityId id) { GetPlugin()->AddPlayer(channelId, id); }
void Server_RemovePlayer(int channelId) { GetPlugin()->RemovePlayer(channelId); }
//other
IEntity * Server_GetEntityById(EntityId id, EntityId compId)
{
	//Getting entity by id
	//reason for this method to exist is that 'eid' compression policy works only sometimes(dont know why) and this method will choose valid entity id between uncompressed and compressed id
	IEntity *pEntity = gEnv->pEntitySystem->GetEntity(id);
	if (!pEntity)
		pEntity = gEnv->pEntitySystem->GetEntity(compId);

	return pEntity;
}

SServerComponent * GetServer()
{
	return GetPlugin()->GetServer();
}

float AddDayTime(float time) { return GetPlugin()->AddDayTime(time); }

void ConsoleLog(Vec3 vector)
{
	CryLogAlways("X = %f, Y = %f, Z = %f", vector.x, vector.y, vector.z);
}

void ConsoleLog(string message)
{
	CryLogAlways(message);
}

void ConsoleLog(int integer)
{
	CryLogAlways("NUMBER = %d", integer);
}

void ConsoleLog(float floating)
{
	CryLogAlways("NUMBER = %f", floating);
}
